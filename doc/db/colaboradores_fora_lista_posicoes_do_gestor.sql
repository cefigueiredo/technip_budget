-- Identificar colaboradores de um gestor cuja posição não estão entre as disponíveis para um gestor
select collab.*
  from bra_customhr_collaborators_v collab
 inner join bra_customhr_collaborators_v manager on manager.employee_person_id = collab.manager_person_id
 where collab.manager_person_id = 3081
   and not exists (
        -- posicoes disponiveis para a equipe do gestor.
        select DISTINCT
            hapf.position_id
            ,pj.job_id job_id
            ,haou.organization_id
            ,hapf.name position
            ,pj.name job
            ,haou.name uniorg
        from hr_all_positions_f                   hapf
            ,hr.per_jobs                          pj
            ,hr.hr_all_organization_units        haou
            ,apps.per_org_structure_elements_v    pose
            ,hr.per_all_people_f                  papf
            ,hr.per_all_assignments_f             paaf
            ,hr.per_periods_of_service            ppos
            ,hr.per_organization_structures       pos
            ,hr.per_org_structure_versions        posv
            ,hr.per_job_definitions               pjd
        where papf.business_group_id              = 511
        and paaf.business_group_id               = 511
        and hapf.business_group_id               = 511
        and paaf.primary_flag                    = 'Y'
        and paaf.assignment_type                 = 'E'
        and hapf.job_id                          = pj.job_id
        and hapf.organization_id                 = haou.organization_id
        and papf.person_id                       = paaf.person_id
        and papf.person_id                       = ppos.person_id
        and pjd.job_definition_id                = pj.job_definition_id
        and pose.organization_id_parent          = paaf.organization_id
        and hapf.organization_id                 = pose.organization_id_child
        and pose.org_structure_version_id        = posv.org_structure_version_id
        and pos.organization_structure_id        = posv.organization_structure_id
        and hapf.availability_status_id          <> 5
        and (     posv.date_to                   is null
              or  posv.date_to                   >= sysdate   )
        and (     pj.date_to                     is null
              or  pj.date_to                     >= sysdate   )
        and ((hapf.status <> 'INVALID')          or hapf.status is null)
        and (ppos.actual_termination_date        is null or ppos.actual_termination_date > sysdate)
        and sysdate between hapf.effective_start_date and hapf.effective_end_date
        and sysdate between papf.effective_start_date and papf.effective_end_date
        and sysdate between paaf.effective_start_date and paaf.effective_end_date
        and to_char(pos.organization_structure_id) in (select fpov.profile_option_value
                                                        from apps.fnd_profile_options_vl            fpo
                                                            ,apps.fnd_profile_option_values         fpov
                                                        where fpov.profile_option_id                 = fpo.profile_option_id
                                                          and fpo.profile_option_name                = 'BRA_HR_HIERARQUIA_GERENCIAL'
                                                          and to_char(pos.organization_structure_id) = fpov.profile_option_value)
        and papf.employee_number = manager.employee_number
        and hapf.position_id     = collab.position_id
        )