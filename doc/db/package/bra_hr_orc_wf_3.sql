CREATE OR REPLACE
PACKAGE bra_hr_orc_wf_3 as
--
-- Package referente ao novo sistema de Or�amentos (RH Budget 2.0)
--
-- Objetivos: Manipular o Workflow de aprova��o dos planejamentos de or�amento.
--
-- Vers�o: 1.2.0
--
-- Changelog:
--
-- 1.2.0 - 01/09/2014
--  Felipe Mesquita - Adicionando os motivos na notificacao para cada posicao em cada cargo, separados pelo tipo (contratacao, desligamento ou promocao).
--
-- 1.1.0 - 22/08/2014
--  Carlos Figueiredo - Adicionando campo motivo para promo��es
--
-- 1.0.1 - 15/08/2014
--  Felipe Mesquita - Ajuste ao iniciar o workflow para utilizar como OwnerId o UserId do solicitante.
--
-- 0.0.1 - 21/08/2013 -
--  Felipe Mesquita - Adapta��o do workflow para o novo sistema de budget.

  flag_diretoria boolean := false; --- Flags populados pela procedure diretoria.
  flag_dir_rh boolean    := false; --- Flags populados pela procedure diretoria
  flag_dir_geral boolean := false; --- Flags populados pela procedure diretoria
  g_erro varchar2(200)   := null;

  g_itemtype CONSTANT wf_items.item_type%type:= 'BRAORC';

  g_count_total             number := 0;
  g_count_inseridos         number := 0;
  g_count_atualizados       number := 0;
  g_count_erro              number := 0;

  procedure cria_workflow (p_solicitante_id in number, p_budget_id in number, p_owner_id in number);

  procedure notifica_corte (p_solicitante_id in number, p_budget_id in number, p_owner_id in number);

  Function get_role (p_employee_id in number) return varchar2;

  procedure carrega_atributos  (itemtype     in     varchar2
                               ,itemkey      in     varchar2
                               ,actid        in     number
                               ,funcmode     in     varchar2
                               ,resultout    in out varchar2);

  procedure verifica_planejamento  (itemtype     in     varchar2
                               ,itemkey      in     varchar2
                               ,actid        in     number
                               ,funcmode     in     varchar2
                               ,resultout    in out varchar2);

  procedure identifica_filial  (itemtype     in     varchar2
                               ,itemkey      in      varchar2
                               ,actid        in     number
                               ,funcmode     in     varchar2
                               ,resultout    in out varchar2);

  procedure get_diretor        (itemtype     in     varchar2
                               ,itemkey      in      varchar2
                               ,actid        in     number
                               ,funcmode     in     varchar2
                               ,resultout    in out varchar2);

  procedure get_rh             (itemtype   in     varchar2
                               ,itemkey    in      varchar2
                               ,actid      in     number
                               ,funcmode   in     varchar2
                               ,resultout  in out varchar2);

  procedure get_super           (itemtype   in     varchar2
                                ,itemkey    in      varchar2
                                ,actid      in     number
                                ,funcmode   in     varchar2
                                ,resultout  in out varchar2);

  procedure set_doc_aprovadores (document_id   in varchar2
                                ,display_type  in varchar2
                                ,document        in out nocopy clob
                                ,document_type in out nocopy varchar2);

  procedure set_doc_corte       (document_id   in varchar2
                                ,display_type  in varchar2
                                ,document      in out nocopy clob
                                ,document_type in out nocopy varchar2);

  procedure commit_aprovacao    (itemtype     in     varchar2
                                ,itemkey      in     varchar2
                                ,actid        in     number
                                ,funcmode     in     varchar2
                                ,resultout    in out varchar2);

  procedure commit_rejeicao     (itemtype     in     varchar2
                                ,itemkey      in     varchar2
                                ,actid        in     number
                                ,funcmode     in     varchar2
                                ,resultout    in out varchar2);

  procedure update_status       (itemtype     in     varchar2
                                ,itemkey      in     varchar2
                                ,actid        in     number
                                ,funcmode     in     varchar2
                                ,resultout    in out varchar2);

  procedure atualiza_role_from       (itemtype     in     varchar2
                                ,itemkey      in     varchar2
                                ,actid        in     number
                                ,funcmode     in     varchar2
                                ,resultout    in out varchar2);






  procedure limpaPosicaoVazia (p_solicitante_id in number, p_budget_id in number);
  procedure ENCERRAR_WORKFLOW (p_solicitante_id in number, p_budget_id in number , p_owner_id in number, p_motivo in varchar2 );
  function getStatus (p_budget_id in number,p_solicitante_id in number) return varchar2;

  procedure concurrent
 (  errbuff  out    varchar2
 ,  retcode  out    NUMBER
 ,  p_budget_id in    number  );
end;
/

CREATE OR REPLACE
PACKAGE BODY bra_hr_orc_wf_3 as

-------------------------------------------------------------------------
-------------------- Procedure  limpaPosicaoVazia ---------------------------
-------------------------------------------------------------------------
procedure saveDadosRelatorio
    ( p_BUDGET_ID       in bra_hr_orc_relatorio.BUDGET_ID%type
    , p_BUDGET_NAME     in bra_hr_orc_relatorio.BUDGET_NAME%type
    , p_MES             in bra_hr_orc_relatorio.MES%type
    , p_ANO             in bra_hr_orc_relatorio.ANO%type
    , p_PROJETO_CCUSTO  in bra_hr_orc_relatorio.PROJETO_CCUSTO%type
    , p_QUANT_REAL      in bra_hr_orc_relatorio.QUANT_REAL%type
    , p_QUANT_ORCADO    in bra_hr_orc_relatorio.QUANT_ORCADO%type )
is
    l_msg varchar2(2000);

BEGIN

    INSERT INTO bra_hr_orc_relatorio
                (BUDGET_ID
                ,BUDGET_NAME
                ,MES
                ,ANO
                ,PROJETO_CCUSTO
                ,QUANT_REAL
                ,QUANT_ORCADO)
    VALUES
        (p_BUDGET_ID
        ,p_BUDGET_NAME
        ,p_MES
        ,p_ANO
        ,p_PROJETO_CCUSTO
        ,p_QUANT_REAL
        ,p_QUANT_ORCADO);

    g_count_inseridos := g_count_inseridos + 1;

    commit;

exception
    when others then

        if ( sqlcode = -1 ) then

            begin
                update bra_hr_orc_relatorio a
                   set BUDGET_ID      = p_BUDGET_ID
                      ,BUDGET_NAME    = p_BUDGET_NAME
                      ,MES            = p_MES
                      ,ANO            = p_ANO
                      ,PROJETO_CCUSTO = p_PROJETO_CCUSTO
                      ,QUANT_REAL     = nvl(p_QUANT_REAL,QUANT_REAL)
                      ,QUANT_ORCADO   = nvl(p_QUANT_ORCADO,QUANT_ORCADO)
                 where BUDGET_ID      = p_BUDGET_ID
                   and MES            = p_MES
                   and ANO            = p_ANO
                   and PROJETO_CCUSTO = p_PROJETO_CCUSTO;

                g_count_atualizados := g_count_atualizados + 1;

                commit;

            exception
                when others then
                    g_count_erro := g_count_erro +1;
                    l_msg := 'BRA_HR_ORC_WF_3.saveDadosRelatorio: Erro ao salvar dados (BUDGET_ID='||p_BUDGET_ID||',MES='||p_MES||',ANO='||p_ANO||',PROJETO_CCUSTO='||p_PROJETO_CCUSTO||') - ' || SQLERRM;
                    fnd_file.put_line(fnd_file.log,l_msg);
                    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);
                    rollback;
            end;

        else
            g_count_erro := g_count_erro +1;
            l_msg := 'BRA_HR_ORC_WF_3.saveDadosRelatorio: Erro ao salvar dados (BUDGET_ID='||p_BUDGET_ID||',MES='||p_MES||',ANO='||p_ANO||',PROJETO_CCUSTO='||p_PROJETO_CCUSTO||') - ' || SQLERRM;
            fnd_file.put_line(fnd_file.log,l_msg);
            BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);
            rollback;

        end if;

END saveDadosRelatorio;

-------------------------------------------------------------------------
-------------------- Procedure  carregaBudgetOrcado -----------------------
-------------------------------------------------------------------------
procedure carregaBudgetAtual(p_budget_id   in number
                            ,p_budget_name in BRA_BUDGET_HEADER.budget_name%type) is

    l_msg                   varchar2(2000);
    e_budget_not_found      exception;

    -- Projetos
    cursor c_budget_projetos is
        select * from BRA_HR_ORC_PROJETOS_V
        where budget_id = p_budget_id;

    -- CCusto
    cursor c_budget_ccustos is
        select * from BRA_HR_ORC_CCUSTO_V
        where budget_id = p_budget_id;

BEGIN


    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetAtual: Importando posi��o atual do Budget '|| p_budget_name ||'.';
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    g_count_total       := 0;
    g_count_inseridos   := 0;
    g_count_atualizados := 0;
    g_count_erro        := 0;

    -- Salvando PROJETOS
    for r1 in c_budget_projetos loop

        g_count_total := g_count_total + 1;
        saveDadosRelatorio(r1.BUDGET_ID,p_BUDGET_NAME,r1.MES,r1.ANO,r1.PROJETO_CCUSTO,r1.QUANT,null);

    end loop;

    -- Salvando CCusto
    for r1 in c_budget_ccustos loop

        g_count_total := g_count_total + 1;
        saveDadosRelatorio(r1.BUDGET_ID,p_BUDGET_NAME,r1.MES,r1.ANO,r1.PROJETO_CCUSTO,r1.QUANT,null);

    end loop;

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetAtual: Importa��o da posi��o atual do Budget '|| p_budget_name ||' conclu�da.';
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetAtual: Total='|| g_count_total;
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetAtual: Inseridos='|| g_count_inseridos;
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetAtual: Atualizados='|| g_count_atualizados;
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetAtual: Erros='|| g_count_erro;
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);


EXCEPTION
    when others then
        BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT','BRA_HR_ORC_WF_3.carregaBudgetAtual: '||SQLERRM);

END carregaBudgetAtual;



-------------------------------------------------------------------------
-------------------- Procedure  carregaBudgetOrcado -----------------------
-------------------------------------------------------------------------
procedure carregaBudgetOrcado(p_budget_id   in number
                             ,p_budget_name in BRA_BUDGET_HEADER.budget_name%type) is

    l_dt_inicio_periodo     BRA_BUDGET_HEADER.dt_inicio_periodo%type;
    l_dt_fim_periodo        BRA_BUDGET_HEADER.dt_fim_periodo%type;
    l_budget_name           BRA_BUDGET_HEADER.budget_name%type;
    l_reg                   BRA_HR_ORC_RELATORIO%rowtype;
    l_projeto_ccusto        BRA_HR_ORC_RELATORIO.projeto_ccusto%type;
    l_month                 date;

    l_msg                   varchar2(2000);
    e_budget_not_found      exception;


    -- Informa��es do HEADER
    cursor c_budget_header is
        select distinct bhbh.dt_inicio_periodo
        ,      bhbh.dt_fim_periodo
        ,      bhbh.budget_name
        from BRA_BUDGET_HEADER bhbh
        where bhbh.id_budget = p_budget_id;

    -- Posi��es or�adas para o Budget para um m�s espec�fico
    cursor c_orcado(p_month date) is
        select     bbe.id_budget
        ,        bhcp.projeto_ccusto
        ,        count(bbl.position_id)      quant
        from     BRA_BUDGET_EMPLOYEES   bbe
        ,         BRA_BUDGET_LINES       bbl
        ,        BRA_HR_ORC_CCUSTO_PROJETOS_V bhcp
        where     bbe.id_budget             = bbl.id_budget
        and       bbe.id_solicitante         = bbl.id_solicitante
        and       bbe.id_budget_line         = bbl.id_budget_line
        and     bbl.position_id         = bhcp.position_id
        and     trunc(p_month) between trunc(bbe.dt_inicio_periodo) and trunc(bbe.dt_fim_periodo)
        group by bbe.id_budget
        ,        bhcp.projeto_ccusto;

BEGIN

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetOrcado: Importando posi��o futura do Budget '|| p_budget_name ||'.';
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    g_count_total       := 0;
    g_count_inseridos   := 0;
    g_count_atualizados := 0;
    g_count_erro        := 0;

    -- Recuperando as informa��es do HEADER
    for r1 in c_budget_header loop

        l_budget_name         := r1.budget_name;
        l_dt_inicio_periodo   := trunc(r1.dt_inicio_periodo);
        l_dt_fim_periodo      := trunc(r1.dt_fim_periodo);

    end loop;

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetOrcado: l_budget_name= '|| l_budget_name ||'.';
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetOrcado: l_dt_inicio_periodo= '|| l_dt_inicio_periodo ||'.';
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetOrcado: l_dt_fim_periodo= '|| l_dt_fim_periodo ||'.';
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_month := l_dt_inicio_periodo;

    -- Percorrend m�s a m�s as posi��es
    while (trunc(l_month) between trunc(l_dt_inicio_periodo) and trunc(l_dt_fim_periodo)) loop

        l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetOrcado: l_month= '|| l_month ||'.';
        fnd_file.put_line(fnd_file.log,l_msg);
        BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

        for r2 in c_orcado(l_month) loop

            g_count_total := g_count_total + 1;
            saveDadosRelatorio(r2.id_budget,l_budget_name,to_char(l_month,'MM') ,to_char(l_month,'YYYY'),r2.PROJETO_CCUSTO,null,r2.QUANT);

        end loop;

        l_month := add_months(l_month,1);

    end loop;

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetOrcado: Importa��o da posi��o futura do Budget '|| p_budget_name ||' conclu�da.';
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetOrcado: Total='|| g_count_total;
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetOrcado: Inseridos='|| g_count_inseridos;
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetOrcado: Atualizados='|| g_count_atualizados;
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);

    l_msg := 'BRA_HR_ORC_WF_3.carregaBudgetOrcado: Erros='|| g_count_erro;
    fnd_file.put_line(fnd_file.log,l_msg);
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);


EXCEPTION
    when others then
        BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT','BRA_HR_ORC_WF_3.carregaBudgetOrcado: '||SQLERRM);

END carregaBudgetOrcado;



--------------------------------------------------------------------------
-- ***** Procedure  concurrent *****   ----
--------------------------------------------------------------------------
procedure concurrent
 (  errbuff    out    varchar2
 ,  retcode    out    number
 ,  p_budget_id in    number  )
is
    l_msg                   varchar2(2000);
    l_budget_name           BRA_BUDGET_HEADER.budget_name%type;
    e_budget_not_found      exception;

BEGIN

    l_msg := 'BRA_HR_ORC_WF_3.concurrent: Iniciando importa��o do or�amento '||l_budget_name||'.';
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);
    fnd_file.put_line(fnd_file.log,l_msg);

    begin
        select distinct budget_name
        into l_budget_name
        from BRA_BUDGET_HEADER
        where id_budget = p_budget_id;
    exception
        when others then
            raise e_budget_not_found;
    end;

    carregaBudgetAtual(p_budget_id,l_budget_name);
    carregaBudgetOrcado(p_budget_id,l_budget_name);

    -- Retirando os valores nulos das colunas.
    update BRA_HR_ORC_RELATORIO set quant_real = nvl(quant_real,0), quant_orcado = nvl(quant_orcado,0);
    commit;

    l_msg := 'BRA_HR_ORC_WF_3.concurrent: Importa��o de or�amento finalizada.';
    BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);
    fnd_file.put_line(fnd_file.log,l_msg);

EXCEPTION
    when e_budget_not_found THEN
        l_msg := 'BRA_HR_ORC_WF_3.concurrent: ERRO - Budget_id('||p_budget_id||') n�o encontrado na tabela de header de or�amento (BRA_BUDGET_HEADER).';
        fnd_file.put_line(fnd_file.log,l_msg);
        BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT',l_msg);
        rollback;

    when others THEN
        fnd_file.put_line(fnd_file.log,'ERRO - '||sqlerrm);
        BRA_HR_WF_TOOLS.add_log_line(g_itemtype,'CONCURRENT','BRA_HR_ORC_WF_3.concurrent: '||SQLERRM);
        rollback;

END concurrent;


-------------------------------------------------------------------------
-------------------- Procedure  limpaPosicaoVazia ---------------------------
-------------------------------------------------------------------------
function getStatus (p_budget_id in number,p_solicitante_id in number) return varchar2 is

    l_status BRA_BUDGET_STATUS.tx_status%type := 'Em branco';

BEGIN

    select bbs.tx_status
    into   l_status
    from   BRA_BUDGET_HEADER bbh
    ,      BRA_BUDGET_STATUS bbs
    where  bbh.id_solicitante = p_solicitante_id
    and    bbh.id_budget      = p_budget_id
    and    bbs.id_status      = bbh.id_status;

    return l_status;

EXCEPTION
    when others then
        return l_status;

END;

-------------------------------------------------------------------------
-------------------- Procedure  limpaPosicaoVazia ---------------------------
-------------------------------------------------------------------------
procedure limpaPosicaoVazia (p_solicitante_id in number, p_budget_id in number)is

    cursor c_posicao_removida is
        select b.id_budget,b.id_solicitante,b.id_budget_line
        from BRA_POSICAO_REMOVIDA_V a
        ,    BRA_BUDGET_LINES   b
        where a.id_solicitante = b.id_solicitante
        and   a.id_budget      = b.id_budget
        and   a.position_id    = b.position_id
        and   a.id_solicitante = p_solicitante_id
        and   a.id_budget      = p_budget_id;

BEGIN

    for r1 in c_posicao_removida loop

        delete from BRA_BUDGET_EMPLOYEES b
        where b.id_solicitante = r1.id_solicitante
        and   b.id_budget      = r1.id_budget
        and   b.id_budget_line = r1.id_budget_line;

        delete from BRA_BUDGET_LINES b
        where b.id_solicitante = r1.id_solicitante
        and   b.id_budget      = r1.id_budget
        and   b.id_budget_line = r1.id_budget_line;

    end loop;

EXCEPTION
    when others then
        raise;

END;


PROCEDURE notifica_corte(
  p_solicitante_id IN NUMBER
 ,p_budget_id IN NUMBER
 ,p_owner_id IN NUMBER)
IS

  l_itemtype VARCHAR2(8):= 'BRAORC';
  l_itemkey  VARCHAR2(50);
  l_process  VARCHAR2(20) := 'BRA_ORCAMENTO_APROV';

  l_sequence NUMBER;
  l_owner    VARCHAR2(200);

  l_owner_id NUMBER;

  BEGIN
    SELECT bra_hr_orc_s.nextval
      INTO l_sequence from dual;

    l_itemkey:=  l_sequence;

    -- FORCANDO QUE O OWNER_ID SEJA USER_ID NA FND_USER.
    BEGIN
      SELECT bcev.user_id
        INTO l_owner_id
        FROM bra_customhr_employees_v bcev
      WHERE bcev.employee_person_id = p_owner_id;

    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

    wf_engine.createprocess(l_itemtype, l_itemkey, l_process);

    wf_engine.setItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_ID_SOLICITANTE', p_solicitante_id);
    wf_engine.setItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_BUDGET_ID', p_budget_id);
    wf_engine.setItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_OWNER', l_owner_id);

    BEGIN
      SELECT user_name
        INTO l_owner
        FROM apps.fnd_user a
      WHERE a.user_id = l_owner_id
      AND rownum = 1;

    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

    UPDATE bra.bra_customhr_move_proposals
      SET itemkey_corte = l_itemkey
      WHERE id = p_budget_id;

    COMMIT;

    wf_engine.setItemOwner (l_itemtype,l_itemkey,l_owner);
    wf_engine.startprocess (l_itemtype,l_itemkey);

END;

PROCEDURE cria_workflow(
  p_solicitante_id IN NUMBER
 ,p_budget_id IN NUMBER
 ,p_owner_id IN NUMBER)
IS

  l_itemtype VARCHAR2(8):= 'BRAORC';
  l_itemkey  VARCHAR2(50);
  l_process  VARCHAR2(20) := 'BRA_ORCAMENTO';

  l_sequence NUMBER;
  l_owner    VARCHAR2(200);
  l_owner_id NUMBER;

  BEGIN

    -- FORCANDO QUE O OWNER_ID SEJA USER_ID NA FND_USER.
    BEGIN
      SELECT bcev.user_id
        INTO l_owner_id
        FROM bra_customhr_employees_v bcev
      WHERE bcev.employee_person_id = p_owner_id;

    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

    SELECT bra_hr_orc_s.nextval
      INTO l_sequence from dual;

    l_itemkey:=  l_sequence;

    wf_engine.createprocess(l_itemtype, l_itemkey, l_process);

    wf_engine.setItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_ID_SOLICITANTE', p_solicitante_id);
    wf_engine.setItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_BUDGET_ID', p_budget_id);
    wf_engine.setItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_OWNER', l_owner_id);

    BEGIN
      SELECT user_name
        INTO l_owner
        FROM apps.fnd_user a
      WHERE a.user_id = l_owner_id
      AND rownum = 1;

    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

    UPDATE bra.bra_customhr_move_proposals
      SET itemkey = l_itemkey
      WHERE id = p_budget_id;

    COMMIT;

    wf_engine.setItemOwner (l_itemtype,l_itemkey,l_owner);
    wf_engine.startprocess (l_itemtype,l_itemkey);

END;



-------------------------------------------------------------------------
  -------------------- Procedure  ENCERRAR_WORKFLOW ---------------------------
  -------------------------------------------------------------------------
 procedure ENCERRAR_WORKFLOW (p_solicitante_id in number, p_budget_id in number , p_owner_id in number, p_motivo in varchar2 )is

   l_itemtype varchar2(8):= 'BRAORC';
   l_itemkey varchar2(50);
   l_process varchar2(20) := 'BRA_ORC_ANALISTA_RH';

   l_sequence number;
   l_owner varchar2(200);

   Begin

     select bra_hr_orc_s.nextval into l_sequence from dual;
     l_itemkey:=  l_sequence;

     wf_engine.createprocess(l_itemtype,l_itemkey,l_process);

     wf_engine.setItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_ID_SOLICITANTE', p_solicitante_id);
     wf_engine.setItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_BUDGET_ID', p_budget_id);
     wf_engine.setItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_OWNER', p_owner_id);
     wf_engine.setitemattrtext(l_itemtype, l_itemkey, 'BRA_MOTIVO_REJEICAO', p_motivo);

     begin

      select  user_name
        into  l_owner
        from  apps.fnd_user a
       where  a.user_id = p_owner_id
         and  rownum = 1;

     exception
        when others then
            null;
     end;

     wf_engine.setItemOwner (l_itemtype,l_itemkey,l_owner);
     wf_engine.startprocess (l_itemtype,l_itemkey);

   END;

  ----------------------------------------------------------------------------
  --------------------------- FUNCTION GET_ROLE ------------------------------
  ----------------------------------------------------------------------------
  Function get_role (p_employee_id in number) return varchar2 is
    l_role wf_users.name%type ;
  Begin
    begin
      select name
      into l_role
      from wf_users
      where orig_system_id = p_employee_id
      and  orig_system  =  'PER'
      and rownum < 2 ;
    exception
      when others then
        null;
    end;
    return l_role;
  end;

PROCEDURE verifica_planejamento(itemtype     in     varchar2
                               ,itemkey      in     varchar2
                               ,actid        in     number
                               ,funcmode     in     varchar2
                               ,resultout    in out varchar2)
IS

  l_budget_id       number;
  l_id_solicitante  number;
  l_solicitante     varchar2(500);
  l_director_id     number;
  l_director        varchar2(500);
  l_budget          varchar2(500);
  l_doc             varchar2(100) := 'PLSQLCLOB:BRA_HR_ORC_WF_3.SET_DOC_CORTE/';

BEGIN
  IF (funcmode <> wf_engine.eng_run) THEN
    resultout := wf_engine.eng_null;
    RETURN;
  END IF;

  l_budget_id      := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_BUDGET_ID');
  l_id_solicitante := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_ID_SOLICITANTE');

  SELECT bcmp2.calendar_name
        ,bcmp.director_id
    INTO l_budget
        ,l_director_id
    FROM bra.bra_customhr_move_proposals bcmp
        ,bra.bra_customhr_move_plans bcmp2
    WHERE bcmp.planning_id = bcmp2.id
    AND bcmp.id = l_budget_id;

  wf_engine.setItemAttrText(itemtype, itemkey, 'BRA_ORC_BUDGET', l_budget);

  SELECT bccv.user_name
    INTO l_director
    FROM bra_customhr_collaborators_v bccv
    WHERE bccv.employee_person_id = l_director_id;

  wf_engine.setItemAttrText(itemtype, itemkey, 'BRA_ORC_DIRETOR', l_director);

  SELECT bccv.user_name
    INTO l_director
    FROM bra_customhr_collaborators_v bccv
    WHERE bccv.employee_person_id = l_director_id;

  wf_engine.setItemAttrText(itemtype, itemkey, 'BRA_ORC_SOLICITANTE', l_solicitante);

  wf_engine.setitemattrtext(itemtype,itemkey,'BRA_ORC_DOC', l_doc||itemtype||':'||itemkey);

  resultout := 'COMPLETE:';

  EXCEPTION
    WHEN OTHERS THEN
      wf_core.context('BRA_HR_ORC_WF', 'VERIFICA_PLANEJAMENTO', itemtype, itemkey, actid, funcmode);
      RAISE;

END verifica_planejamento;

  procedure carrega_atributos  (itemtype     in     varchar2
                               ,itemkey      in     varchar2
                               ,actid        in     number
                               ,funcmode     in     varchar2
                               ,resultout    in out varchar2)
  is

    l_budget_id       number;
    l_budget_line_id  number;
    l_id_solicitante  number;
    l_solicitante     varchar2(500);
    l_reg             bra_hr_historico_wf%rowtype;
    l_owner_role      varchar2(200);
    l_budget          varchar2(200);
    l_calendar        varchar2(200);
    l_ini_per         varchar2(200);
    l_fim_per         varchar2(200);
    l_creation_date   varchar2(200);
    l_posicao         varchar2(200);
    l_cargo           varchar2(200);
    l_uniorg          varchar2(200);
    l_efetivo_atual   number;
    l_efetivo_proximo number;
    l_ini_fisc        varchar2(200);
    l_fim_fisc        varchar2(200);
    l_budget_line     number;
    l_sol             NUMBER;
    l_nome            VARCHAR2(4000);
    l_doc             varchar2(100) := 'PLSQLCLOB:BRA_HR_ORC_WF_3.SET_DOC_APROVADORES/';

  begin

    if (funcmode <> wf_engine.eng_run) then
      resultout := wf_engine.eng_null;
      return;
    end if;

    l_id_solicitante := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_ID_SOLICITANTE');
    l_budget_id      := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_BUDGET_ID');

    -- CAPTURANDO INFORMA��ES DO SOLICITANTE.
    BEGIN
      SELECT bcev.employee_number
            ,bcev.user_name
        INTO l_sol
            ,l_solicitante
        FROM bra_customhr_employees_v bcev
        WHERE bcev.employee_person_id = l_id_solicitante;

      SELECT owner_role
        INTO l_owner_role
        FROM wf_items
        WHERE item_type = itemtype
        AND item_key    = itemkey;

    EXCEPTION
      WHEN OTHERS THEN
        wf_core.context('BRA_HR_ORC_WF', 'CARREGA_ATRIBUTOS', itemtype, itemkey, actid, funcmode);
        RAISE;
    END;

    wf_engine.setItemAttrNumber(itemtype, itemkey, 'BRA_ORC_SOL', l_sol);


    -- CAPTURANDO O HEADCOUNT ATUAL E PR�XIMO.
    BEGIN
      SELECT plan.calendar_name
        INTO l_budget
        FROM bra.bra_customhr_move_plans  plan
       INNER JOIN bra.bra_customhr_move_proposals prop ON prop.planning_id = plan.id
       WHERE prop.id = l_budget_id;


      SELECT NVL(SUM(bcmr.actual), 0)
            ,NVL(SUM(bcmr.planned), 0)
        INTO l_efetivo_atual
            ,l_efetivo_proximo
        FROM bra.bra_customhr_move_resumes bcmr
        WHERE bcmr.proposal_id = l_budget_id;
    EXCEPTION
      WHEN OTHERS THEN
        l_efetivo_atual   := 0;
        l_efetivo_proximo := 0;
        NULL;
    END;

    wf_engine.setItemAttrText(itemtype, itemkey, 'BRA_ORC_SOLICITANTE', l_solicitante);
    wf_engine.setItemAttrText(itemtype, itemkey, 'ROLE_FROM', l_owner_role);
    wf_engine.setItemAttrText(itemtype, itemkey, 'BRA_ORC_BUDGET', l_budget);
    wf_engine.setItemAttrNumber(itemtype, itemkey, 'BRA_ORC_TOTAL_ORCADO', l_efetivo_proximo);
    wf_engine.setItemAttrNumber(itemtype, itemkey, 'BRA_ORC_TOTAL_ATUAL', l_efetivo_atual);
    wf_engine.setitemattrtext(itemtype,itemkey,'BRA_ORC_DOC',l_doc||itemtype||':'||itemkey);

    resultout:='COMPLETE:';

  exception
    when others then
      wf_core.context('BRA_HR_ORC_WF', 'CARREGA_ATRIBUTOS',itemtype,itemkey,actid,funcmode);
      raise;

  end;

  -------------------------------------------------------------------------
  ------------------ Procedure Identifica Filial --------------------------
  -------------------------------------------------------------------------
  PROCEDURE identifica_filial       (itemtype     in     varchar2
                                    ,itemkey      in     varchar2
                                    ,actid        in     number
                                    ,funcmode     in     varchar2
                                    ,resultout    in out varchar2) IS

  l_person_id number ;
  l_filial varchar2(100);
 begin
   if (funcmode = 'RUN') then
     l_person_id := wf_engine.GetItemAttrNumber(itemtype,itemkey,'BRA_ORC_ID_SOLICITANTE');

     begin
     select distinct nvl (decode (hu.attribute1,'1','MACAE','2','RIO','3','VITORIA','4','ANGRA'),'ERRO')
     into l_filial
     from  PER_ALL_ASSIGNMENTS_F  paa
          ,hr_all_organization_units hu
     WHERE paa.PERSON_ID = l_person_id
     and   paa.effective_end_date >= sysdate
     and   paa.effective_start_date <= sysdate
     and   paa.organization_id = hu.organization_id;
     Exception
       When others then
         l_filial := 'ERRO';
     end;

     if l_filial = 'RIO' OR l_filial = 'MACAE' OR l_filial = 'ANGRA' then
       wf_engine.SetItemAttrText(itemtype,itemkey,'BRA_ORC_FILIAL',l_filial);
       resultout:='COMPLETE:'||'BRA_ORC_RIO_MACAE';
     elsif l_filial = 'VITORIA' then
       wf_engine.SetItemAttrText(itemtype,itemkey,'BRA_ORC_FILIAL',l_filial);
       resultout:='COMPLETE:'||'BRA_ORC_VIT';
     elsif l_filial = 'ERRO' then
       resultout:='COMPLETE:'||'ERRO';
     end if;

   end if;
   if (funcmode = 'CANCEL') then
    resultout := 'COMPLETE:';
    return;
   end if;
 exception
   when others then
     wf_core.context('BRA_HR_ORC_WF', 'IDENT_FILIAL',itemtype,itemkey,actid,funcmode);
     raise;

  end  identifica_filial;

  --------------------------------------------------------------------------
  ------------------------ PROCEDURE Get_Diretor ---------------------------
  --------------------------------------------------------------------------
  PROCEDURE get_diretor              (itemtype     in     varchar2
                                     ,itemkey      in     varchar2
                                     ,actid        in     number
                                     ,funcmode     in     varchar2
                                     ,resultout    in out varchar2) IS

    l_role_dir_dep          wf_users.name%type ;
    l_employee_id           number;
    l_temp_dir_departamento number;
    l_fnd_itemkey           varchar2(100);
    l_responsibility_id     number ;
    l_organization_id       number ;
    l_versao_id             number;
    l_organization_pai      number;
    l_solicitante           number;
    l_role_name             varchar2(500) := 'DIRETOR_DEPARTAMENTO';
    l_manager_org           number;
    l_salario               varchar2(200);
    l_ultimo varchar2(100);
    l_supervisor number ;
    dir_hr boolean := false;
    l_get_dir_error exception;
    l_filial varchar2(100);
  BEGIN
    if (funcmode <> wf_engine.eng_run) then
      resultout := wf_engine.eng_null;
      return;
    end if;

    l_solicitante := wf_engine.GetItemAttrText(itemtype,itemkey,'BRA_ORC_ID_SOLICITANTE');
    l_filial:= wf_engine.GetItemAttrText(itemtype,itemkey,'BRA_ORC_FILIAL');
    bra_hr_wf_util_2.diretoria(l_solicitante,flag_diretoria,flag_dir_rh,flag_dir_geral);
    if (flag_diretoria) or (flag_dir_rh) or (flag_dir_geral)  THEN
      resultout:='COMPLETE:N';
    else
      l_supervisor:= bra_hr_wf_util_2.get_supervisor(l_solicitante); --- Lcastro 11/07 recupar o supervisor
      l_organization_id := bra_hr_wf_util_2.get_org_id(l_supervisor); ---Lcastro 11/07 recupar org do supervisor
      l_organization_pai := bra_hr_wf_util_2.get_org_pai(l_organization_id, l_supervisor);  ---- J� PEGUEI A ORGANIZA��O PAI
      if l_organization_pai = -1 then
        raise l_get_dir_error;
      Else
        l_manager_org := bra_hr_wf_util_2.get_manager_org(l_organization_pai);
        if l_manager_org = -1 then  --- n�o existe manager_flag (criar a role)
          bra_hr_wf_util_2.create_role (l_role_name ,l_organization_pai);
          l_role_dir_dep := l_role_name;
        else
          l_role_dir_dep := bra_hr_wf_util_2.get_role (l_manager_org);
        end if;
        wf_engine.SetItemAttrText(itemtype,itemkey,'BRA_ORC_DIRETOR',l_role_dir_dep);
        wf_engine.setItemAttrText(itemtype, itemkey, 'BRA_ORC_ROLE_FROM'   , l_role_dir_dep);

        l_temp_dir_departamento:= bra_hr_wf_util_2.get_timeout ('BRA_HR_ORC_TEMP_DIR',l_filial);
        wf_engine.SetItemAttrText(itemtype,itemkey,'BRA_ORC_TEMPO_DIRETOR',l_temp_dir_departamento);

        resultout:='COMPLETE:Y';

      end if;
    end if;
  exception
    when  l_get_dir_error then  --- Modifica��o para atender a Diretoria Geral
      resultout:='COMPLETE:N';
      raise;
    when others then
      wf_core.context('BRA_HR_ORC_WF','GET_DIRETOR',itemtype,itemkey,actid,funcmode);
      resultout:='COMPLETE:ERRO';
      raise;

  end  get_diretor;

  --------------------------------------------------------------------------
  -------------------------- PROCEDURE  Get_RH -----------------------------
  --------------------------------------------------------------------------
  PROCEDURE get_rh
  (   itemtype     in     varchar2
  ,   itemkey      in     varchar2
  ,   actid        in     number
  ,   funcmode     in     varchar2
  ,   resultout    in out varchar2)
  IS
    l_role_adm_pessoal    wf_users.name%type;
    l_manager_org         number;
    l_role_name           varchar2(500);
    l_role_display_name   varchar2(100);
    l_filial              varchar2(200);
    l_org_type            varchar2(200);

  BEGIN
    IF (funcmode != wf_engine.eng_run) THEN
      resultout := wf_engine.eng_null;
      return;
    END IF;

      l_filial:= wf_engine.GetItemAttrText(itemtype,itemkey,'BRA_ORC_FILIAL');
      bra_hr_wf_util_2.get_cs_type_filial(l_filial,l_org_type, l_role_name);
      if l_filial != 'VITORIA' then
        l_org_type := 'ADM ORCAMENTO';
      END IF;
      l_role_adm_pessoal := bra_hr_wf_util_2.create_role_org(l_role_name, l_org_type);
      wf_engine.SetItemAttrText(itemtype,itemkey,'BRA_ORC_RH',l_role_adm_pessoal);
       wf_engine.setItemAttrText(itemtype, itemkey, 'BRA_ORC_ROLE_FROM'   , l_role_adm_pessoal);

      resultout:='COMPLETE:';

  EXCEPTION
    WHEN others THEN
      wf_core.context('BRA_HR_ORC_WF', 'GET_RH',itemtype,itemkey,actid,funcmode);
  END  get_rh;

  --------------------------------------------------------------------------
  ------------------ Procedure Get_Superintendnete -------------------------
  --------------------------------------------------------------------------
  procedure  get_super            (itemtype     in     varchar2
                                  ,itemkey      in      varchar2
                                  ,actid        in     number
                                  ,funcmode     in     varchar2
                                  ,resultout    in out varchar2) IS

  l_role_superintendente  wf_users.name%type ;
  l_employee_id           number;
  l_temp_superintendente  number;
  l_fnd_itemkey           varchar2(100);
  l_responsibility_id     number ;
  l_organization_id       number ;
  l_organization_pai      number;
  l_solicitante           number;
  l_role_name             varchar2(500) := 'SUPERINTEDENTE';
  l_manager_org           number;
  l_salario               varchar2(200);
  l_ultimo varchar2(100);
  l_supervisor number ;
  dir_hr boolean := false ;
  l_tipo_req varchar2(200);
  l_filial varchar2(100);
  l_gerente_aprov varchar2(1) ;

  BEGIN
    if (funcmode <> wf_engine.eng_run) then
      resultout := wf_engine.eng_null;
      return;
    end if;

    l_solicitante := wf_engine.GetItemAttrText(itemtype,itemkey,'BRA_ORC_ID_SOLICITANTE');
    l_tipo_req := bra_hr_wf_util_2.tipo_requisitante (l_solicitante);
    l_filial:= wf_engine.GetItemAttrText(itemtype,itemkey,'BRA_ORC_FILIAL');
    ----- Implementar a Fun��o -- Mesma regra do Diretor de Departamento .... s� que pega o gerente.
    if (l_tipo_req = 'DIRETORIA') or (l_tipo_req = 'DIRETORIA_RH') or (l_gerente_aprov = 'S') then
      resultout:='COMPLETE:N';
    else
      l_supervisor:= bra_hr_wf_util_2.get_supervisor(l_solicitante);
      l_organization_id := bra_hr_wf_util_2.get_org_id(l_supervisor);
      l_organization_pai := bra_hr_wf_util_2.get_org_superintendente(l_organization_id,l_supervisor);
      if l_organization_pai = -1 then
        resultout:='COMPLETE:N';
      else
        l_manager_org := bra_hr_wf_util_2.get_manager_org(l_organization_pai);
        IF l_manager_org = -1 THEN  --- n�o existe manager_flag (criar a role)
          bra_hr_wf_util_2.create_role (l_role_name,l_organization_pai);
          l_role_superintendente := l_role_name;
        else
          l_role_superintendente := bra_hr_wf_util_2.get_role (l_manager_org);
        end if;
          wf_engine.SetItemAttrText(itemtype,itemkey,'BRA_ORC_SUPER',l_role_superintendente);
          wf_engine.setItemAttrText(itemtype, itemkey, 'BRA_ORC_ROLE_FROM'   , l_role_superintendente);


          l_temp_superintendente := bra_hr_wf_util_2.get_timeout ('BRA_HR_ORC_TEMP_SUPER',l_filial);
          wf_engine.SetItemAttrNumber(itemtype,itemkey,'BRA_ORC_TEMPO_SUPER',l_temp_superintendente);
          resultout:='COMPLETE:Y';
      end if;
    end if;
  exception
    when others then
      wf_core.context('BRA_HR_ORC_WF','GET_SUPER',itemtype,itemkey,actid,funcmode);
      resultout := 'ERRO';
      raise;
  end  get_super;


PROCEDURE set_doc_corte(
  document_id   IN VARCHAR2
 ,display_type  IN VARCHAR2
 ,document      IN OUT NOCOPY CLOB
 ,document_type IN OUT NOCOPY VARCHAR2)
IS

  l_itemkey         VARCHAR2(100) ;
  l_colon           NUMBER;
  l_colon2          NUMBER;
  l_itemtype        VARCHAR2(8):= 'BRAORC';
  l_solicitante_id  NUMBER;
  l_solicitante     VARCHAR2(2000);
  l_diretor_id      NUMBER;

  l_budget          VARCHAR2(2000);
  l_budget_id       NUMBER;
  l_planning_id     VARCHAR2(2000);

BEGIN
  l_colon   := instrb(document_id, ':');
  l_colon2  := instrb(document_id, ':', l_colon + 1);
  l_itemkey := substrb(document_id, l_colon + 1);

  l_budget_id      := wf_engine.getItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_BUDGET_ID');
  l_solicitante_id := wf_engine.getItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_ID_SOLICITANTE');

  SELECT bccv.full_name
    INTO l_solicitante
    FROM bra_customhr_collaborators_v bccv
    WHERE employee_person_id = l_solicitante_id;

  SELECT bcmp.planning_id
    INTO l_planning_id
    FROM bra.bra_customhr_move_proposals bcmp
    WHERE bcmp.id = l_budget_id;

  SELECT bcmp.calendar_name
    INTO l_budget
    FROM bra.bra_customhr_move_plans bcmp
    WHERE bcmp.id = l_planning_id;

  document := '</br>';
  document := document || 'Prezado,<br/><br/>';
  document := document || 'O planejamento de or�amento do gestor <strong>' || l_solicitante || '</strong> para o calend�rio <strong>' || l_budget || '</strong> foi alterado pelo RH. <br /><br />';
  document := document || 'Para mais detalhes, acesse o sistema de planejamento de or�amento. <br />';

end set_doc_corte;


  ---------------------------------------------------------------------------------
  -------------------- PROCEDURE  Set_Doc_Aprovadores  ----------------------------
  ---------------------------------------------------------------------------------
  PROCEDURE set_doc_aprovadores (document_id   in varchar2,
                                dISplay_type  in varchar2,
                                         document         in out nocopy clob,
                                        document_type in out nocopy varchar2) IS
  l_itemkey                 varchar2(100) ;
  l_colon                   number;
  l_colon2                  number;
  l_itemtype                varchar2(8):= 'BRAORC';
  l_solicitante_id          number;
  l_solicitante             varchar2(2000);
  l_budget                  varchar2(2000);
  l_motivo             varchar2(2000);
  l_total_orcado            number;
  l_total_atual             number;
  l_id_solicitante          number;
  l_budget_id               number;
  l_budget_line             number;
  l_motivo_contratacao      varchar2(2000);
  l_motivo_demissao         varchar2(2000);
  l_motivo_promocao         varchar2(2000);
  l_comentarios             varchar2(4000);
  l_comentarios2            varchar2(4000);

  l_owner_id                NUMBER;
  l_owner                   VARCHAR2(2000);

  CURSOR c_col IS
    SELECT pj.job_id
          ,pj.name cargo
          ,bcmr.actual headcount_atual
          ,bcmr.planned headcount_orcado
      FROM bra.bra_customhr_move_resumes bcmr
          ,per_jobs pj
      WHERE bcmr.job_id = pj.job_id
      AND pj.date_to IS NULL
      AND pj.business_group_id = 511
      AND bcmr.proposal_id     = l_budget_id
      ORDER BY pj.name;

  BEGIN
    l_colon     := instrb(document_id, ':');
    l_colon2    := instrb(document_id, ':', l_colon+1);
    l_itemkey   := substrb(document_id, l_colon +1);

    l_budget             := wf_engine.getItemAttrText(l_itemtype, l_itemkey, 'BRA_ORC_BUDGET');
    l_total_orcado       := wf_engine.getItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_TOTAL_ORCADO');
    l_total_atual        := wf_engine.getItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_TOTAL_ATUAL');
    l_id_solicitante     := wf_engine.getItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_SOL');
    l_budget_id          := wf_engine.getItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_BUDGET_ID');
    l_solicitante_id     := wf_engine.getItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_ID_SOLICITANTE');
    l_motivo             := wf_engine.getItemAttrText(l_itemtype, l_itemkey, 'BRA_MOTIVO_REJEICAO');
    l_owner_id           := wf_engine.getItemAttrNumber(l_itemtype, l_itemkey, 'BRA_ORC_OWNER');

    begin
     select display_name into l_solicitante    from wf_users  where orig_system_id= l_solicitante_id;
    exception
      when others then
        null;
    end;

    BEGIN
      SELECT full_name
        INTO l_owner
        FROM bra_customhr_employees_v
        WHERE user_id = l_owner_id;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

    ----------------------------------------------------------------------------------------------------------
    document := '</br>';
    document := document || '<table border=0 cellpadding=2>';
    document := document || '<tr bgcolor="#CCCC99">';
    document := document || '<td align="left"><font color="#336699"><b>Atributos da Notifica��o</b></font></td>';
    document := document || '<td align="left"><font color="#336699"><b>Valores </b></font></td>';
    document := document || '</tr>';
    document := document || '<tr>';
    document := document || '<td align="left" bgcolor="#F7F7E7" style="bold">'||'Nome do Budget'|| '</td>';
    document := document || '<td align="left" bgcolor="#F7F7E7">'||l_budget|| '</td>';
    document := document || '</tr>';
    document := document || '<tr>';
    document := document || '<td align="left" bgcolor="#F7F7E7" style="bold">'||'Solicitante'|| '</td>';
    document := document || '<td align="left" bgcolor="#F7F7E7">'||l_owner|| '</td>';
    document := document || '</tr>';
    document := document || '<tr>';
    document := document || '<td align="left" bgcolor="#F7F7E7" style="bold">'||'Gestor'|| '</td>';
    document := document || '<td align="left" bgcolor="#F7F7E7">'||l_solicitante|| '</td>';
    document := document || '</tr>';
    document := document || '<tr>';
    document := document || '<td align="left" bgcolor="#F7F7E7" style="bold">'||'Total do efetivo atual'|| '</td>';
    document := document || '<td align="left" bgcolor="#F7F7E7">'||To_Char(l_total_atual)|| '</td>';
    document := document || '</tr>';
    document := document || '<tr>';
    document := document || '<td align="left" bgcolor="#F7F7E7" style="bold">'||'Total do efetivo or�ado'|| '</td>';
    document := document || '<td align="left" bgcolor="#F7F7E7">'||To_Char(l_total_orcado)|| '</td>';
    document := document || '</tr>';
    if l_motivo is not null then
        document := document || '<tr>';
        document := document || '<td align="left" bgcolor="#F7F7E7" style="bold">'||'Motivo'|| '</td>';
        document := document || '<td align="left" bgcolor="#F7F7E7">'||l_motivo|| '</td>';
        document := document || '</tr>';
    end if;
    document := document || '</table>';

    document := document || '<br>';
    document := document || '<table border=0 cellpadding=2 width=100%>';
    document := document || '<tr>';
    document := document || '<td align="left" bgcolor="#CCCC99"><font color="#336699"><b>'||'Cargo'    || '</b></font></td>';
    document := document || '<td align="left" bgcolor="#CCCC99"><font color="#336699"><b>'||'Qte. Atual' || '</b></font></td>';
    document := document || '<td align="left" bgcolor="#CCCC99"><font color="#336699"><b>'||'Qte. Or�ada'|| '</b></font></td>';
    document := document || '<td align="left" bgcolor="#CCCC99"><font color="#336699"><b>'||'Coment�rios'|| '</b></font></td>';
    document := document || '</tr>';

    for r_col in c_col loop

      l_comentarios := '';

      for r_position in (
                        select distinct hapf.position_id
                              ,hapf.name position_name
                            from bra.bra_customhr_move_positions pos
                                ,hr.hr_all_positions_f hapf
                            where NVL(pos.position_id, pos.previous_position_id) = hapf.position_id
                            and TRUNC(sysdate) BETWEEN hapf.effective_start_date AND hapf.effective_end_date
                            AND hapf.availability_status_id <> 5
                            AND ((hapf.status <> 'INVALID') OR hapf.status IS NULL)
                            AND hapf.business_group_id = 511
                            and pos.proposed_by_hr = 0
                            and pos.uncountable = 0
                            and pos.motive is not null
                            and pos.proposal_id = l_budget_id
                            and hapf.job_id = r_col.job_id) loop

        l_comentarios := l_comentarios || '<b>' || r_position.position_name || '</b><br />';

        select listagg(orc.tipo || ' - ' || DECODE(orc.colaborador, NULL, orc.motivo, orc.colaborador || ' - ' || orc.motivo), '<br />') within group(order by tipo) as comentarios
            into l_comentarios2
            from (
        select hapf.job_id
              ,case pos.iteration
                 when 'FIRED' then 'DESLIGAMENTO'
                 when 'HIRE'  then 'CONTRATA��O'
                 when 'PROMOTION' then 'PROMO��O'
                 when 'MAINTENANCE' THEN 'MANTIDO'
                 else 'N�O IDENTIFICADO'
               end tipo
              ,bccv.full_name colaborador
              ,pos.motive motivo
          from bra.bra_customhr_move_positions pos
              ,apps.bra_customhr_collaborators_v bccv
              ,hr.hr_all_positions_f hapf
          where bccv.employee_person_id (+)= pos.collaborator_id
            and NVL(pos.position_id, pos.previous_position_id) = hapf.position_id
            and TRUNC(sysdate) BETWEEN hapf.effective_start_date AND hapf.effective_end_date
            AND hapf.availability_status_id <> 5
            AND ((hapf.status <> 'INVALID') OR hapf.status IS NULL)
            AND hapf.business_group_id = 511
            and pos.proposed_by_hr = 0
            and pos.uncountable = 0
            and pos.motive is not null
            and pos.proposal_id  = l_budget_id
            and hapf.job_id      = r_col.job_id
            and hapf.position_id = r_position.position_id) orc
          order by orc.tipo;

        l_comentarios := l_comentarios || l_comentarios2 || '<br /><br />';

      end loop;

      document := document || '<tr>';
      document := document || '<td align="left" bgcolor="#F7F7E7">'||r_col.cargo|| '</td>';
      document := document || '<td align="left" bgcolor="#F7F7E7">'||To_Char(r_col.headcount_atual)|| '</td>';
      document := document || '<td align="left" bgcolor="#F7F7E7">'||To_Char(r_col.headcount_orcado)|| '</td>';
      document := document || '<td align="left" bgcolor="#F7F7E7">'||To_Char(l_comentarios)|| '</td>';
      document := document || '</tr>';

    end loop;

    document := document || '</table>';
    document := document || '<br>';

  end set_doc_aprovadores;

  --------------------------------------------------------------------------
  ---------------------- Procedure Commit_Aprovacao ------------------------
  --------------------------------------------------------------------------
  procedure commit_aprovacao
  (   itemtype     in     varchar2
  ,   itemkey      in     varchar2
  ,   actid        in     number
  ,   funcmode     in     varchar2
  ,   resultout    in out varchar2)
  is
    l_budget_line_id  number ;
    l_budget_id       number ;
    l_id_solicitante  number ;
    l_tem             number ;
    l_owner           number ;

  begin
    if (funcmode = 'RUN') then

      l_id_solicitante := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_SOL');
      l_budget_id      := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_BUDGET_ID');
      l_owner          := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_OWNER');
      wf_engine.SetItemAttrText(itemtype,itemkey,'BRA_ORC_STATUS','APROVADO');

      begin
        UPDATE bra.bra_customhr_move_proposals
          SET status = 'PENDING-HR'
          WHERE id = l_budget_id;

        COMMIT;
      exception
        when others then
          null;
      end;


      resultout:='COMPLETE:';
    end if;
    if (funcmode = 'CANCEL') then
      resultout := 'COMPLETE:';
      return;
    end if;
  exception
    when others then
      wf_core.context('BRA_HR_ORC_WF', 'COMMIT_APROVACAO',itemtype,itemkey,actid,funcmode);
  end commit_aprovacao;


  --------------------------------------------------------------------------
  ---------------------- Procedure Commit_Rejeicao -------------------------
  --------------------------------------------------------------------------
  procedure commit_rejeicao
  (   itemtype     in     varchar2
  ,   itemkey      in     varchar2
  ,   actid        in     number
  ,   funcmode     in     varchar2
  ,   resultout    in out varchar2)
  is
    l_budget_line_id  number ;
    l_budget_id       number ;
    l_id_solicitante  number ;
    l_tem             number ;
    l_owner           number ;

  begin
    if (funcmode = 'RUN') then

      l_id_solicitante := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_SOL');
      l_budget_id      := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_BUDGET_ID');
      l_owner          := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_OWNER');

      wf_engine.SetItemAttrText(itemtype,itemkey,'BRA_ORC_STATUS','REJEITADO');

      BEGIN

        UPDATE bra.bra_customhr_move_proposals
          SET status = 'REJECTED-DIRECTOR'
          WHERE id = l_budget_id;

        COMMIT;
      exception
        when others then
          null;
      end;


      resultout:='COMPLETE:';
    end if;
    if (funcmode = 'CANCEL') then
      resultout := 'COMPLETE:';
      return;
    end if;
  exception
    when others then
      wf_core.context('BRA_HR_ORC_WF', 'COMMIT_REJEICAO',itemtype,itemkey,actid,funcmode);

  end  commit_rejeicao;

  -----------------------------------------------------------------------------
  ------------------------ PROCEDURE UPDATE_STATUS ----------------------------
  -----------------------------------------------------------------------------
  procedure update_status       (itemtype     in     varchar2
                                ,itemkey      in     varchar2
                                ,actid        in     number
                                ,funcmode     in     varchar2
                                ,resultout    in out varchar2) is

    l_budget_line_id  number ;
    l_budget_id       number ;
    l_id_solicitante  number ;
    l_owner           number ;
  begin
    if (funcmode = 'RUN') then
      l_id_solicitante := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_SOL');
      l_budget_id      := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_BUDGET_ID');
      l_owner          := wf_engine.getItemAttrNumber(itemtype, itemkey, 'BRA_ORC_OWNER');

      wf_engine.SetItemAttrText(itemtype,itemkey,'BRA_ORC_STATUS','TIMEOUT');

      begin

        UPDATE bra.bra_customhr_move_proposals
          SET status = 'REJECTED-DIRECTOR-TIMEOUT'
          WHERE id = l_budget_id;

        commit;
      exception
        when others then
          null;
      end;

      resultout:='COMPLETE:';
    end if;
    if (funcmode = 'CANCEL') then
      resultout := 'COMPLETE:';
      return;
    end if;
  exception
    when others then
      wf_core.context('BRA_HR_ORC_WF', 'UPDATE_STATUS',itemtype,itemkey,actid,funcmode);


  end update_status;

  procedure atualiza_role_from       (itemtype     in     varchar2
                                ,itemkey      in     varchar2
                                ,actid        in     number
                                ,funcmode     in     varchar2
                                ,resultout    in out varchar2)is
    l_role VARCHAR2(200);
  begin
    if (funcmode = 'RUN') THEN
      l_role := wf_engine.getItemAttrText(itemtype, itemkey, 'BRA_ORC_ROLE_FROM');
      wf_engine.SetItemAttrText(itemtype,itemkey,'ROLE_FROM',l_role );
      resultout := 'COMPLETE:';
    end if;
    if (funcmode = 'CANCEL') then
      resultout := 'COMPLETE:';
      return;
    end if;

  end;


end;
/
