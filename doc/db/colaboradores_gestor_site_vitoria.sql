  SELECT bccv.*
    FROM bra_customhr_collaborators_v bccv
   INNER join hr_all_organization_units haou ON UPPER(haou.name) = UPPER(bccv.dept_name)
   WHERE haou.attribute1 = '3'
   START WITH bccv.manager_person_id = 3081 -- employee_person_id do gestor do site vitoria
 CONNECT BY PRIOR bccv.employee_person_id = bccv.manager_person_id
     AND PRIOR haou.type not in (8, 23, 12, 15, 19, 24, 16)