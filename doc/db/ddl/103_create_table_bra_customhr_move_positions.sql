CREATE TABLE bra.bra_customhr_move_positions (
  id                   NUMBER(15,0)                NOT NULL,
  proposal_id          NUMBER(15,0)                NOT NULL,
  previous_position_id NUMBER(38,0)                NULL,
  position_id          NUMBER(38,0)                NULL,
  collaborator_id      NUMBER(15,0)                NULL,
  begin_date           DATE                        NULL,
  end_date             DATE                        NULL,
  iteration            VARCHAR2(20)                NULL,
  quantity             NUMBER(5,0)                 DEFAULT 1 NULL,
  motive               VARCHAR2(100)               NULL,
  quantity_approved    NUMBER(15,0)                DEFAULT null NULL,
  proposed_by_hr       NUMBER(1,0)                 DEFAULT 0 NULL,
  created_at           TIMESTAMP(6) WITH TIME ZONE NULL,
  updated_at           TIMESTAMP(6) WITH TIME ZONE NULL
)
/

ALTER TABLE bra.bra_customhr_move_positions
  ADD CONSTRAINT sys_c00241588 PRIMARY KEY (
    id
  )
/

ALTER TABLE bra.bra_customhr_move_positions
  ADD CONSTRAINT fk_prpsldtls_prpsl FOREIGN KEY (
    proposal_id
  ) REFERENCES bra_customhr_move_proposals (
    id
  ) ON DELETE CASCADE
/

CREATE SEQUENCE bra.bra_customhr_move_positions_s
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  CACHE 20
/

