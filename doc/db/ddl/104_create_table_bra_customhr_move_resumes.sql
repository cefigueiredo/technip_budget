CREATE TABLE bra.bra_customhr_move_resumes (
  id                 NUMBER(15,0) NOT NULL,
  proposal_id        NUMBER(15,0) NULL,
  planning_id        NUMBER(15,0) NULL,
  manager_person_id  NUMBER(15,0) NULL,
  job_id             NUMBER(15,0) NULL,
  actual             NUMBER(15,0) NULL,
  planned            NUMBER(15,0) NULL,
  created_at         DATE         NULL,
  updated_at         DATE         NULL,
  created_by         NUMBER(15,0) NULL,
  director_person_id NUMBER(15,0) NULL,
  approved           NUMBER(15,0) DEFAULT 0 NULL
)
/

ALTER TABLE bra.bra_customhr_move_resumes
  ADD CONSTRAINT PRIMARY KEY (
    id
  )
/

CREATE SEQUENCE bra.bra_customhr_move_resumes_s
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  NOCACHE
/
