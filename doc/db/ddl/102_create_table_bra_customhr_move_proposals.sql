CREATE TABLE bra.bra_customhr_move_proposals (
  id                 NUMBER(15,0)                NOT NULL,
  planning_id        NUMBER(15,0)                NOT NULL,
  manager_id         NUMBER(15,0)                NOT NULL,
  status             VARCHAR2(20)                NOT NULL,
  parent_proposal_id NUMBER(15,0)                NULL,
  created_by         NUMBER(15,0)                NOT NULL,
  created_at         TIMESTAMP(6) WITH TIME ZONE NULL,
  updated_at         TIMESTAMP(6) WITH TIME ZONE NULL,
  itemkey            VARCHAR2(255)               NULL,
  director_id        NUMBER(15,0)                NULL,
  approved_by_hr     NUMBER(1,0)                 DEFAULT 0 NULL,
  itemkey_corte      VARCHAR2(50)                NULL
)
/

ALTER TABLE bra.bra_customhr_move_proposals
  ADD CONSTRAINT PRIMARY KEY (
    id
  )
/

ALTER TABLE bra.bra_customhr_move_proposals
  ADD CONSTRAINT fk_prpsls_plns FOREIGN KEY (
    planning_id
  ) REFERENCES bra_customhr_move_plans (
    id
  )
/

CREATE SEQUENCE bra.bra_customhr_move_proposals_s
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  CACHE 20
/

