CREATE TABLE bra.bra_customhr_move_plans (
  id                  NUMBER(15,0) NOT NULL,
  min_begin_date      DATE         NULL,
  max_end_date        DATE         NULL,
  begin_planning_date DATE         NULL,
  end_planning_date   DATE         NULL,
  calendar_name       VARCHAR2(50) NULL,
  current_planning    NUMBER(1,0)  DEFAULT 0 NULL,
  created_at          DATE         NULL,
  updated_at          DATE         NULL
)
/

ALTER TABLE bra.bra_customhr_move_plans
  ADD PRIMARY KEY (
    id
  )
/

CREATE SEQUENCE bra.bra_customhr_move_plans_s
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  CACHE 20
/