ALTER TABLE bra.bra_customhr_move_positions
  ADD job_id  NUMBER(8)
/
ALTER TABLE bra.bra_customhr_move_positions
  ADD previous_job_id  NUMBER(8)
/
UPDATE bra.bra_customhr_move_positions positions
   set positions.job_id = (
        SELECT job_id
          FROM hr_all_positions_f
         WHERE TRUNC(sysdate) BETWEEN hr_all_positions_f.effective_start_date AND hr_all_positions_f.effective_end_date
           AND hr_all_positions_f.availability_status_id <> 5
           AND ((hr_all_positions_f.status <> 'INVALID') OR hr_all_positions_f.status IS NULL)
           AND hr_all_positions_f.business_group_id = 511
           AND position_id = positions.position_id
       )
/
UPDATE bra.bra_customhr_move_positions positions
   set positions.previous_job_id = (
        SELECT job_id
          FROM hr_all_positions_f
         WHERE TRUNC(sysdate) BETWEEN hr_all_positions_f.effective_start_date AND hr_all_positions_f.effective_end_date
           AND hr_all_positions_f.availability_status_id <> 5
           AND ((hr_all_positions_f.status <> 'INVALID') OR hr_all_positions_f.status IS NULL)
           AND hr_all_positions_f.business_group_id = 511
           AND position_id = positions.previous_position_id
       )
/