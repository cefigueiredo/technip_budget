CREATE OR REPLACE VIEW apps.bra_customhr_managers_v AS
SELECT *
  FROM (
    SELECT bccv.*
          ,(SELECT COUNT(*)
              FROM per_all_assignments_f paaf
              WHERE paaf.assignment_type IN ('E', 'C')
              AND paaf.primary_flag = 'Y'
              AND TRUNC(SYSDATE) BETWEEN TRUNC(paaf.effective_start_date) AND TRUNC(paaf.effective_end_date)
              AND paaf.supervisor_id = bccv.employee_person_id) AS subordinates
      FROM bra_customhr_collaborators_v bccv
    )
  WHERE subordinates > 0
/
