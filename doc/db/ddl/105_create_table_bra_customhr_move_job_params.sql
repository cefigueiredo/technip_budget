CREATE TABLE bra.bra_customhr_move_job_params(
  id                NUMBER(15,0),
  planning_id       NUMBER(15,0),
  job_id            NUMBER(15,0),
  job_name          VARCHAR2(255),
  created_at        DATE,
  updated_at        DATE
)
/

ALTER TABLE bra.bra_customhr_move_job_params
  ADD PRIMARY KEY (
    id
  )
/

CREATE SEQUENCE bra.bra_customhr_move_job_params_s
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  NOCACHE
/
