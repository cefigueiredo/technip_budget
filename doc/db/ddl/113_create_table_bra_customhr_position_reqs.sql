create table bra.bra_customhr_position_reqs (
  id                NUMBER(8) PRIMARY KEY,
  job_id            NUMBER(8),
  organization_id   NUMBER(8),
  people_group_id   NUMBER(8),
  position_name     VARCHAR2(50),
  motive            VARCHAR2(1000),
  requested_by      NUMBER(8),
  created_at        TIMESTAMP,
  updated_at        TIMESTAMP
)
/

CREATE SEQUENCE bra.bra_customhr_position_reqs_s
/
