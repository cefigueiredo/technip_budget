CREATE TABLE bra.bra_customhr_move_org_params (
  id                  number(8) primary key,
  planning_id         number(8),
  organization_id     number(8),
  organization_name   varchar2(50),
  created_at          timestamp,
  updated_at          timestamp
)
/

CREATE SEQUENCE bra.bra_customhr_move_org_params_s
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  CACHE 20
/