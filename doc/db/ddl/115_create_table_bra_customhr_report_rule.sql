create table bra.bra_customhr_move_report_rules as (
  id                numeric(8) primary key,
  planning_id       numeric(8) not null,
  user_id           numeric(8) not null,
  authorized_sites  varchar2(20) not null,
  created_at        timestamp,
  updated_at        timestamp
)
/