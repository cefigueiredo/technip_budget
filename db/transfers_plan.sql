PROMPT =========================================================================
PROMPT TABLE DEFINITIONS
PROMPT =========================================================================

PROMPT CREATE TABLE bra_customhr_move_plans
CREATE TABLE bra_customhr_move_plans (
  id NUMBER(15,0) NOT NULL
)
  TABLESPACE apps_ts_tx_data
  STORAGE (
    NEXT       1024 K
  )
  LOGGING
/



PROMPT CREATE TABLE bra_customhr_move_positions
CREATE TABLE bra_customhr_move_positions (
  id                   NUMBER       NOT NULL,
  proposal_id          NUMBER(15,0) NOT NULL,
  previous_position_id NUMBER(38,0) NULL,
  position_id          NUMBER(38,0) NULL,
  collaborator_id       NUMBER(15,0) NULL,
  begin_date           DATE         NULL,
  end_date             DATE         NULL,
  iteration            VARCHAR2(20) NULL
)
  TABLESPACE apps_ts_tx_data
  STORAGE (
    NEXT       1024 K
  )
  LOGGING
/



PROMPT CREATE TABLE bra_customhr_move_proposals
CREATE TABLE bra_customhr_move_proposals (
  id                 NUMBER(15,0) NOT NULL,
  planning_id        NUMBER(15,0) NOT NULL,
  manager_id         NUMBER(15,0) NOT NULL,
  status             VARCHAR2(20) NOT NULL,
  parent_proposal_id NUMBER       NULL,
  created_by         NUMBER(15,0) NOT NULL,
  created_at         DATE         NULL,
  updated_at         DATE         NULL
)
  TABLESPACE apps_ts_tx_data
  STORAGE (
    NEXT       1024 K
  )
  LOGGING
/



PROMPT =========================================================================
PROMPT PRIMARY AND UNIQUE CONSTRAINTS
PROMPT =========================================================================

PROMPT ALTER TABLE bra_customhr_move_plans ADD CONSTRAINT sys_c00241577 PRIMARY KEY
ALTER TABLE bra_customhr_move_plans
  ADD CONSTRAINT sys_c00241577 PRIMARY KEY (
    id
  )
  USING INDEX
    TABLESPACE apps_ts_tx_data
    STORAGE (
      NEXT       1024 K
    )
    LOGGING
/

PROMPT ALTER TABLE bra_customhr_move_positions ADD CONSTRAINT sys_c00241588 PRIMARY KEY
ALTER TABLE bra_customhr_move_positions
  ADD CONSTRAINT sys_c00241588 PRIMARY KEY (
    id
  )
  USING INDEX
    TABLESPACE apps_ts_tx_data
    STORAGE (
      NEXT       1024 K
    )
    LOGGING
/

PROMPT ALTER TABLE bra_customhr_move_proposals ADD CONSTRAINT sys_c00241582 PRIMARY KEY
ALTER TABLE bra_customhr_move_proposals
  ADD CONSTRAINT sys_c00241582 PRIMARY KEY (
    id
  )
  USING INDEX
    TABLESPACE apps_ts_tx_data
    STORAGE (
      NEXT       1024 K
    )
    LOGGING
/

PROMPT =========================================================================
PROMPT FOREIGN CONSTRAINTS
PROMPT =========================================================================

PROMPT ALTER TABLE bra_customhr_move_positions ADD CONSTRAINT fk_prpsldtls_prpsl FOREIGN KEY
ALTER TABLE bra_customhr_move_positions
  ADD CONSTRAINT fk_prpsldtls_prpsl FOREIGN KEY (
    proposal_id
  ) REFERENCES bra_customhr_move_proposals (
    id
  )
/

PROMPT ALTER TABLE bra_customhr_move_proposals ADD CONSTRAINT fk_prpsls_plns FOREIGN KEY
ALTER TABLE bra_customhr_move_proposals
  ADD CONSTRAINT fk_prpsls_plns FOREIGN KEY (
    planning_id
  ) REFERENCES bra_customhr_move_plans (
    id
  )
/

