var searchApp = angular.module('searchApp', ['ui.bootstrap', 'ui.select2'])

searchApp.controller('SearchModalCtrl', function ($scope, $modalInstance, $http, search_params){
  $scope.jobType = search_params.jobType

  $scope.jobSearch = function () {
    return { ajax: {
        url: search_params.path,
        dataType: 'json',
        quietMillis: 500,

        data: function (term, page) {
          var result = { term: term }

          return result
        },
        results: function (data, page) {
          var results = _(data).map(function (job) {
            job.id = job.job_id
            job.text = job.name

            return job
          })

          return { results: results }
        }
      }
    }
  }

  $scope.uniorgSearch = function () {
    return { ajax: {
        url: search_params.path,
        dataType: 'json',
        quietMillis: 500,

        data: function (term, page) {
          var result = { term: term }

          return result
        },
        results: function (data, page) {
          var results = _(data).map(function (uniorg) {
            uniorg.id = uniorg.organization_id
            uniorg.organization_name = uniorg.name
            uniorg.text = uniorg.name

            return uniorg
          })

          return { results: results }
        }
      }
    }
  }

  $scope.userSearch = function () {
    return { ajax: {
        url: search_params.path,
        dataType: 'json',
        quietMillis: 500,

        data: function (term, page) {
          var result = { term: term }

          return result
        },
        results: function (data, page) {
          var results = _(data).map(function (result) {
            user = result.user
            user.id = user.employee_person_id
            user.name = user.full_name
            user.text = user.full_name

            return user
          }, this)

          return { results: results }
        }
      }
    }
  }

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel')
  }

  $scope.confirm = function (selected) {
    $modalInstance.close(selected)
  }
})

searchApp.controller('SearchCtrl', function ($scope, SearchFactory){
  $scope.initializeWith = function (model, searchType, search_path) {
    $scope.searchFactory = new SearchFactory(search_path, searchType)

    if (model)
      $scope.result = $scope.searchFactory.searchOptions[searchType](model)
    $scope.search = $scope.searchFactory.search
  }

})

searchApp.factory("SearchFactory", function () {
  function SearchFactory(path, searchType) {
    this.path = path;

    this.searchOptions = this.searchOptions[searchType]
  }

  SearchFactory.prototype = {
    searchOptions: {
      uniorg: function (uniorg) {
        uniorg.id = uniorg.organization_id
        uniorg.organization_name = uniorg.name
        uniorg.text = uniorg.name

        return uniorg
      },

      job: function (job) {
        job.id = job.job_id
        job.text = job.name

        return job
      },

      people_group: function (people_group) {
        people_group.id = people_group.people_group_id
        people_group.text = people_group.group_name

        return people_group
      },
    },

    search: function () {
      return {
        ajax: {
          url: this.path,
          dataType: 'json',
          quietMillis: 500,
          optionsSlicer: this.searchOptions,

          data: function (term, page) {
            var result = { term: term }

            return result
          },
          results: function (data, page) {
            var results = _(data).map(this.optionsSlicer)

            return { results: results }
          }
        }
      }
    },
  }

  return SearchFactory
})