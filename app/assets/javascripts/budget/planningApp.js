var planningApp = angular.module('planningApp', ['ui.bootstrap', 'ui.select2', 'searchApp'])

planningApp.config(['datepickerConfig', 'datepickerPopupConfig', function(datepickerConfig, datepickerPopupConfig) {
  datepickerConfig.showWeeks = false;
  datepickerPopupConfig.showButtonBar = false;
}]);

planningApp.controller('DatepickerCtrl', function($scope) {
  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  $scope.setDate = function(date) {
    var splitted_date

    if (date && date.match(/\d{2}\/\d{2}\/\d{4}/)) {
      splitted_date = date.split('/')
      $scope.dt = new Date(splitted_date[2], splitted_date[1] - 1, splitted_date[0])
    }
  }
})

planningApp.controller('ManagersListCtrl', ['$scope', function ($scope){
  $scope.initializeWith = function(data) {
    $scope.filteredManagers = [];
    $scope.currentPage = 1;
    $scope.numPerPage = 10;
    $scope.maxSize = 5;
    $scope.managers = _(data).flatten();
    $scope.totalItems = $scope.managers.length
  };

  $scope.numPages = function () {
    return Math.ceil($scope.managers.length / $scope.numPerPage);
  };

  $scope.hidePagination = function () {
    return $scope.totalItems <= $scope.numPerPage;
  };

  $scope.emptyList = function() {
    return _($scope.managers).isEmpty()
  };

  $scope.$watch('currentPage + numPerPage', function() {
    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
    , end = begin + $scope.numPerPage;

    $scope.filteredManagers = $scope.managers.slice(begin, end);
  });
}])

planningApp.factory('Job', function() {
  function Job (attributes) {
    _(this).extend(attributes)
  }

  return Job
});

planningApp.factory('JobsContainer', ['Job', function (Job){
  function JobsContainer (attributes){
    _(this).extend(attributes)

    this.jobs = _(this.jobs).map(function (job) {
        return new Job(job.job_parameter)
      }, this)
  };

  JobsContainer.prototype = {
    addJob: function(job, job_type) {
      job = _(job).extend({planning_id: this.planning_id, job_type: job_type})
      job.id = null
      job.job_name = job.name
      this.jobs.push(new Job(job))
    },

    removeJob: function (job) {
      if(job.id) {
        job._destroy = true
      }
      else {
        this.jobs = _(this.jobs).without(job)
      }
    },
  }

  return JobsContainer
}]);

planningApp.factory('Uniorg', function(){
  function Uniorg (attributes){
    _(this).extend(attributes)
  };

  return Uniorg
});

planningApp.factory('UniorgsContainer', function (Uniorg) {
  function UniorgsContainer (attributes) {
    _(this).extend(attributes)

    this.list = _(this.list).map(function (uniorg) {
      return new Uniorg(uniorg.org_parameter)
    }, this)
  };

  UniorgsContainer.prototype = {
    addUniorg: function (uniorg) {
      uniorg = _(uniorg).extend({planning_id: this.planning_id})
      uniorg.id = null
      this.list.push(new Uniorg(uniorg))
    },

    removeUniorg: function (uniorg) {
      if (uniorg.id) {
        uniorg._destroy = true
      } else {
        this.list = _(this.list).without(uniorg)
      }
    },
  }

  return UniorgsContainer
})

planningApp.factory('ReportRule', function(){
  function ReportRule(attributes){
    _(this).extend(attributes)

    this._destroy = false
    this.user_id = this.user.employee_person_id
    this.user_number = this.user.employee_number
    this.user_name = this.user.full_name
  };

  ReportRule.prototype = {
    selectedSite: function (site) {
      return _(this.authorized_sites).contains(String(site))
    }
  }

  return ReportRule
})

planningApp.factory('ReportRulesContainer', function (ReportRule){
  function ReportRulesContainer(attributes){
    _(this).extend(attributes)

    this.list = _(this.list).map(function (rule) {
      return new ReportRule(rule)
    }, this)
  };

  ReportRulesContainer.prototype = {
    addReportRule: function(user) {
      var rule_attributes = {
        planning_id: this.planning_id,
        user: user
      }

      this.list.push(new ReportRule(rule_attributes))
    },

    removeReportRule: function (rule) {
      if (rule.id) {
        rule._destroy = true
      } else {
        this.list = _(this.list).without(rule)
      }
    }
  }

  return ReportRulesContainer
})

planningApp.controller('IneligibleJobsCtrl', function ($scope, JobsContainer, $modal) {
  $scope.initializeWith = function (data) {
    $scope.ineligibles = new JobsContainer(data)
  }

  $scope.chooseIneligible = function() {
    var modalInstance = $modal.open({
      templateUrl: 'jobSearchTemplate.html',
      controller: 'SearchModalCtrl',
      size: 'md',
      backdrop: 'static',
      resolve: {
        search_params: function() {
          return {
            jobType: 'Inelegíveis',
            path: $scope.ineligibles.path
          }
        }
      }
    })

    modalInstance.result.then(function (selectedJob) {
        $scope.ineligibles.addJob(selectedJob, 'INELIGIBLE')
    })
  }
});

planningApp.controller('UncountableJobsCtrl', function ($scope, JobsContainer, $modal) {
  $scope.initializeWith = function (data) {
    $scope.uncountables = new JobsContainer(data)
  }

  $scope.chooseUncountable = function() {
    var modalInstance = $modal.open({
      templateUrl: 'jobSearchTemplate.html',
      controller: 'SearchModalCtrl',
      size: 'md',
      backdrop: 'static',
      resolve: {
        search_params: function() {
          return {
            jobType: 'Incontáveis',
            path: $scope.uncountables.path
          }
        }
      }
    })

    modalInstance.result.then(function (selectedJob) {
      $scope.uncountables.addJob(selectedJob, 'UNCOUNTABLE')
    })
  }
});

planningApp.controller('UniorgsCtrl', function ($scope, $modal, UniorgsContainer) {
  $scope.initializeWith = function (data) {
    $scope.uniorgs = new UniorgsContainer(data)
  }

  $scope.chooseUniorg = function() {
    var modalInstance = $modal.open({
      templateUrl: 'uniorgSearchTemplate.html',
      controller: 'SearchModalCtrl',
      size: 'md',
      backdrop: 'static',
      resolve: {
        search_params: function() {
          return {
            path: $scope.uniorgs.path
          }
        }
      }
    })

    modalInstance.result.then(function (selected) {
      $scope.uniorgs.addUniorg(selected)
    })
  }
})


planningApp.controller('ReportRulesCtrl', function ($scope, $modal, ReportRulesContainer) {
  $scope.initializeWith = function (data) {
    $scope.report_rules = new ReportRulesContainer(data)
  }

  $scope.chooseUser = function() {
    var modalInstance = $modal.open({
      templateUrl: 'userSearchTemplate.html',
      controller: 'SearchModalCtrl',
      size: 'md',
      backdrop: 'static',
      resolve: {
        search_params: function() {
          return {
            path: $scope.report_rules.path
          }
        }
      }
    })

    modalInstance.result.then(function (selected) {
      $scope.report_rules.addReportRule(selected)
    })
  }
})
