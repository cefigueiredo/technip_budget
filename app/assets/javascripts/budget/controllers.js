var proposalApp = angular.module('proposalApp', ['ui.bootstrap', 'ui.select2', 'searchApp'])

proposalApp.config([
  "$httpProvider", function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = angular.element('meta[name=csrf-token]').attr('content');
  }
]);

proposalApp.controller('ProposalController', function ($scope, $http, ProposalService) {
  $scope.loading = true

  $scope.initializeWith = function(path, isHr, openPeriod, isNested) {
    $scope.proposal_path = path + '.json'
    $scope.approval_path = path + '/approve.json'
    $scope.tab = 1

    $scope.openPeriod = openPeriod

    if(isNested) {
      $scope.send_to_approval_path = path + '/send_proposal.json'
    } else {
      $scope.send_to_approval_path = 'send_proposal.json'
    }

    $http.get($scope.proposal_path).success(function(data) {
      $scope.proposal = data;
      $scope.parent_proposal = data.parent_proposal;
      $scope.service = ProposalService
      $scope.loading = false;
      $scope.isNested = isNested;

      ProposalService.initialize($scope.proposal)

      if(isHr) {
        $scope.isHr = true
        $scope.loadCuttingProposal()
      }
      else if($scope.service.isFinished()) {
        $scope.loadCuttingProposal()
      }
      else {
        $scope.isHr = false
      }
    })
  }

  $scope.loadCuttingProposal = function() {
    $scope.isCutPerspective = true
    $scope.tab = 1
    ProposalService.initialize($scope.proposal)
  },

  $scope.loadOriginalProposal = function() {
    $scope.isCutPerspective = false
    $scope.tab = 0
    ProposalService.initialize($scope.parent_proposal)
  }

  $scope.loadCurrentTeam = function () {
    $scope.isCutPerspective = true
    $scope.tab = 2
    ProposalService.initialize($scope.proposal)
  }

  $scope.isSummary = function () {
    if(!!$scope.service) {
      return $scope.service.isFinished() && ($scope.tab == 2)
    }
    return false
  }

  $scope.currentTab = function (tab) {
    return tab == $scope.tab
  }

  $scope.openMovementsModal = function(job) {
    $scope.modal = job;

    $('#movements-modal').modal({
      show: true,
      backdrop: 'static',
      keyboard: false
    });

    $scope.modal.cancel = function() {
      $('#movements-modal').modal('hide')

      $scope.modal = null;
      $scope.service.revert()
    },

    $scope.modal.saveData = function() {
      if($scope.jobPlanning.$valid){

        $scope.save().success(function () {
          $('#movements-modal').modal('hide')
          $scope.modal.submitted = false
        })
      } else {
        $scope.modal.submitted = true
      }
    }
  }

  $scope.send = function() {
    $scope.saving = true

    if(window.confirm('O planejamento será enviado e não poderá ser alterado, deseja continuar?')) {
      return $http({
        method: 'post',
        url: $scope.send_to_approval_path,
        data: ProposalService.serializeProposal()
      }).success(function(data) {
        $scope.error = false
        $scope.proposal = data
        $scope.service.initialize($scope.proposal)
        $scope.saving = false
        window.alert('O planejamento foi enviado com sucesso!')
      }).error(function(data) {
        window.alert('Não foi possível enviar a proposta agora, tente mais tarde.')
        $scope.error = true
        $scope.saving = false
      })
    }
  }

  $scope.save = function() {
    var saveMethod = $scope.proposal.id == null ? 'post' : 'put'
    $scope.saving = true

    return $http({
      method: saveMethod,
      url: $scope.proposal_path,
      data: ProposalService.serializeProposal()
    }).success(function(data) {
      $scope.error = false
      $scope.proposal = data;
      $scope.service.initialize($scope.proposal)
      $scope.saving = false;
    }).error(function(data) {
      // TODO: improve error handling
      $scope.saving = false;
      $scope.error = true;
    })
  }

  $scope.absoluteValue = function(value) {
    return Math.abs(value)
  }

  $scope.diffIconClass = function(value) {
    var css_class = ""

    if(value > 0)
      css_class = "fa fa-plus-circle"
    if (value < 0)
      css_class = "fa fa-minus-circle"

    return css_class
  }

  $scope.approve = function() {

    confirmed = window.confirm("Deseja aprovar esta proposta? Propostas aprovadas não poderão ser modificadas.")

    if(confirmed == true) {
      $scope.saving = true
      return $http({
        method: 'post',
        url: $scope.approval_path,
        data: ProposalService.serializeProposal()
      }).success(function(data) {
        $scope.error = false
        $scope.proposal = data;
        $scope.parent_proposal = data.parent_proposal
        $scope.service.initialize($scope.proposal)
        $scope.saving = false;
        window.alert("Proposta aprovada com sucesso!")
      }).error(function(data) {
        $scope.saving = false;
        $scope.error = true;
        window.alert("Não foi possível aprovar a proposta devido a um erro inesperado!")
      })
    }
  }

  $scope.isChangesLocked = function() {
    if($scope.openPeriod) {
      if($scope.isHr && $scope.isCutPerspective) {
        return $scope.service.isHrChangesLocked()
      }
      else if (!_($scope.service).isUndefined() && !$scope.isHr ) {
        return $scope.service.isManagerChangesLocked()
      }
      else {
        return true
      }
    }
    else {
      return true
    }
  }


  $scope.datepicker = {
    open: function () {
      $scope.datepicker.opened = true
    }
  }
})

proposalApp.directive('validatePromotion', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {

      ctrl.$parsers.unshift(function(viewValue) {
        // bind valid value to ngModel or undefined
        if (scope.movement.isValidPromotion(viewValue)) {
          ctrl.$setValidity('samePosition', true);
          return viewValue;
        } else {
          ctrl.$setValidity('samePosition', false);
          return undefined;
        }
      })

      ctrl.$formatters.unshift(function(viewValue) {
        ctrl.$setValidity('samePosition', scope.movement.isValidPromotion(viewValue))

        return viewValue;
      })
    }
  };
});

proposalApp.filter('orderPositionByQuantityOf', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });

    filtered = _(filtered).sortBy(function(pos) { return _(pos.movements).pluck(field).length })

    if(reverse) filtered.reverse();
    return filtered;
  };
});
