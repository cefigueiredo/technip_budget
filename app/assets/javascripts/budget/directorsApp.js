var directorsApp = angular.module('directorsApp', [])

directorsApp.controller('ManagersListController', function ($scope, $http) {
  $scope.initializeWith = function(path) {
    $scope.path = path + '.json';
    $scope.loading = true;

    $http.get($scope.path).success(function(data) {
      $scope.director = data;
      $scope.loading = false;
    })
  }

  $scope.orderByHeadcount = function() {
    $scope.order = '-diff_manager_headcount.value'
  }

  $scope.orderByName = function() {
    $scope.order = 'full_name'
  }

  $scope.orderByHires = function() {
    $scope.order = '-diff_manager_hires'
  }

  $scope.checkOrdering = function(orderBy) {
    if(orderBy == 'headcount') {
      return '-diff_manager_headcount.value' == $scope.order
    }
    else if(orderBy == 'name') {
      return 'full_name' == $scope.order
    }
    else if(orderBy == 'hires') {
      return '-diff_manager_hires' == $scope.order
    }
  }
})