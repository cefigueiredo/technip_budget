proposalApp.factory('PositionRequest', function (){
  function PositionRequest(attributes) {
    _(this).extend(attributes)
  };

  return PositionRequest;
})

proposalApp.factory('Movement', function (Movements, PositionRequest) {
  function Movement(attributes) {
    _(this).extend(attributes)

    this.synchronizedMovement = attributes

    if(this.position_request_id) {
      if(_(this.position_request_id).isNumber()) {
        this.temporary_position_id = 'INEXISTENT_' + this.position_request_id
      }
      else {
        this.temporary_position_id = this.position_request_id
      }
    }
    else {
      this.temporary_position_id = this.position_id
    }

    this.position_id = null
    this.position_request_id = null

    this._destroy = false
    this.changed = false

  }

  Movement.prototype = {
    fireCollaborator: function() {
      if(this.iteration != 'FIRED') {
        this.iteration = 'FIRED'
        this.position_id = null
        this.begin_date = null
        this.end_date = null
      }
      else {
        this.revert();
      }
    },

    promoteCollaborator: function() {
      if(this.iteration != 'PROMOTION') {
        this.iteration = 'PROMOTION'
        this.begin_date = null
        this.end_date = null
      }
      else {
        this.revert();
      }
    },

    serialize: function() {
      attributes = [
        'id',
        'position_id',
        'position_request_id',
        'proposal_id',
        'collaborator_id',
        'previous_position_id',
        'begin_date',
        'end_date',
        'iteration',
        'quantity',
        'quantity_approved',
        'proposed_by_hr',
        'motive',
        'uncountable'
      ]
      if(this._destroy == true){
        attributes.push('_destroy')
      }
      if(_(this.quantity_approved).isNull()) {
        this.quantity_approved = this.quantity
      }

      if(_(this.temporary_position_id).isString() && this.temporary_position_id.match(/INEXISTENT_.*/)) {
        this.position_request_id = this.temporary_position_id.split('_')[1]
      }
      else {
        this.position_id = this.temporary_position_id
      }

      serialized_attributes = _(this).pick(attributes)

      if(this.position_request_attributes) {
        var position_attributes = ["id", "job_id", "organization_id", "people_group_id", "requested_by", "position_name"]

        _(serialized_attributes).extend({
          "position_request_attributes": _(this.position_request_attributes).pick(position_attributes)
        })
      }
      return serialized_attributes
    },

    revert: function() {
      if(this.iteration != 'HIRE') {
        this.iteration = 'MAINTENANCE'

        this.position_id = null
        this.position_request_id = null
        this.begin_date = null
        this.end_date = null
      }
      else if ( !_(this.id).isNull() ) {
        this._destroy = true
      }
    },

    reject: function() {
      if(this.isHire()) {
        this.quantity_approved = 0
        this.changed = true
      }
      else {
        this.revert()
      }
    },

    revertReject: function() {
      if(this.isHire()) {
        this.quantity_approved = this.quantity
        this.changed = false
      }
    },

    hireRejected: function() {
      if(this.isHire() && this.proposedByManager()) {
        return this.quantity_approved != this.quantity
      }
    },

    isValidPromotion: function(promo_id) {
      if(!!promo_id) {
        return promo_id != this.previous_position_id
      }
      return !!this.inexistentPositionRequest
    },

    isPromotionOut: function(positionId) {
      return this.iteration == 'PROMOTION' && this.previous_position_id == positionId
    },

    isPromotionIn: function(positionId) {
      return this.iteration == 'PROMOTION' && this.temporary_position_id == positionId && !this.isPromotionOut(positionId)
    },

    isFire: function() {
      return this.iteration == 'FIRED' ? true : false
    },

    isHire: function() {
      return this.iteration == 'HIRE' ? true : false
    },

    open: function () {
      this.datepicker.opened = true
    },

    persisted: function() {
      return !_(this.id).isNull()
    },

    proposedByHr: function() {
      return this.proposed_by_hr
    },

    proposedByManager: function() {
      return ! this.proposedByHr()
    },

    quantityApproved: function() {
      if(!Movements.isManagerLocked) {
        this.quantity_approved = this.quantity
        return this.quantity
      }
      else {
        return this.quantity_approved
      }
    },

    changedByHr: function () {
      return this.quantityApproved() != this.quantity
    },

    promoteToInexistentPosition: function (requester_id) {
      this.inexistentPositionRequest = true;
      this.temporary_position_id = "";
      this.position_request_attributes = new PositionRequest();
    },

    dismissInexistentPosition: function () {
      this.inexistentPositionRequest = false
      this.position_request_attributes = null
    }
  }

  return Movement
})

proposalApp.factory('Position', function (Movement, Movements) {
  function Position (attributes) {
    _(this).extend(attributes)

    this.originalHeadcount = 0
    this.hiresCount = 0
    this.firesCount = 0
    this.promotionsInCount = 0
    this.promotionsOutCount = 0
    this.hiresUncountable = 0
    this.firesUncountable = 0
    this.hasImpatriates = false

    this.movements = _(this.movements).map(function (mov) {
      return new Movement(mov)
    })
  }

  Position.prototype = {
    calculateMovements: function() {
      this.originalHeadcount = 0
      this.originalUncountableHeadcount = 0
      this.hiresCount = 0
      this.firesCount = 0
      this.hiresUncountable = 0
      this.firesUncountable = 0
      this.promotionsOutCount = 0
      this.promotionsInCount = this.promotionInMovements().length
      this.changedByHr = false

      _(this.movements).each(function (movement) {
        if(movement.changedByHr()) {
          this.changedByHr = true
        }

        if(movement.uncountable) {

          if(!this.uncountable) {
            this.hasImpatriates = true
          }

          if(movement.iteration != 'HIRE') {
            this.originalUncountableHeadcount += movement.quantityApproved()
          }

          if(movement.iteration == 'HIRE') {
            this.hiresUncountable += movement.quantityApproved()
          }
          else if(movement.iteration == 'FIRED') {
            this.firesUncountable += movement.quantityApproved()
          }
        }

        else {
          if(movement.iteration != 'HIRE') {
            this.originalHeadcount += movement.quantityApproved()
          }

          if(movement.iteration == 'HIRE') {
            this.hiresCount += movement.quantityApproved()
          }
          else if (movement.iteration == 'FIRED') {
            this.firesCount += movement.quantityApproved()
          }
          else if (movement.iteration == 'PROMOTION') {
            this.promotionsOutCount += movement.quantityApproved()
          }
        }
      }, this)
    },

    promotionInMovements: function () {
      return _(Movements.by_temporary_position_id[this.position_id]).filter(function(movement) {
        return movement.iteration == 'PROMOTION'
      })
    },

    hireCollaborator: function(isHr) {
      var movement = new Movement({
        id          : null,
        position_id : this.position_id,
        proposal_id : this.proposal_id,
        begin_date  : null,
        iteration   : 'HIRE',
        quantity_approved: null,
        proposed_by_hr: (isHr || false),
        uncountable: this.uncountable
      })

      this.movements.push(movement)
    },

    hireCollaboratorByHr: function() {
      this.hireCollaborator(true)
    },

    revertHire: function(movement) {
      if(_(movement.id).isNull() && movement.iteration == 'HIRE') {
        this.movements.splice(this.movements.indexOf(movement), 1)
        this.calculateMovements()
      }
      else {
        movement.revert()
      }
    },

    listActualMovements: function() {
      return _(this.movements).filter(function(mov) {
        return mov.iteration != 'HIRE'
      }).concat(this.promotionInMovements())
    },

    listHireMovements: function() {
      return _(this.movements).filter(function(mov) {
        return mov.iteration == 'HIRE' && mov._destroy == false
      })
    }
  }

  return Position
})

proposalApp.factory('Job', function (Position) {
  function Job (attributes) {
    _(this).extend(attributes)

    this.positions = _(this.positions).map(function (pos) {
      pos.uncountable = this.uncountable
      return new Position(pos)
    }, this)

    this.hasImpatriates = false
  }

  Job.prototype = {
    calculateMovements: function () {
      this.originalHeadcount = 0
      this.originalUncountableHeadcount = 0
      this.hiresCount = 0
      this.firesCount = 0
      this.hiresUncountable = 0
      this.firesUncountable = 0
      this.promotionsInCount = 0
      this.promotionsOutCount = 0
      this.changedByHr = false

      _(this.positions).each(function (position) {
        position.calculateMovements()

        if(position.changedByHr) {
          this.changedByHr = true
        }

        if(position.hasImpatriates) {
          this.hasImpatriates = true
        }

        this.originalHeadcount += position.originalHeadcount
        this.originalUncountableHeadcount += position.originalUncountableHeadcount
        this.hiresCount += position.hiresCount
        this.firesCount += position.firesCount
        this.hiresUncountable += position.hiresUncountable
        this.firesUncountable += position.firesUncountable
        this.promotionsInCount += position.promotionsInCount
        this.promotionsOutCount += position.promotionsOutCount
      }, this)
    },

    previousTeamSize: function () {
      return this.previous_team.length
    },

    currentTeamSize: function () {
      return this.current_team.length
    },

    countableTeamSize: function () {
      return this.originalHeadcount
    },

    plannedHeadcount: function() {
      var income = this.hiresCount + this.promotionsInCount
      var outcome = this.firesCount + this.promotionsOutCount

      return this.previousTeamSize() + income - outcome
    },

    diffApprovedHeadcount: function () {
      return this.plannedHeadcount() - this.currentTeamSize()
    },

    deltaHeadcount: function() {
      return this.hiresCount - this.firesCount
    },

    deltaUncountable: function() {
      return this.hiresUncountable - this.firesUncountable
    },

    previousTeamSizeWithUncountables: function() {
      return this.originalHeadcount + this.originalUncountableHeadcount
    },

    hiresCountWithUncountables: function() {
      return this.hiresCount + this.hiresUncountable
    },

    firesCountWithUncountables: function() {
      return this.firesCount + this.firesUncountable
    },

    plannedHeadcountWithUncountables: function() {
      var income = this.hiresCount + this.promotionsInCount + this.hiresUncountable
      var outcome = this.firesCount + this.promotionsOutCount + this.firesUncountable

      return this.previousTeamSizeWithUncountables() + income - outcome
    },

    plannedUncountables: function() {
      var income = this.hiresUncountable
      var outcome = this.firesUncountable

      return this.originalUncountableHeadcount + income - outcome
    }
  }

  return Job
})

proposalApp.service('Movements', function () {
  return {
    initialize: function (jobs, isManagerLocked) {
      this.all = _(jobs).chain().pluck('positions').flatten().pluck('movements').flatten().value()

      this.by_temporary_position_id = _(this.all).groupBy('temporary_position_id')
      this.by_position_request_id = _(this.all).groupBy('position_request_id')
      this.isManagerLocked = isManagerLocked
    },
    all: {},
    by_temporary_position_id: {},
    by_position_request_id: {},
  }
})

proposalApp.service('ProposalService', function (Job, Movements) {
  return {
    initialize: function (proposal) {
      _(this).extend(proposal)
      this.synchronizedProposal = proposal

      this.jobs = _(this.jobs).map(function(job) {
        return new Job(job)
      })


      Movements.initialize(this.jobs, this.isManagerChangesLocked())

      this.movements = Movements

      this.monthsList = []

      this.setPlanningMonths()
      this.calculateTotals()

      this.possiblePositions = _(this.jobs).chain().pluck('positions').flatten().value()

      this.planning.begin_planning_date = this.localizedDate(this.planning.begin_planning_date)
      this.planning.end_planning_date = this.localizedDate(this.planning.end_planning_date)
      this.created_at = this.localizedDate(this.created_at)
      this.updated_at = this.localizedDate(this.updated_at)
    },

    revert: function () {
      this.initialize(this.synchronizedProposal)
    },

    calculateMovements: function () {
      _(this.jobs).each(function(job) {
        job.calculateMovements()
      })
    },

    calculateTotals: function () {
      this.calculateMovements()

      this.previousHeadcount = 0
      this.currentHeadcount = 0
      this.totalHires = 0
      this.totalFires = 0
      this.totalPromotions = 0
      this.totalHeadcount = 0
      this.totalDeltaHeadcount = 0
      this.totalDiffApprovedHeadcount = 0
      this.totalDeltaUncountable = 0
      this.totalUncountables = 0

      _(this.jobs).each(function(job) {
        this.totalDeltaUncountable += job.deltaUncountable()
        this.totalUncountables += job.plannedUncountables()

        if(!job.uncountable) {
          this.previousHeadcount += job.previousTeamSize()
          this.currentHeadcount += job.currentTeamSize()
          this.totalHires += job.hiresCount
          this.totalFires += job.firesCount
          this.totalPromotions += job.promotionsInCount
          this.totalHeadcount += job.plannedHeadcount()
          this.totalDeltaHeadcount += job.deltaHeadcount()
          this.totalDiffApprovedHeadcount += job.diffApprovedHeadcount()
        }
      }, this)
    },

    proposedMovements: function () {
      return _(this.jobs).chain().map(function(job) {
        return _(job.positions).map(function(position) {
          return _(position.movements).map(function(movement) {
            return movement.serialize()
          })
        })
      }).flatten().value()
    },

    serializeProposal: function() {
      return {
        "proposal" : {
          "planning_id": this.planning_id,
          "id": this.id,
          "parent_proposal_id": this.parent_proposal_id,
          "status": this.status,
          "manager_id": this.manager.employee_person_id,
          "created_by": this.created_by,
          "proposed_movements_attributes": _(this.proposedMovements()).map(function (mov) {
            if(mov.position_request_attributes) {
              mov.position_request_attributes.requested_by = this.manager.employee_person_id
            }

            return mov
          }, this)
        }
      }
    },

    getPositionById: function(positionId) {
      return _(this.possiblePositions).findWhere({position_id: positionId })
    },

    setPlanningMonths: function() {
      var dt = this.planning.min_begin_date.split('T')[0].split('-'),
          dt_fim = this.planning.max_end_date.split('T')[0].split('-'),
          planned_date = new Date(dt[0], dt[1] - 1 , 1),
          end_date = new Date(dt_fim[0], dt_fim[1] - 1, 1);

      while(planned_date <= end_date) {
        dt = new Date(planned_date)
        this.monthsList.push({
          value: dt.toJSON().replace('.000', ''),
          label: dt
        })

        planned_date.setMonth(dt.getMonth() + 1, 1)
      }
    },

    isManagerChangesLocked: function() {
      return _(['PENDING-DIRECTOR', 'PENDING-HR', 'FINISHED']).contains(this.status)
    },

    isHrChangesLocked: function() {
      if(_(this.parent_proposal.id).isUndefined()) {
        return true
      }
      else {
        return !_(['PENDING-HR']).contains(this.status)
      }
    },

    possiblePromotionPositions: function(jobId) {
      return _(this.jobs).chain().reject(function(job) { return job.job_id == jobId }).pluck('positions').flatten().value()
    },

    possibleGradeFilter: function(position) {
      return function(pos) {
        return pos.mid_value >= position.mid_value
      }
    },

    breadcrumbPlaceholder: function() {
      if(this.manager.id == this.director.id) {
        return 'DIRETOR'
      }
      else {
        return 'GESTOR'
      }
    },

    isFinished: function() {
      return this.status == 'FINISHED'
    },

    localizedDate: function(date_str) {
      return date_str ? date_str.replace('Z', '-0300') : date_str
    }
  }
})
