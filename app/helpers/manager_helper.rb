module ManagerHelper

  def manager_back(subj)
    if subj.manager_person_id == current_user.id
      managers_path
    else
      manager_path(:id => subj.manager_person_id)
    end
  end
end