module DirectorsHelper

  def count_finisheds
    finisheds = @directors.select do |director|
      count_managers = director.nested_managers_id.count

      director_totals = ActiveRecord::Base.connection.select_value("
        select sum(case when status = 'FINISHED' then 1 else 0 end) as count_finished
          from bra.bra_customhr_move_proposals
         where planning_id = #{@planning.id}
           and director_id = #{director.id}
      ")

      director_totals == count_managers
    end

    finisheds.count
  end

  def count_pendings
    count_directors = User.find_all_directors_by_site(current_user.site).count

    count_directors - count_finisheds
  end

  def count_pending_presidents
    count_directors = User.find_all_presidents_by_site(current_user.site).count

    count_directors - count_finisheds
  end

  def pending_approvals?
    return false if @director.nested_managers_id.empty?

    sanitized_nested_managers = @director.nested_managers_id.join(",")

    count = ActiveRecord::Base.connection.select_value("
      select count(1) as total
        from bra.bra_customhr_move_proposals
       where planning_id = #{@planning.id}
         and manager_id in (#{sanitized_nested_managers})
         and status in ('PENDING-HR')
    ")

    count > 0
  end

  def css_status(director)
    status = ActiveRecord::Base.connection.select_one("
      select count(1) as total, sum(case when status = 'FINISHED' then 1 else 0 end) as finisheds
        from bra.bra_customhr_move_proposals
       where planning_id = #{@planning.id}
         and director_id = #{director.id}
    ")

    total_expected = director.nested_managers_id.count + 1

    status["total"] == total_expected && status["total"] == status["finisheds"] ? 'finished' : 'pending'
  end

  def custom_back
    if @director.is_director?
      directors_path
    else
      director_path(@director.manager_person_id)
    end
  end

  def user_back(subj)
    if subj.president?
      users_path
    else
      user_path(subj.manager_person_id)
    end
  end

end