# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  # field do
  #   <p>content</p>
  # end
  #
  # Example:
  # <div class='field'>
  #   <p>content</p>
  #   <div class='clear'></div>
  # </div>
  #
  def field(options = {}, &block)
    options[:class] = "field #{options[:class]}"
    concat(content_tag('div', capture(&block).concat(clear_tag), :class => options[:class]))
  end

  # title 'Add new field'
  #
  # Example:
  # <h2 class='main'>Add new field<h2/>
  # <br />
  #
  # title 'Add new field', 'Some details here'
  #
  # Example:
  # <h2 class='main'>Add new field</h2>
  # <p>Some details here</p>
  # <br />
  #
  def title(value, details = nil)
    t = content_tag('h2', value, :class => 'main')
    d = details.nil?? '' : content_tag('p', details)
    return "#{t}#{d}<br />"
  end

  def web_context
    return (Rails.env == 'production') ? "/budget" : ''
  end

  # translated attribute
  def ta(key)
    I18n.t("activerecord.attributes.#{key}")
  end

  def timestamp
    "#{Time.now.to_f}+#{rand}".gsub('.','').gsub('+', '').gsub('_', '')
  end

  def to_utf16(str)
    Iconv.conv('utf-16le', 'utf-8', str)
  end

  def clear_tag
    content_tag('div', '', :class => 'clear')
  end

  def logout_url
    WebParameter.get_by_key('LOGOUT_URL').value || '#'
  end

  def css_difference(counter=0)
    if counter > 0
      'plus'
    elsif counter < 0
      'minus'
    end
  end

  def pretty_date(date)
    date.strftime('%d/%m/%Y') unless date.blank?
  end

  def to_utf16(str)
    Iconv.conv('utf-16le', 'utf-8', str)
  end

  def counter_icon(counter=0, html_safe=false)
    if counter > 0
      difference = 'fa fa-plus'
    end

    return "<i class='#{difference}'></i>" if difference && !html_safe
    difference if html_safe
  end

end