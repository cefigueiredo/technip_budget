# -*- encoding : utf-8 -*-
class CoreOrganizationUnit < ActiveRecord::Base
  set_table_name "hr_all_organization_units"
  self.primary_key = 'organization_id'

  has_many :core_positions, :foreign_key => "organization_id", :primary_key => "organization_id"

  def self.options_of_organization_units(name)
    connection.select_all "
      SELECT
            haou.organization_id
           ,haou.name
           ,(haou.name || ' (' || flv1.meaning || ' - ' || flv2.meaning || ')') name_displayed
           ,haou.attribute20 name_formatted
       FROM hr.hr_all_organization_units haou
           ,apps.fnd_lookup_values       flv1
           ,apps.fnd_lookup_values       flv2
       WHERE haou.business_group_id      = 511
        and flv1.lookup_type             = 'ORG_TYPE'
        and flv1.language                = 'PTB'
        and flv1.enabled_flag            = 'Y'
        and flv1.lookup_code             = haou.type
        and flv2.lookup_type             = 'TC_HR_SITE_LOCAL'
        and flv2.language                = 'PTB'
        and flv2.enabled_flag            = 'Y'
        and flv2.lookup_code             = haou.attribute1
        and haou.date_to is null
        and haou.attribute20 is not null
        and haou.type is not null
        and haou.type not in ('1','2')
        #{sanitize_sql_for_assignment(["AND UPPER(haou.name) LIKE UPPER(?)", "%#{name}%"])}
       ORDER BY haou.attribute20"
  end

  self.inheritance_column = :_type_disabled

  def name_formatted
    self.attribute20
  end

end
