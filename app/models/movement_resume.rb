# -*- encoding: utf-8 -*-
class MovementResume < ActiveRecord::Base
  set_table_name "bra.bra_customhr_move_resumes"
  set_sequence_name "bra.bra_customhr_move_resumes_s"

  belongs_to :manager,
    :class_name => "User",
    :primary_key => "employee_person_id",
    :foreign_key => "manager_person_id"

  belongs_to :planning,
    :class_name => "MovementPlanning",
    :foreign_key => "planning_id"

  belongs_to :proposal,
    :class_name => "MovementProposal",
    :foreign_key => "proposal_id"

  belongs_to :owner,
    :class_name => "User",
    :primary_key => "employee_person_id",
    :foreign_key => "created_by"

  belongs_to :director,
    :class_name => "User",
    :primary_key => "employee_person_id",
    :foreign_key => "director_person_id"
end
