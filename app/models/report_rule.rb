class ReportRule < ActiveRecord::Base
  set_table_name "bra.bra_customhr_move_report_rules"
  set_sequence_name "bra.bra_customhr_mvrprtrls_s"

  belongs_to :planning, :class_name => "MovementPlanning"
  belongs_to :user, :primary_key => "employee_person_id"

  def sites
    ReportRule.sites.select do |site|
      authorized_sites.include?(site["id"].to_s)
    end
  end

  def self.sites
    [
      {"id" => 1, "name" => "MACAE" },
      {"id" => 2, "name" => "RIO" },
      {"id" => 3, "name" => "VITORIA" },
      {"id" => 4, "name" => "ANGRA"},
    ]
  end

  def self.has_rules_associated?(user, planning)
    !!find(:first, :conditions => ["user_id = ? and planning_id = ?", user.id, planning.id])
  end

  def report_authorized_sites(site = nil)
    sql = "
      SELECT DISTINCT
           'COM ORCAMENTO' orcamento
           ,bccv_g.employee_number matricula_gestor
           ,bccv_g.full_name gestor
           ,pap.payroll_name tipo_colab
           ,bccv_c.employee_number matricula_colaborador
           ,bccv_c.full_name          colaborador
           ,CASE psc.iteration
              WHEN 'MAINTENANCE'  THEN 'MANTIDO'
              WHEN 'PROMOTION'    THEN 'PROMOVIDO'
              WHEN 'FIRED'        THEN 'DESLIGADO'
              WHEN 'HIRE'         THEN 'A CONTRATAR'
              ELSE 'NAO IDENTIFICADO'
           END tipo
           ,apps.bra_infohr.bra_retorna_dadoshr('DIRETOR',NULL,NVL(bccv_c.employee_person_id, bccv_g.employee_person_id),'BRAAVADE') diretor
           ,DECODE(psc.iteration, 'FIRED', (psc.quantity * -1), psc.quantity) quantidade
           ,DECODE(psc.iteration, 'HIRE', NULL, pj_a.name) cargo_atual
           ,DECODE(psc.iteration, 'HIRE', NULL, pjd_a.segment2) nome_cargo_atual
           ,DECODE(psc.iteration, 'HIRE', NULL, hapf_a.name) posicao_atual
           ,pj_n.name novo_cargo
           ,pjd_n.segment2 nome_novo_cargo
           ,hapf_n.name nova_posicao
           ,haou.attribute7 centro_custo
           ,flv.meaning site
           ,haou.attribute20 codigo_uniorg
           ,SUBSTR(haou.name,(INSTR(haou.name,'-',1)+1 )) nome_uniorg
           ,haou.name uniorg
           ,CASE psc.iteration
              WHEN 'HIRE' THEN (SELECT unig.group_name
                                  FROM apps.BRA_HR_UNIORG_EMPRESA_V unig
                                  WHERE uniorg_child = haou.attribute20
                                  AND ROWNUM <= 1)
              ELSE bccv_c.people_group_name
            END empresa
           ,CASE ppl.status
              WHEN 'PENDING-MANAGER'            THEN 'GESTOR PLANEJANDO'
              WHEN 'PENDING-DIRECTOR'           THEN 'AGUARDANDO APROVAÇÃO DO DIRETOR'
              WHEN 'PENDING-HR'                 THEN 'AGUARDANDO APROVAÇÃO DO RH'
              WHEN 'REJECTED-DIRECTOR'          THEN 'REJEITADO PELO DIRETOR'
              WHEN 'REJECTED-DIRECTOR-TIMEOUT'  THEN 'REJEITADO PELO DIRETOR (TIMEOUT)'
              WHEN 'FINISHED'                   THEN 'APROVADO'
              ELSE 'NAO IDENTIFICADO'
            END status
           ,bcmp.calendar_name calendario
           ,CASE psc.iteration
              WHEN 'MAINTENANCE' THEN NULL
              ELSE to_char(psc.begin_date, 'MON-YYYY')
            END a_partir_de
           ,psc.motive motivo
      FROM bra.bra_customhr_move_plans bcmp
          ,bra.bra_customhr_move_proposals ppl
          ,bra.bra_customhr_move_positions psc
          ,apps.bra_customhr_collaborators_v bccv_g
          ,apps.bra_customhr_collaborators_v bccv_c
          ,apps.per_all_assignments_f paaf
          ,apps.pay_all_payrolls_f pap
          ,hr.hr_all_positions_f hapf_a
          ,hr.hr_all_positions_f hapf_n
          ,apps.hr_all_organization_units haou
          ,apps.per_jobs pj_a
          ,apps.per_jobs pj_n
          ,apps.per_job_definitions pjd_a
          ,apps.per_job_definitions pjd_n
          ,apps.fnd_lookup_values flv
      WHERE ppl.id = psc.proposal_id
      AND ppl.manager_id      = bccv_g.employee_person_id
      AND psc.collaborator_id = bccv_c.employee_person_id (+)
      -- planejamentos realizados pelos gestores
      AND ppl.parent_proposal_id IS NULL
      -- posicao atual
      AND NVL(psc.previous_position_id, psc.position_id) = hapf_a.position_id
      AND TRUNC(sysdate) BETWEEN hapf_a.effective_start_date AND hapf_a.effective_end_date
      AND hapf_a.availability_status_id <> 5
      AND ((hapf_a.status <> 'INVALID') OR hapf_a.status IS NULL)
      AND hapf_a.business_group_id = 511
      -- nova posicao
      AND NVL(psc.position_id, psc.previous_position_id) = hapf_n.position_id
      AND TRUNC(sysdate) BETWEEN hapf_n.effective_start_date AND hapf_n.effective_end_date
      AND hapf_n.availability_status_id <> 5
      AND ((hapf_n.status <> 'INVALID') OR hapf_n.status IS NULL)
      AND hapf_n.business_group_id = 511
      -- uniorg
      AND hapf_n.organization_id = haou.organization_id
      -- site
      AND flv.lookup_code = haou.attribute1
      AND flv.lookup_type = 'TC_HR_SITE_LOCAL'
      AND flv.LANGUAGE = 'PTB'
      -- cargo atual
      AND hapf_a.job_id = pj_a.job_id
      AND pj_a.job_definition_id = pjd_a.job_definition_id
      -- novo cargo
      AND hapf_n.job_id = pj_n.job_id
      AND pj_n.job_definition_id = pjd_n.job_definition_id
      -- cargos inelegíveis
      AND pj_a.job_id NOT IN (select distinct job_id
                                from bra.bra_customhr_move_job_params ineleg
                                where ineleg.planning_id = bcmp.id)
      -- tipo colab
      AND paaf.assignment_id = bccv_c.assignment_id
      AND paaf.payroll_id = pap.payroll_id
      AND paaf.business_group_id = 511
      AND paaf.assignment_type = 'E'
      AND paaf.primary_flag = 'Y'
      AND sysdate BETWEEN paaf.effective_start_date AND paaf.effective_end_date
      -- calendário
      AND ppl.planning_id = bcmp.id
      AND bcmp.id = #{planning_id}
      -- authorized_sites
      AND haou.attribute1 in (#{authorized_sites})

    UNION all

    SELECT DISTINCT
           'SEM ORCAMENTO' orcamento
          ,bccv_g.employee_number matricula_gestor
          ,bccv_g.full_name gestor
          ,pap.payroll_name as tipo_colab
          ,bccv.employee_number matricula_colaborador
          ,bccv.full_name colaborador
          ,'MANTIDO' tipo
          ,apps.bra_infohr.bra_retorna_dadoshr('DIRETOR',NULL,bccv.employee_person_id,'BRAAVADE') diretor
          ,1 quantidade
          ,bccv.job_name cargo_atual
          ,pjd.segment2 nome_cargo_atual
          ,hapf.name posicao_atual
          ,NULL novo_cargo
          ,NULL nome_novo_cargo
          ,NULL nova_posicao
          ,haou.attribute7 centro_custo
          ,flv.meaning site
          ,haou.attribute20 codigo_uniorg
          ,SUBSTR(haou.name,(INSTR(haou.name,'-',1)+1 )) nome_uniorg
          ,haou.name uniorg
          ,bccv.people_group_name empresa
          ,'GESTOR PLANEJANDO' status
          ,plann.calendar_name
          ,NULL a_partir_de
          ,NULL motivo
      FROM apps.bra_customhr_collaborators_v bccv
          ,apps.bra_customhr_collaborators_v bccv_g
          ,apps.per_all_assignments_f paaf
          ,apps.pay_all_payrolls_f pap
          ,hr.hr_all_positions_f hapf
          ,apps.hr_all_organization_units haou
          ,apps.per_jobs pj
          ,apps.per_job_definitions pjd
          ,apps.fnd_lookup_values flv
          ,bra.bra_customhr_move_plans plann
      WHERE NOT EXISTS (SELECT *
                          FROM bra.bra_customhr_move_positions bcmp_1
                              ,bra.bra_customhr_move_proposals bcmp_2
                          WHERE bcmp_1.proposal_id = bcmp_2.id
                          AND bcmp_2.planning_id = plann.id
                          AND bccv.employee_person_id = bcmp_1.collaborator_id) -- ORÇAMENTO 2015
      -- gestor
      AND bccv_g.employee_person_id = bccv.manager_person_id
      -- posicao atual
      AND bccv.position_id = hapf.position_id
      AND TRUNC(sysdate) BETWEEN hapf.effective_start_date AND hapf.effective_end_date
      AND hapf.availability_status_id <> 5
      AND ((hapf.status <> 'INVALID') OR hapf.status IS NULL)
      AND hapf.business_group_id = 511
      -- cargo atual
      AND hapf.job_id = pj.job_id
      AND pj.job_definition_id = pjd.job_definition_id
      -- uniorg
      AND hapf.organization_id = haou.organization_id
      -- site
      AND flv.lookup_code = haou.attribute1
      AND flv.lookup_type = 'TC_HR_SITE_LOCAL'
      AND flv.LANGUAGE = 'PTB'
      -- cargos inelegíveis
      AND pj.job_id NOT IN (select distinct job_id
                                from bra.bra_customhr_move_job_params ineleg
                                where ineleg.planning_id = plann.id)
      -- tipo collab
      AND paaf.assignment_id = bccv.assignment_id
      AND paaf.payroll_id = pap.payroll_id
      AND paaf.business_group_id = 511
      AND paaf.assignment_type = 'E'
      AND paaf.primary_flag = 'Y'
      AND sysdate BETWEEN paaf.effective_start_date AND paaf.effective_end_date
      -- calendario
      AND plann.id = #{planning_id}
      -- authorized_sites
      AND haou.attribute1 in (#{authorized_sites})
    ORDER by gestor, colaborador
    "

    connection.select_all(sql)
  end
end