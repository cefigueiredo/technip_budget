# -*- encoding : utf-8 -*-
class CoreJob < ActiveRecord::Base
  set_table_name "per_jobs"
  self.primary_key = 'job_id'

  has_many :core_positions, :foreign_key => "job_id", :primary_key => "job_id"

  def core_positions_by_organization_id(organization_id)
    self.core_positions.find(
      :all,
      :conditions => ['organization_id = ?', organization_id]
    )
  end

  def core_positions_with_organization
    self.core_positions.find(
      :all,
      :include => :core_organization_unit)
  end

  def self.options_of_jobs(country, name)
    case country
    when 'france'
      return options_of_jobs_in_france(name)
    when 'brazil'
      return options_of_jobs_in_brazil(name)
    end
  end

  def self.options_of_jobs_in_france(name)
    connection.select_all "
      SELECT lookup_code job_id,
             meaning name
      FROM fnd_lookup_values
      WHERE lookup_type = 'TC_HR_CARGO_MATRIZ'
      AND LANGUAGE = 'PTB'
      #{sanitize_sql_for_assignment(["AND UPPER(meaning) LIKE UPPER(?)", "%#{name}%"])}
      ORDER BY meaning ASC"
  end

  def self.options_of_jobs_in_brazil(name)
    connection.select_all "
      SELECT DISTINCT
             pj.job_id
            ,pj.name
            ,(SELECT pjd.segment2
              FROM per_job_definitions pjd
              WHERE pjd.job_definition_id = pj.job_definition_id) name_segmented
       FROM hr.per_jobs pj
       WHERE pj.date_to is null
       AND pj.business_group_id = 511
       #{sanitize_sql_for_assignment(["AND UPPER(pj.name) LIKE UPPER(?)", "%#{name}%"])}
       ORDER BY pj.name"
  end

  def name_formatted
    unless new_record?
      ActiveRecord::Base.connection.select_value("
        SELECT pjd.segment2
         FROM hr.per_job_definitions pjd
         WHERE pjd.job_definition_id = #{self.job_definition_id}")
    end
  end

  def self.find_job_in_france(job_id)
    find_by_sql("
      SELECT lookup_code job_id,
             meaning name
      FROM fnd_lookup_values
      WHERE lookup_type = 'TC_HR_CARGO_MATRIZ'
      AND LANGUAGE = 'PTB'
      AND lookup_code = ?", job_id).first
  end

  def subordinate_positions(manager_employee_number)
    sql = "
      with hapf_with_salaries as (
        SELECT hapf.*
             , pgrf.minimum
             , pgrf.mid_value
             , pgrf.maximum
          FROM hr_all_positions_f hapf
             , pay_grade_rules_f pgrf
         WHERE hapf.entry_grade_rule_id = pgrf.grade_rule_id
           AND TRUNC(sysdate) BETWEEN hapf.effective_start_date AND hapf.effective_end_date
           AND hapf.availability_status_id <> 5
           AND ((hapf.status <> 'INVALID') OR hapf.status IS NULL)
           AND hapf.business_group_id = 511
           AND TRUNC(SYSDATE) BETWEEN pgrf.effective_start_date AND pgrf.effective_end_date
      )
      select distinct
            hapf.name name
           ,hapf.position_id position_id
           ,hapf.organization_id organization_id
       from hapf_with_salaries                   hapf
           ,hr.per_jobs                          pj
           ,apps.per_org_structure_elements_v    pose
           ,hr.per_all_people_f                  papf
           ,hr.per_all_assignments_f             paaf
           ,hr.per_periods_of_service            ppos
           ,hr.per_organization_structures       pos
           ,hr.per_org_structure_versions        posv
           ,hr.per_job_definitions               pjd
       where papf.business_group_id              = 511
        and paaf.business_group_id               = 511
        and paaf.primary_flag                    = 'Y'
        and paaf.assignment_type                 = 'E'
        and hapf.job_id                          = pj.job_id
        and papf.person_id                       = paaf.person_id
        and papf.person_id                       = ppos.person_id
        and pjd.job_definition_id                = pj.job_definition_id
        and pose.organization_id_parent          = paaf.organization_id
        and hapf.organization_id                 = pose.organization_id_child
        and pose.org_structure_version_id        = posv.org_structure_version_id
        and pos.organization_structure_id        = posv.organization_structure_id
        and hapf.availability_status_id          <> 5
        and (     posv.date_to                   is null
              or  posv.date_to                   >= sysdate   )
        and (     pj.date_to                     is null
              or  pj.date_to                     >= sysdate   )
        and ((hapf.status <> 'INVALID')          or hapf.status is null)
        and (ppos.actual_termination_date        is null or ppos.actual_termination_date > sysdate)
        and sysdate between hapf.effective_start_date and hapf.effective_end_date
        and sysdate between papf.effective_start_date and papf.effective_end_date
        and sysdate between paaf.effective_start_date and paaf.effective_end_date
        and to_char(pos.organization_structure_id) in (select fpov.profile_option_value
                                                        from apps.fnd_profile_options_vl            fpo
                                                            ,apps.fnd_profile_option_values         fpov
                                                        where fpov.profile_option_id                 = fpo.profile_option_id
                                                          and fpo.profile_option_name                = 'BRA_HR_HIERARQUIA_GERENCIAL'
                                                          and to_char(pos.organization_structure_id) = fpov.profile_option_value)
        and papf.employee_number in (?)
        and pj.job_id = ?
       order by hapf.name"

    CorePosition.find_by_sql(sql, manager_employee_number, self.job_id)
  end
end
