class ReportPlanning < Struct.new(:name, :status, :current_team, :author_summary, :hr_summary)
  def self.build(planning, author)
    author_proposal = MovementProposal.find_or_initialize_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, author.id, nil)
    hr_proposal = MovementProposal.find_or_initialize_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, author.id, author_proposal.id)

    new(
      author.full_name,
      author_proposal.status_text,
      author.countable_collaborators.count,
      Summary.build(author_proposal.proposed_movements.countables),
      Summary.build(hr_proposal.proposed_movements.countables)
    )
  end

  def +(report_planning)
    self.class.new(
      nil,
      nil,
      current_team + report_planning.current_team,
      author_summary + report_planning.author_summary,
      hr_summary + report_planning.hr_summary
    )
  end
end