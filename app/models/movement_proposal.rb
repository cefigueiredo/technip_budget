require "ruby_plsql"

# -*- encoding: utf-8 -*-
class MovementProposal < ActiveRecord::Base
  set_table_name "bra.bra_customhr_move_proposals"
  set_sequence_name "bra.bra_customhr_move_proposals_s"

  belongs_to :manager,
    :class_name => "User",
    :primary_key => "employee_person_id",
    :foreign_key => "manager_id"

  belongs_to :director,
    :class_name => "User",
    :primary_key => "employee_person_id",
    :foreign_key => "director_id"

  belongs_to :owner,
    :class_name => "User",
    :primary_key => "employee_person_id",
    :foreign_key => "created_by"

  belongs_to :planning,
    :class_name => "MovementPlanning",
    :foreign_key => "planning_id"

  has_many :proposed_movements,
    :class_name => "MovementPosition",
    :foreign_key => "proposal_id"

  belongs_to :parent_proposal,
    :class_name => "MovementProposal",
    :foreign_key => "parent_proposal_id"

  validate :planning, :created_by, :status, :presence => true

  before_create :define_director!

  accepts_nested_attributes_for :proposed_movements, :allow_destroy => true

  named_scope :pending_manager, :conditions => { :status => 'PENDING-MANAGER' }
  named_scope :pending_director, :conditions => { :status => 'PENDING-DIRECTOR' }
  named_scope :pending_hr, :conditions => { :status => 'PENDING-HR' }
  named_scope :rejected, :conditions => { :status => ['REJECTED-DIRECTOR', 'REJECTED-DIRECTOR-TIMEOUT'] }
  named_scope :finished, :conditions => { :status => 'FINISHED' }
  named_scope :unfinished, :conditions => ["status != 'FINISHED'"]
  named_scope :approved, :conditions => { :approved_by_hr => 1 }

  def existent_positions
    sql = "
      with hapf_with_grades as (
        SELECT hapf.*
             , cast(pgrf.minimum as number(5)) as minimum
             , cast(pgrf.mid_value as number(5)) as mid_value
             , cast(pgrf.maximum as number(5)) as maximum
          FROM hr_all_positions_f hapf
             , pay_grade_rules_f pgrf
         WHERE hapf.entry_grade_rule_id = pgrf.grade_rule_id
           AND TRUNC(sysdate) BETWEEN hapf.effective_start_date AND hapf.effective_end_date
           AND hapf.availability_status_id <> 5
           AND ((hapf.status <> 'INVALID') OR hapf.status IS NULL)
           AND hapf.business_group_id = 511
           AND TRUNC(SYSDATE) BETWEEN pgrf.effective_start_date AND pgrf.effective_end_date
      )
      select DISTINCT
          hapf.position_id
          ,pj.job_id job_id
          ,haou.organization_id
          ,hapf.name position
          ,pj.name job
          ,haou.name uniorg
          ,hapf.minimum
          ,hapf.mid_value
          ,hapf.maximum
      from hapf_with_grades                    hapf
          ,hr.per_jobs                          pj
          ,hr.hr_all_organization_units        haou
          ,apps.per_org_structure_elements_v    pose
          ,hr.per_all_people_f                  papf
          ,hr.per_all_assignments_f             paaf
          ,hr.per_periods_of_service            ppos
          ,hr.per_organization_structures       pos
          ,hr.per_org_structure_versions        posv
          ,hr.per_job_definitions               pjd
      where papf.business_group_id              = 511
      and paaf.business_group_id               = 511
      and paaf.primary_flag                    = 'Y'
      and paaf.assignment_type                 = 'E'
      and hapf.job_id                          = pj.job_id
      and hapf.organization_id                 = haou.organization_id
      and papf.person_id                       = paaf.person_id
      and papf.person_id                       = ppos.person_id
      and pjd.job_definition_id                = pj.job_definition_id
      and pose.organization_id_parent          = paaf.organization_id
      and hapf.organization_id                 = pose.organization_id_child
      and pose.org_structure_version_id        = posv.org_structure_version_id
      and pos.organization_structure_id        = posv.organization_structure_id
      and hapf.availability_status_id          <> 5
      and (     posv.date_to                   is null
            or  posv.date_to                   >= sysdate   )
      and (     pj.date_to                     is null
            or  pj.date_to                     >= sysdate   )
      and ((hapf.status <> 'INVALID')          or hapf.status is null)
      and (ppos.actual_termination_date        is null or ppos.actual_termination_date > sysdate)
      and sysdate between hapf.effective_start_date and hapf.effective_end_date
      and sysdate between papf.effective_start_date and papf.effective_end_date
      and sysdate between paaf.effective_start_date and paaf.effective_end_date
      and to_char(pos.organization_structure_id) in (select fpov.profile_option_value
                                                      from apps.fnd_profile_options_vl            fpo
                                                          ,apps.fnd_profile_option_values         fpov
                                                      where fpov.profile_option_id                 = fpo.profile_option_id
                                                        and fpo.profile_option_name                = 'BRA_HR_HIERARQUIA_GERENCIAL'
                                                        and to_char(pos.organization_structure_id) = fpov.profile_option_value)
    "

    authority_condition = ""
    if manager.site != '3'
      authority_condition = self.class.sanitize([" and papf.employee_number = ?", manager.employee_number])
    else
      employee_numbers_group = manager.supervisors.map(&:employee_number) << manager.employee_number
      authority_condition = self.class.sanitize([" and papf.employee_number in (?)", employee_numbers_group])
    end

    eligibility_condition = ""
    eligibility_condition << self.class.sanitize([" and hapf.job_id not in (?) ", planning.ineligible_job_ids]) if planning.ineligible_job_ids.any?
    eligibility_condition << self.class.sanitize([" and hapf.position_id not in (?) ", planning.ineligible_position_ids]) if planning.ineligible_position_ids.any?

    ordering = "order by pj.name"

    sql = sql + authority_condition + eligibility_condition + ordering
    jobs = connection.select_all(sql)
  end

  def inexistent_positions
    sql = "
      select 'INEXISTENT_' || bcpr.id position_id
           , bcpr.job_id
           , bcpr.organization_id
           , bcpr.position_name position
           , pj.name job
           , haou.name uniorg
           , 8 minimum
           , 10 mid_value
           , 12 maximum
        from bra.bra_customhr_position_reqs bcpr
       inner join per_jobs pj                    on pj.job_id = bcpr.job_id
       inner join hr_all_organization_units haou on haou.organization_id = bcpr.organization_id
       where requested_by = #{manager.employee_person_id}
    "
    eligibility_condition = ""
    eligibility_condition << self.class.sanitize([" and pj.job_id not in (?) ", planning.ineligible_job_ids]) if planning.ineligible_job_ids.any?

    sql = sql + eligibility_condition

    jobs = connection.select_all(sql)
  end

  def subordinates_jobs
    inexistent_positions + existent_positions
  end

  def movements_by_previous_position(position_id)
    movements = proposed_movements

    movements.select do |movement|
      if movement.iteration == 'HIRE'
        positionId = movement.position_request_id ? "INEXISTENT_#{movement.position_request_id}" : movement.position_id
      else
        positionId = movement.previous_position_id
      end

      positionId == position_id
    end
  end

  def movements_job_hierarchy
    job_hierarchy = proposed_movements.map { |mov| mov.job_hierarchy }

    job_hierarchy.flatten.uniq
  end

  def self.sanitize(args)
    sanitize_sql_for_assignment(args)
  end

  def unified_job_hierarchy
    jobs = subordinates_jobs + movements_job_hierarchy
    jobs.flatten.uniq
  end

  def grouped_job_hierarchy
    unified_job_hierarchy.group_by { |position| position["job"] }
  end

  def serialize
    define_director!
    movements = list_movements!
    uncountable_collaborators_ids = manager.uncountable_collaborators.map(&:id)

    manager_collaborators = manager.eligible_collaborators.map do |user|
      user.attributes.tap do |usr|
        usr["countable"] = !uncountable_collaborators_ids.include?(user.employee_person_id)
      end
    end

    attributes.merge({
      :manager   => manager.attributes.merge({
          :is_director => manager.is_director?
        }),
      :director => director.attributes,
      :planning  => planning.attributes,
      :parent_proposal => (parent_proposal.serialize if parent_proposal) || {},
      :jobs => grouped_job_hierarchy.map do |job|
        job = { :name => job[0], :positions => job[1] }
        {
          :name          => job[:name],
          :job_id        => job[:positions].first['job_id'],
          :previous_team => job[:positions].map do |pos|
            movements.select {|mov| !mov.hire? && !mov.uncountable && mov.previous_position_id == pos['position_id'] }
          end.flatten ,
          :current_team  => manager_collaborators.select { |user| user["job_name"] == job[:name] && user["countable"] },
          :positions     => job[:positions].map do |position|
            position.merge({
              :movements    => movements_by_previous_position(position['position_id']).map do |mov|
                mov.attributes.merge({
                  :collaborator_number => (mov.collaborator.employee_number if mov.collaborator),
                  :collaborator_name => (mov.collaborator_name),
                  :position_request => (mov.position_request.serialize if mov.position_request)
                })
              end
            })
          end,
          :uncountable => planning.uncountable_job_ids.include?(job[:positions].first['job_id'])
        }
      end,
      :status_message => I18n.t("interface.proposal.status.#{status}")
    })
  end

  def pending_manager?
    ['PENDING-MANAGER', 'REJECTED-DIRECTOR', 'REJECTED-DIRECTOR-TIMEOUT'].include?(self.status)
  end

  def pending_director?
    self.status == 'PENDING-DIRECTOR'
  end

  def pending_hr?
    self.status == 'PENDING-HR'
  end

  def finished?
    self.status == 'FINISHED'
  end

  def submit_to_approval(current_user)
    if self.pending_manager?
      self.status = 'PENDING-DIRECTOR'
      save_resume(current_user)
      self.save!
      start_workflow
    end
  end

  def approve(manager)
    if status == 'PENDING-HR'
      self.status = 'FINISHED'
      self.approved_by_hr = 1
      self.proposed_movements = self.list_movements!
      save_resume(manager)

      changes = proposed_movements.select do |movement|
        movement.quantity != movement.quantity_approved
      end
      start_cut_workflow unless changes.empty?

      self.save!
      if parent_proposal
        parent_proposal.status = 'FINISHED'
        parent_proposal.save
      end
    end
  end

  def save_resume(current_user)
    MovementResume.delete_all(['proposal_id = ?', self.id])

    grouped_job_hierarchy.map do | job |
      positions = job[1]
      job_id = positions.first['job_id']

      MovementResume.create({
        :planning   => planning,
        :proposal   => self,
        :manager    => manager,
        :director   => director,
        :owner      => current_user,
        :job_id     => job_id,
        :actual     => proposed_movements.countables.all(
          :conditions => { :previous_job_id => job_id }
        ).count,
        :planned    => positions.reduce(0) do | sum, pos|
            count = 0
            movements = []
            position_id = pos['position_id']

            if(pos['position_id'].to_i != 0)
              movements = proposed_movements.countables.all(
                :conditions => [
                  'previous_position_id = ? OR position_id = ?',
                  position_id, position_id
                ]
              )
            else
              position_id = pos['position_id'].split('-')[1].to_i
              movements = proposed_movements.countables.all(
                :conditions => ['position_request_id = ?', position_id]
              )
            end
            movements.each do | mov |
              count   += mov.quantity if mov.hire?
              count   += 1 if mov.maintenance? || mov.promotion_in?(position_id)

              count   += 0 if mov.fired? || mov.promotion_out?(position_id)
            end
            sum + count
          end,
        :approved   => positions.reduce(0) do | sum, pos |
            count = 0
            movements = []
            position_id = pos['position_id']

            if(pos['position_id'].to_i != 0)
              movements = proposed_movements.countables.all(
                :conditions => [
                  'previous_position_id = ? OR position_id = ?',
                  position_id, position_id
                ]
              )
            else
              position_id = pos['position_id'].split('-')[1].to_i
              movements = proposed_movements.countables.all(
                :conditions => ['position_request_id = ?', position_id]
              )
            end
            movements.each do | mov |
              count += mov.quantity_approved if mov.hire?
              count += mov.quantity_approved if mov.maintenance? || mov.promotion_in?(position_id)

              count += 0 if mov.fired? || mov.promotion_out?(position_id)
            end

            sum + count
          end
      })
    end
  end

  def start_workflow
    plsql.connection = ActiveRecord::Base.connection.raw_connection
    begin
      plsql.bra_hr_orc_wf_3.cria_workflow(
        :p_solicitante_id => self.manager_id,
        :p_budget_id      => self.id,
        :p_owner_id       => self.created_by)

    rescue NativeException => native_exception
      raise ActiveRecord::RollBack
    end
  end

  def start_cut_workflow
    plsql.connection = ActiveRecord::Base.connection.raw_connection
    begin
      plsql.bra_hr_orc_wf_3.notifica_corte(
        :p_solicitante_id => self.manager_id,
        :p_budget_id      => self.id,
        :p_owner_id       => self.created_by)

    rescue NativeException => native_exception
      raise ActiveRecord::RollBack
    end

  end

  def list_movements!(proposed_by_hr=false)
    if proposed_movements.empty?
      build_movements!
    else
      synchronize_movements if synchronizable?
      proposed_movements
    end
  end

  def synchronizable?
    if parent_proposal
      pending_hr?
    else
      pending_manager?
    end
  end

  def status_text
    if self.status
      I18n.t("interface.proposal.status.#{self.status}")
    else
      I18n.t("interface.proposal.status.PENDING-MANAGER")
    end
  end

  def self.summaries(planning_id, user, params)
    director_condition = "AND UPPER(dir.full_name) like UPPER('%#{sanitize_sql(["%s", params[:director]])}%')"
    site_condition = ""

    if user.site != '2'
      site_condition += "AND haou.attribute1 = #{user.site}"
    end

    ordering = "\nORDER BY manager_name"

    core_sql = "
      WITH manager_headcount as (
        SELECT bcmp.manager_id
             , bcmp.status
             , SUM(CASE WHEN bcmp.status in ('PENDING-HR', 'FINISHED')
                        THEN CASE WHEN iteration = 'FIRED'
                                  THEN -1 * quantity
                                  ELSE quantity
                              END
                        ELSE null
                    END) as diff_headcount
          FROM bra.bra_customhr_move_proposals bcmp
          LEFT JOIN bra.bra_customhr_move_positions bcmps ON bcmps.proposal_id = bcmp.id
         WHERE bcmp.planning_id = #{planning_id}
           AND bcmp.parent_proposal_id is NULL
           AND bcmps.uncountable = 0
         GROUP BY bcmp.manager_id, bcmp.status
      ),
      finished_headcount as (
        SELECT man_h.manager_id
             , bcmp.status
             , SUM(CASE WHEN iteration = 'FIRED' THEN (-1 * quantity_approved) ELSE quantity_approved END) as diff_approved_headcount
             , SUM(CASE WHEN iteration = 'FIRED' THEN (-1 * quantity_approved) ELSE quantity_approved END) - man_h.diff_headcount as diff_cut_headcount
          FROM bra.bra_customhr_move_proposals bcmp
         INNER JOIN bra.bra_customhr_move_positions bcmps ON bcmps.proposal_id = bcmp.id
         INNER JOIN manager_headcount man_h on man_h.manager_id = bcmp.manager_id
         WHERE bcmp.planning_id = #{planning_id}
           AND bcmp.parent_proposal_id is not NULL
           AND bcmp.status in ('PENDING-HR', 'FINISHED')
           AND bcmps.iteration IN ('FIRED', 'HIRE')
           AND bcmps.uncountable = 0
         GROUP BY man_h.manager_id, bcmp.status, man_h.diff_headcount
      )
      SELECT bcmv.employee_person_id as manager_id
           , bcmv.full_name as manager_name
           , dir.employee_person_id as director_id
           , dir.full_name as director_name
           , haou.name as uniorg
           , haou.attribute20 as cod_uniorg
           , coalesce(m_h.status, 'PENDING-MANAGER') as status
           , COALESCE(TO_CHAR(m_h.diff_headcount), '-') AS diff_headcount
           , COALESCE(TO_CHAR(f_h.diff_cut_headcount), '-') AS diff_cut_headcount
           , COALESCE(TO_CHAR(f_h.diff_approved_headcount), '-') AS diff_approved_headcount
        FROM bra_customhr_managers_v bcmv
       INNER JOIN hr_all_organization_units haou on haou.organization_id = bcmv.organization_id
        LEFT JOIN bra_customhr_collaborators_v dir on dir.employee_person_id = BRA_INFOHR.BRA_RETORNA_DADOSHR('DIRETOR',NULL,bcmv.employee_person_id,null)
        LEFT JOIN manager_headcount m_h ON m_h.manager_id = bcmv.employee_person_id
        LEFT JOIN finished_headcount f_h ON f_h.manager_id = bcmv.employee_person_id
       WHERE UPPER(bcmv.full_name) like UPPER('%#{sanitize_sql(['%s', params[:manager]])}%')
         AND haou.attribute1 != '3'
         AND coalesce(m_h.status, 'PENDING-MANAGER') like '%#{sanitize_sql(['%s', params[:status] != 'all' ? params[:status] : nil])}%'
         #{director_condition}
         #{site_condition}

       UNION -- Managers from site Vitoria
      SELECT bcmv.employee_person_id as manager_id
           , bcmv.full_name as manager_name
           , dir.employee_person_id as director_id
           , dir.full_name as director_name
           , haou.name as uniorg
           , haou.attribute20 as cod_uniorg
           , coalesce(m_h.status, 'PENDING-MANAGER') as status
           , COALESCE(TO_CHAR(m_h.diff_headcount), '-') AS diff_headcount
           , COALESCE(TO_CHAR(f_h.diff_cut_headcount), '-') AS diff_cut_headcount
           , COALESCE(TO_CHAR(f_h.diff_approved_headcount), '-') AS diff_approved_headcount
        FROM bra_customhr_managers_v bcmv
       INNER JOIN hr_all_organization_units haou on haou.organization_id = bcmv.organization_id
        LEFT JOIN bra_customhr_collaborators_v dir on dir.employee_person_id = BRA_INFOHR.BRA_RETORNA_DADOSHR('DIRETOR',NULL,bcmv.employee_person_id,null)
        LEFT JOIN manager_headcount m_h ON m_h.manager_id = bcmv.employee_person_id
        LEFT JOIN finished_headcount f_h ON f_h.manager_id = bcmv.employee_person_id
       WHERE UPPER(bcmv.full_name) like UPPER('%#{sanitize_sql(['%s', params[:manager]])}%')
         AND haou.attribute1 = '3'
         AND haou.type in (8, 23, 12, 15, 19, 24, 16)
         AND coalesce(m_h.status, 'PENDING-MANAGER') like '%#{sanitize_sql(['%s', params[:status] != 'all' ? params[:status] : nil])}%'
         #{director_condition}
         #{site_condition}
         "

    sql = core_sql + ordering
    self.connection.select_all(sql)
  end

  protected
    def define_director!
      self.director = if manager.is_director? || manager.president?
        manager
      else
        manager.director
      end
    end

    def build_maintenance_movement(collaborator)
      uncountable_collaborator_ids = manager.uncountable_collaborators.map(&:employee_person_id)

      proposed_movements.build(
        :position_id          => collaborator.position_id,
        :previous_position_id => collaborator.position_id,
        :collaborator         => collaborator,
        :begin_date           => planning.min_begin_date,
        :end_date             => planning.max_end_date,
        :quantity             => 1,
        :quantity_approved    => 1,
        :proposed_by_hr       => approved_by_hr,
        :iteration            => "MAINTENANCE",
        :uncountable          => uncountable_collaborator_ids.include?(collaborator.employee_person_id)
      )
    end

    def build_movements!
      if parent_proposal
        movements_by_collaborator_id = parent_proposal.proposed_movements.reduce(Hash.new) do |hash_mov, mov|
          hash_mov[mov.collaborator_id] = [] if !hash_mov[mov.collaborator_id]
          hash_mov[mov.collaborator_id] << mov
          hash_mov
        end

        manager.eligible_collaborators.each do |collab|
          movement = movements_by_collaborator_id[collab.id].first if movements_by_collaborator_id[collab.id]
          if movement
            proposed_movements << movement.clone.tap do |copied|
              copied.proposal_id = id
              copied.quantity_approved = copied.quantity
            end
          else
            build_maintenance_movement(collab)
          end
        end
        #HIRES
        if movements_by_collaborator_id[nil]
          movements_by_collaborator_id[nil].each do |movement|
            proposed_movements << movement.clone.tap do |copied|
              copied.proposal_id = id
              copied.quantity_approved = copied.quantity_approved
            end
          end
        end
      else
        manager.eligible_collaborators.each do |collaborator|
          build_maintenance_movement(collaborator)
        end
      end
      proposed_movements
    end

    def synchronize_movements
      eligible_collaborators_ids = manager.eligible_collaborators.map(&:id)
      collaborator_ids_with_movements = proposed_movements.not_hire.map(&:collaborator_id)

      proposed_movements.not_hire.each do |movement|
        movement.destroy if !eligible_collaborators_ids.include?(movement.collaborator_id)
      end

      manager.eligible_collaborators.each do |collab|
        if !collaborator_ids_with_movements.include?(collab.id)
          build_maintenance_movement(collab)
        end
      end
      proposed_movements
    end
end
