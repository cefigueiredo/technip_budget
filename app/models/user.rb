# -*- encoding: utf-8 -*-
class User < ActiveRecord::Base
  set_table_name "apps.bra_customhr_collaborators_v"
  self.primary_key = "employee_person_id"

  has_many :direct_collaborators,
    :class_name => "User",
    :foreign_key => "manager_person_id",
    :primary_key => "employee_person_id",
    :order => "full_name"

  belongs_to :core_job, :foreign_key => 'job_id', :primary_key => 'job_id'
  belongs_to :core_position, :foreign_key => 'position_id', :primary_key => 'position_id'
  belongs_to :core_organization_unit, :foreign_key => 'organization_id', :primary_key => 'organization_id'

  def uncountable_collaborators
    current_planning = MovementPlanning.current

    uncountable_job_ids = current_planning.uncountable_job_ids
    impatriate_user_ids = User.find_all_impatriates.map(&:employee_person_id)

    collaborators.select do |collab|
      uncountable_job_ids.include?(collab.job_id) || impatriate_user_ids.include?(collab.employee_person_id)
    end
  end

  def countable_collaborators
    eligible_collaborators - uncountable_collaborators
  end

  def eligible_collaborators
    collaborators - ineligible_collaborators
  end

  def ineligible_collaborators
    current_planning = MovementPlanning.current

    ineligible_job_ids = current_planning.ineligible_job_ids
    ineligible_uniorg_ids = current_planning.ineligible_uniorg_ids

    self.collaborators.select do |collab|
      ineligible_job_ids.include?(collab.job_id) ||
      ineligible_uniorg_ids.include?(collab.organization_id)
    end
  end

  def collaborators
    return direct_collaborators if site != '3' && manager?

    self.class.find_by_sql(["
      SELECT bccv.*
        FROM bra_customhr_collaborators_v bccv
       INNER join hr_all_organization_units haou ON UPPER(haou.name) = UPPER(bccv.dept_name)
       START WITH bccv.manager_person_id = #{self.employee_person_id}
     CONNECT BY PRIOR bccv.employee_person_id = bccv.manager_person_id
         AND PRIOR haou.type not in (8, 23, 12, 15, 19, 24, 16)
    "])
  end

  def manager?
    return direct_collaborators.any? if site != '3'

    manager_dept = self.class.connection.select_value("
      select count(1)
        from hr_all_organization_units haou
       where haou.TYPE in (8, 23, 12, 15, 19, 24, 16)
         and UPPER(haou.name) = UPPER('#{self.dept_name}')
    ")
    !!manager_dept.nonzero?
  end

  def supervisor?
    return false if site != '3'

    supervision_dept = self.class.connection.select_value("
      select count(1)
        from hr_all_organization_units haou
       where haou.TYPE not in (8, 23, 12, 15, 19, 24, 16)
         and UPPER(haou.name) = UPPER('#{self.dept_name}')
    ")

    direct_collaborators.any? && supervision_dept.nonzero?
  end

  def is_director?
    count = ActiveRecord::Base.connection.select_value("
      SELECT count(*)
      FROM per_all_assignments_f paaf
          ,per_organization_units_v pouv
          ,apps.bra_customhr_collaborators_v bcev
          ,hr_all_organization_units haou
      WHERE paaf.organization_id = pouv.organization_id
      AND paaf.person_id = bcev.employee_person_id
      AND bcev.dept_name = haou.name
      AND sysdate BETWEEN paaf.effective_start_date AND paaf.effective_end_date
      AND paaf.assignment_type = 'E'
      AND paaf.primary_flag = 'Y'
      AND bcev.employee_person_id = #{self.employee_person_id}
      AND UPPER(pouv.organization_type) LIKE UPPER('DIRETORIA%')
      ORDER BY bcev.full_name")

    return (!count.blank? and !count.to_i.zero?)
  end

  def president?
    count = ActiveRecord::Base.connection.select_value("
      SELECT count(*)
      FROM per_all_assignments_f paaf
          ,per_organization_units_v pouv
          ,apps.bra_customhr_collaborators_v bcev
          ,hr_all_organization_units haou
      WHERE paaf.organization_id = pouv.organization_id
      AND paaf.person_id = bcev.employee_person_id
      AND bcev.dept_name = haou.name
      AND sysdate BETWEEN paaf.effective_start_date AND paaf.effective_end_date
      AND paaf.assignment_type = 'E'
      AND paaf.primary_flag = 'Y'
      AND bcev.employee_person_id = #{self.employee_person_id}
      AND UPPER(pouv.organization_type) LIKE UPPER('PRESIDENCIA%')
      ORDER BY bcev.full_name")

    return (!count.blank? and !count.to_i.zero?)
  end

  def site
    ActiveRecord::Base.connection.select_value("
      SELECT haou.attribute1 site
      FROM hr_all_organization_units haou
      WHERE UPPER(haou.name) = UPPER('#{self.dept_name}')")
  end

  def director
    director_person_id = ActiveRecord::Base.connection.select_value("
      SELECT apps.BRA_INFOHR.BRA_RETORNA_DADOSHR('DIRETOR',NULL,#{self.employee_person_id},null)
        FROM DUAL")

    User.find(director_person_id) unless director_person_id.blank?
  end

  def managers
    collaborators.select do |collab|
      collab.manager?
    end
  end

  def nested_managers_id
    return [] unless self.manager?

    connection.select_values("
      SELECT employee_person_id
        FROM bra_customhr_managers_v bcmv
       INNER JOIN hr_all_organization_units haou ON UPPER(haou.name) = UPPER(bcmv.dept_name)
       WHERE haou.attribute1 != '3'
       START WITH manager_person_id = #{self.id}
     CONNECT BY PRIOR employee_person_id = manager_person_id

       UNION
      -- NESTED MANAGERS for managers which site is VITORIA
      SELECT employee_person_id
        FROM bra_customhr_managers_v bcmv
       INNER JOIN hr_all_organization_units haou ON UPPER(haou.name) = UPPER(bcmv.dept_name)
       WHERE haou.attribute1 = '3'
         AND haou.type in (8, 23, 12, 15, 19, 24, 16)
       START WITH manager_person_id = #{self.id}
     CONNECT BY PRIOR employee_person_id = manager_person_id
    ")
  end

  def supervisors
    collaborators.select {|collab| collab.supervisor? }
  end

  def diff_headcount(planning_id)
    sql = "
      SELECT SUM(CASE WHEN iteration = 'FIRED' THEN -1 * quantity ELSE quantity END)
        FROM bra.bra_customhr_move_proposals bcmp
            ,bra.bra_customhr_move_positions bcmps
        WHERE bcmp.id = bcmps.proposal_id
        AND bcmp.planning_id = #{planning_id}
        AND bcmp.parent_proposal_id IS NULL
        AND bcmp.status in ('PENDING-HR', 'FINISHED')
        AND bcmps.iteration IN ('FIRED', 'HIRE')
        AND bcmps.uncountable = 0
        AND (
          bcmp.manager_id = #{self.id} OR bcmp.manager_id IN (SELECT employee_person_id
                                                          FROM apps.bra_customhr_managers_v
                                                          START WITH manager_person_id = #{self.id}
                                                          CONNECT BY PRIOR employee_person_id = manager_person_id))
    "
    connection.select_value(sql).to_i
  end

  def approved_headcount(planning_id)
    sql = "
      SELECT SUM(CASE WHEN iteration = 'FIRED' THEN (-1 * quantity_approved) ELSE quantity_approved END)
        FROM bra.bra_customhr_move_proposals bcmp
            ,bra.bra_customhr_move_positions bcmps
        WHERE bcmp.id = bcmps.proposal_id
        AND bcmp.planning_id = #{planning_id}
        AND bcmp.approved_by_hr = 1
        and bcmp.status in ('PENDING-HR', 'FINISHED')
        AND bcmps.iteration IN ('FIRED', 'HIRE')
        AND bcmps.uncountable = 0
        AND (
          bcmp.manager_id = #{self.id} OR bcmp.manager_id IN (SELECT employee_person_id
                                                          FROM apps.bra_customhr_managers_v
                                                          START WITH manager_person_id = #{self.id}
                                                          CONNECT BY PRIOR employee_person_id = manager_person_id))
    "

    connection.select_value(sql).to_i
  end

  def cut_headcount(planning_id)
    sql = "
      SELECT SUM(
                CASE WHEN iteration = 'FIRED'
                     THEN (-1 * quantity_approved) - (-1 * quantity)
                ELSE quantity_approved - quantity END )
        FROM bra.bra_customhr_move_proposals bcmp
            ,bra.bra_customhr_move_positions bcmps
        WHERE bcmp.id = bcmps.proposal_id
        AND bcmp.planning_id = #{planning_id}
        AND bcmp.approved_by_hr = 1
        and bcmp.status in ('PENDING-HR', 'FINISHED')
        AND bcmps.iteration IN ('FIRED', 'HIRE')
        AND bcmps.uncountable = 0
        AND (
          bcmp.manager_id = #{self.id} OR bcmp.manager_id IN (SELECT employee_person_id
                                                          FROM apps.bra_customhr_managers_v
                                                          START WITH manager_person_id = #{self.id}
                                                          CONNECT BY PRIOR employee_person_id = manager_person_id))
    "

    connection.select_value(sql).to_i
  end

  def diff_headcount_director(planning_id)
    sql = "
      SELECT SUM(CASE WHEN iteration = 'FIRED' THEN -1 * quantity ELSE quantity END)
        FROM bra.bra_customhr_move_proposals bcmp
            ,bra.bra_customhr_move_positions bcmps
        WHERE bcmp.id = bcmps.proposal_id
        AND bcmp.planning_id = #{planning_id}
        AND bcmp.parent_proposal_id IS NULL
        AND bcmp.status in ('FINISHED', 'PENDING-HR')
        AND bcmps.iteration IN ('FIRED', 'HIRE')
        AND bcmps.uncountable = 0
        AND bcmp.director_id = #{self.id}
    "
    connection.select_value(sql).to_i
  end

  def approved_headcount_director(planning_id)
    sql = "
      SELECT SUM(CASE WHEN iteration = 'FIRED' THEN (-1 * quantity_approved) ELSE quantity_approved END)
        FROM bra.bra_customhr_move_proposals bcmp
            ,bra.bra_customhr_move_positions bcmps
        WHERE bcmp.id = bcmps.proposal_id
        AND bcmp.planning_id = #{planning_id}
        AND bcmp.approved_by_hr = 1
        AND bcmp.status in ('PENDING-HR', 'FINISHED')
        AND bcmps.iteration IN ('FIRED', 'HIRE')
        AND bcmps.uncountable = 0
        AND bcmp.director_id = #{self.id}
    "

    connection.select_value(sql).to_i
  end

  def cut_headcount_director(planning_id)
    sql = "
      SELECT SUM(
                CASE WHEN iteration = 'FIRED'
                     THEN (-1 * quantity_approved) - (-1 * quantity)
                ELSE quantity_approved - quantity END )
        FROM bra.bra_customhr_move_proposals bcmp
            ,bra.bra_customhr_move_positions bcmps
        WHERE bcmp.id = bcmps.proposal_id
        AND bcmp.planning_id = #{planning_id}
        AND bcmp.approved_by_hr = 1
        AND bcmp.status in ('PENDING-HR', 'FINISHED')
        AND bcmps.iteration IN ('FIRED', 'HIRE')
        AND bcmps.uncountable = 0
        AND bcmp.director_id = #{self.id}
    "
    connection.select_value(sql).to_i
  end

  def count_hires(planning_id)
    sql = "
      SELECT SUM(quantity)
        FROM bra.bra_customhr_move_proposals bcmp
            ,bra.bra_customhr_move_positions bcmps
        WHERE bcmp.id = bcmps.proposal_id
        AND bcmp.planning_id = #{planning_id}
        AND bcmp.parent_proposal_id IS NULL
        AND bcmps.iteration = 'HIRE'
        AND bcmps.uncountable = 0
        AND (
          bcmp.manager_id = #{self.id} OR bcmp.manager_id IN (SELECT employee_person_id
                                                          FROM apps.bra_customhr_managers_v
                                                          START WITH manager_person_id = #{self.id}
                                                          CONNECT BY PRIOR employee_person_id = manager_person_id))
    "
    connection.select_value(sql).to_i
  end


  def self.find_all_directors_by_site(site)
    if site == '2' # rio de janeiro
      find_all_directors
    else
      find_directors_by_site(site)
    end
  end

  def self.find_all_presidents_by_site(site)
    if site == '2' # rio de janeiro
      find_all_presidents
    else
      find_presidents_by_site(site)
    end
  end

  def self.find_all_directors
    find_by_sql(["
      SELECT bcev.*
      FROM per_all_assignments_f paaf
          ,per_organization_units_v pouv
          ,apps.bra_customhr_collaborators_v bcev
          ,hr_all_organization_units haou
      WHERE paaf.organization_id = pouv.organization_id
      AND paaf.person_id = bcev.employee_person_id
      AND bcev.dept_name = haou.name
      AND sysdate BETWEEN paaf.effective_start_date AND paaf.effective_end_date
      AND paaf.assignment_type = 'E'
      AND paaf.primary_flag = 'Y'
      AND UPPER(pouv.organization_type) LIKE UPPER(?)
      ORDER BY bcev.full_name", "DIRETORIA%"])
  end

  def self.find_directors_by_site(site)
    find_by_sql(["
      SELECT bcev.*
      FROM per_all_assignments_f paaf
          ,per_organization_units_v pouv
          ,apps.bra_customhr_collaborators_v bcev
          ,hr_all_organization_units haou
      WHERE paaf.organization_id = pouv.organization_id
      AND paaf.person_id = bcev.employee_person_id
      AND bcev.dept_name = haou.name
      AND sysdate BETWEEN paaf.effective_start_date AND paaf.effective_end_date
      AND paaf.assignment_type = 'E'
      AND paaf.primary_flag = 'Y'
      AND UPPER(pouv.organization_type) LIKE UPPER(?)
      AND haou.attribute1 = ?
      ORDER BY bcev.full_name", "DIRETORIA%", site])
  end

  def self.find_all_presidents
    find_by_sql(["
      SELECT bcev.*
      FROM per_all_assignments_f paaf
          ,per_organization_units_v pouv
          ,apps.bra_customhr_collaborators_v bcev
          ,hr_all_organization_units haou
      WHERE paaf.organization_id = pouv.organization_id
      AND paaf.person_id = bcev.employee_person_id
      AND bcev.dept_name = haou.name
      AND sysdate BETWEEN paaf.effective_start_date AND paaf.effective_end_date
      AND paaf.assignment_type = 'E'
      AND paaf.primary_flag = 'Y'
      AND UPPER(pouv.organization_type) LIKE UPPER(?)
      ORDER BY bcev.full_name", "PRESIDENCIA%"])
  end

  def self.find_presidents_by_site(site)
    find_by_sql(["
      SELECT bcev.*
      FROM per_all_assignments_f paaf
          ,per_organization_units_v pouv
          ,bra_customhr_collaborators_v bcev
          ,hr_all_organization_units haou
      WHERE paaf.organization_id = pouv.organization_id
      AND paaf.person_id = bcev.employee_person_id
      AND bcev.dept_name = haou.name
      AND sysdate BETWEEN paaf.effective_start_date AND paaf.effective_end_date
      AND paaf.assignment_type = 'E'
      AND paaf.primary_flag = 'Y'
      AND UPPER(pouv.organization_type) LIKE UPPER(?)
      AND haou.attribute1 = ?
      ORDER BY bcev.full_name", "PRESIDENCIA%", site])
  end

  def self.find_all_impatriates
    find_by_sql(["
      SELECT bccv.*
        from apps.funcionario_dado_Adicional      fda
            ,apps.funcionarios_v                  fu
            ,hr.per_all_people_f                  papf
            ,hr.per_all_assignments_f             paaf
            ,hr.pay_people_groups                 ppg
            ,hr.hr_all_organization_units         haou
            ,apps.bra_customhr_collaborators_v    bccv
       where fda.tdaf_codigo = '212'
         and fu.id = fda.id_funcionario
         and papf.employee_number = to_char(fu.id)
         and papf.person_id = paaf.person_id
         and papf.person_id = bccv.employee_person_id
         and paaf.people_group_id = ppg.people_group_id
         and paaf.organization_id = haou.organization_id
         and trunc(sysdate) between papf.effective_start_date and papf.effective_end_date
         and trunc(sysdate) between paaf.effective_start_date and paaf.effective_end_date
         and paaf.assignment_type = 'E'
         and paaf.primary_flag = 'Y'
         and papf.business_group_id = 511
         and paaf.primary_flag = 'Y'
         and fu.ativo = 'A'
      "])
  end

  def self.find_by_name_or_employee_number(term)
    if term.to_i > 0
      [find_by_employee_number(term.to_i)]
    else
      find_by_sql([%{
        select *
          from apps.bra_customhr_collaborators_v
         where full_name like UPPER('%'|| ? ||'%')
      }, term])
    end
  end
end
