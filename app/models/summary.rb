class Summary < Struct.new(:hires, :fires, :promotions, :total, :delta)
  def self.build(proposed_movements)
    new(
      proposed_movements.hire.reduce(0) { |total, mov| total + mov.quantity_by_context },
      proposed_movements.fired.count,
      proposed_movements.promotion.count,
      total(proposed_movements),
      delta(proposed_movements)
    )
  end

  def +(summary)
    self.class.new(
      hires + summary.hires,
      fires + summary.fires,
      promotions + summary.promotions,
      total + summary.total,
      delta + summary.delta
    )
  end

  private
    def self.total(proposed_movements)
      proposed_movements.reduce(0) do |total, mov|
        if mov.fired?
          total
        elsif mov.hire?
          total + mov.quantity_by_context
        else
          total + 1
        end
      end
    end

    def self.delta(proposed_movements)
      proposed_movements.reduce(0) do |delta, mov|
        if mov.hire?
          delta + mov.quantity_by_context
        elsif mov.fired?
          delta - mov.quantity_by_context
        else
          delta
        end
      end
    end
end
