# -*- encoding: utf-8 -*-
class MovementPlanning < ActiveRecord::Base
  set_table_name "bra.bra_customhr_move_plans"
  set_sequence_name "bra.bra_customhr_move_plans_s"

  has_many :proposals,
    :class_name => "MovementProposal",
    :foreign_key => "planning_id"

  has_many :parameterized_jobs, :class_name  => 'JobParameter', :foreign_key => "planning_id"
  has_many :ineligible_uniorgs, :class_name => 'OrgParameter', :foreign_key => "planning_id"
  has_many :report_rules, :foreign_key => "planning_id"

  validates_presence_of :begin_planning_date, :end_planning_date, :calendar_name, :min_begin_date, :max_end_date
  validate :must_have_consistent_period

  before_save :grant_unicity_for_current!

  accepts_nested_attributes_for :parameterized_jobs, :ineligible_uniorgs, :report_rules, :allow_destroy => true

  def ineligible_jobs
    self.parameterized_jobs.ineligibles.all
  end

  def uncountable_jobs
    self.parameterized_jobs.uncountables.all
  end

  def ineligible_job_ids
    self.ineligible_jobs.map { |job| job.job_id }
  end

  def uncountable_job_ids
    self.uncountable_jobs.map { |job| job.job_id }
  end

  def uncountable_position_ids
    uncountable_jobs.map{ |job| job.core_job.core_position_ids }.flatten
  end

  def ineligible_position_ids
    position_ids = ineligible_jobs.map(&:nested_position_ids)
    position_ids << ineligible_uniorgs.map(&:nested_position_ids)
    position_ids.flatten
  end

  def open_period?
    Date.today.between?(begin_planning_date.to_date, end_planning_date.to_date)
  end

  def self.current
    MovementPlanning.find(:first, :conditions => {
      :current_planning => true
    })
  end

  def available_sites
    connection.select_all("
      select flv.lookup_code as id
           , flv.meaning as name
        from apps.fnd_lookup_value flv
       where flv.lookup_type = 'TC_HR_SITE_LOCAL'
         and flv.LANGUAGE = 'PTB'
    ")
  end

  def get_report_rule(user)
    report_rules.find(:first, :conditions => "user_id = #{user.id}")
  end

  private
    def must_have_consistent_period
      if !self.begin_planning_date.nil? && !self.end_planning_date.nil?
        if self.begin_planning_date > self.end_planning_date
          errors.add("end_planning_date", "Data do final de um planejamento não pode ser menor que a data de início do planejamento.")
        end
      end
    end

    def grant_unicity_for_current!
      current = MovementPlanning.current

      return true unless current

      if self.current_planning == true && self != current
        current.current_planning = false
        current.save
      end
    end
end
