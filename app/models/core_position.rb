# -*- encoding : utf-8 -*-
require "ruby_plsql"

class CorePosition < ActiveRecord::Base
  set_table_name "hr_all_positions_f"
  self.primary_key = 'position_id'

  belongs_to :position, :foreign_key => "position_id"
  belongs_to :core_job, :foreign_key => 'job_id', :primary_key => 'job_id'
  belongs_to :core_organization_unit, :foreign_key => 'organization_id', :primary_key => 'organization_id'

  DEFAULT_CONDITIONS = "
    TRUNC(sysdate) BETWEEN hr_all_positions_f.effective_start_date AND hr_all_positions_f.effective_end_date
    AND hr_all_positions_f.availability_status_id <> 5
    AND ((hr_all_positions_f.status <> 'INVALID') OR hr_all_positions_f.status IS NULL)
    AND hr_all_positions_f.business_group_id = 511"

  default_scope :conditions => DEFAULT_CONDITIONS, :order => 'UPPER(hr_all_positions_f.name) ASC'

  accepts_nested_attributes_for :position

  attr_accessor :clone_id, :job_name, :original_name, :organization_name
  attr_accessor :has_collaborators, :has_unfinished_workflows, :has_available_openings, :joined_with_budget_system
  # composite the core position name
  attr_accessor :area, :complement, :company, :job_name_formatted, :organization_name_formatted

  attr_accessor :cloned_position_id

  def self.competencies_by_position_id(position_id)
    unless position_id.blank?
      connection.select_values "
        SELECT UPPER(competences.name)
        FROM per_competence_elements elements,
             per_competences competences
        WHERE elements.competence_id = competences.competence_id
        AND elements.effective_date_to IS NULL
        #{sanitize_sql_for_assignment(["AND elements.position_id = ?", "#{position_id}"])}
        ORDER BY competences.name ASC"
    end
  end

  def competencies
    self.class.competencies_by_position_id(self.position_id)
  end

  def job_name_formatted
    @job_name_formatted ||= self.core_job.name_formatted
  end

  def job_name
    @job_name ||= self.core_job.name
  end

  def organization_name_formatted
    @organization_name_formatted ||= self.core_organization_unit.name_formatted
  end

  def organization_name
    @organization_name ||= self.core_organization_unit.name
  end

  def original_name
    @original_name ||= self.name
  end

  def entry_grade_name
    @entry_grade_name ||= get_entry_grade_name
  end

  def pay_basis_name
    @pay_basis_name ||= get_pay_basis_name
  end

  def entry_grade_rule_name
    @entry_grade_rule_name ||= get_entry_grade_rule_name
  end

  def pay_freq_payroll_name
    @pay_freq_payroll_name ||= get_pay_freq_payroll_name
  end

  def position_type_name
    @position_type_name ||= get_position_type_name
  end

  def self.options_of_part_of_name(part, args)
    send("options_of_#{part}", args)
  end

  def self.options_of_areas(name)
    connection.select_values("
      SELECT flv.meaning
       FROM fnd_lookup_values flv
       WHERE flv.lookup_type = 'TC_HR_COMPLEMENTO_AREA'
        AND flv.LANGUAGE = 'PTB'
        AND flv.enabled_flag = 'Y'
        #{sanitize_sql_for_assignment(["AND UPPER(flv.meaning) LIKE UPPER(?)", "%#{name}%"])}
       ORDER BY flv.meaning
    ")
  end

  def self.all_options_of_areas
    connection.select_values("
      SELECT flv.meaning
       FROM fnd_lookup_values flv
       WHERE flv.lookup_type = 'TC_HR_COMPLEMENTO_AREA'
        AND flv.LANGUAGE = 'PTB'
        AND flv.enabled_flag = 'Y'
       ORDER BY flv.meaning
    ")
  end

  def self.all_options_of_sites
    %w(MACAE RIO VITORIA ANGRA)
  end

  def self.options_of_complements(name)
    connection.select_values("
      SELECT DISTINCT segment3
       FROM per_position_definitions
       WHERE segment3 IS NOT NULL
       #{sanitize_sql_for_assignment(["AND UPPER(segment3) LIKE UPPER(?)", "%#{name}%"])}
       ORDER BY segment3")
  end

  def self.all_options_of_companies
    connection.select_rows("
      select flv.description, flv.meaning
       from apps.fnd_lookup_values flv
       where flv.lookup_type = 'BRA_HR_EMPRESA_TRANSFER'
        and flv.enabled_flag = 'Y'
        and flv.language = 'PTB'
        ORDER BY flv.meaning")
  end

  def self.options_of_companies(name)
    connection.select_values("
      SELECT ppg.group_name
       FROM pay_people_groups ppg
       WHERE ppg.enabled_flag = 'Y'
        and segment30 is not null
        and group_name is not null
        #{sanitize_sql_for_assignment(["AND UPPER(ppg.group_name) LIKE UPPER(?)", "%#{name}%"])}
       ORDER BY ppg.group_name
    ")
  end

  def area
    self.name.split('|')[2] unless self.name.blank?
  end

  def complement
    self.name.split('|')[3] unless self.name.blank?
  end

  def company
    self.name.split('|')[4] unless self.name.blank?
  end

  def company_by_uniorg
    people_group = PeopleGroup.by_uniorg(organization_name_formatted)
    if people_group.blank? or people_group.first.blank?
      return company
    else
      return people_group.first['group_name']
    end
  end

  def site
    case self.core_organization_unit.attribute1
    when '1' then return 'MACAE'
    when '2' then return 'RIO'
    when '3' then return 'VITORIA'
    when '4' then return 'ANGRA'
    end
  end

  def clone_in_oracle
    capture_incomplete_attributes # autocomplete fields.
    validate_attributes

    if has_valid_attributes?
      # save cloned position in Oracle (core)
      core_position_id = save_in_oracle
      self.cloned_position_id = core_position_id

      # save cloned position in BRA (custom)
      cloned_position = self.position.clone
      cloned_position.name = self.name
      cloned_position.brazil_code = core_position_id
      cloned_position.save

      # save associations and requirements of cloned position (if necessary)
      save_associations(self.position, cloned_position)

      return true
    else
      return false
    end
  end

  def has_valid_attributes?
    self.errors.blank?
  end

  def save_in_oracle
    core_position_base  = self.class.find(self.clone_id)

    core_position_id        = nil
    effective_start_date    = nil
    effective_end_date      = nil
    position_definition_id  = nil
    object_version_number   = nil

    effective_date = Date.today.beginning_of_month

    name = self.name
    segments = name.split('|')

    plsql.connection = ActiveRecord::Base.connection.raw_connection
    begin
      result = plsql.hr_position_api.create_position(
        :p_position_id                 => core_position_id,       # out nocopy number
        :p_effective_start_date        => effective_start_date,   # out nocopy date
        :p_effective_end_date          => effective_end_date,     # out nocopy date
        :p_position_definition_id      => position_definition_id, # in out nocopy number
        :p_name                        => name,                   # in out nocopy varchar2
        :p_object_version_number       => object_version_number,  # out nocopy number

        :p_job_id                      => self.job_id,                   # in  number
        :p_organization_id             => self.organization_id,          # in  number
        :p_effective_date              => effective_date,                    # in  date
        :p_date_effective              => effective_date,                    # in  date
        :p_availability_status_id      => core_position_base.availability_status_id,   # in  number    default null
        :p_business_group_id           => core_position_base.business_group_id,        # in  number    default null
        :p_entry_step_id               => core_position_base.entry_step_id,            # in  number    default null
        :p_entry_grade_rule_id         => self.entry_grade_rule_id,                    # in  number    default null
        :p_location_id                 => core_position_base.location_id,              # in  number    default null
        :p_pay_freq_payroll_id         => self.pay_freq_payroll_id,                    # in  number    default null
        :p_entry_grade_id              => self.entry_grade_id,                         # in  number    default null

        :p_frequency                   => core_position_base.frequency,                    # in  varchar2  default null
        :p_fte                         => 1,
        :p_max_persons                 => 1,
        :p_permanent_temporary_flag    => core_position_base.permanent_temporary_flag,     # in  varchar2  default null
        :p_permit_recruitment_flag     => core_position_base.permit_recruitment_flag,      # in  varchar2  default null
        :p_position_type               => self.position_type,                              # in  varchar2  default 'NONE'
        :p_replacement_required_flag   => core_position_base.replacement_required_flag,    # in  varchar2  default null
        :p_review_flag                 => core_position_base.review_flag,                  # in  varchar2  default null
        :p_seasonal_flag               => core_position_base.seasonal_flag,                # in  varchar2  default null
        :p_time_normal_finish          => core_position_base.time_normal_finish,           # in  varchar2  default null
        :p_time_normal_start           => core_position_base.time_normal_start,            # in  varchar2  default null
        :p_working_hours               => core_position_base.working_hours,                # in  number    default null
        :p_works_council_approval_flag => core_position_base.works_council_approval_flag,  # in  varchar2  default null
        :p_pay_basis_id                => self.pay_basis_id,                               # in  number    default null

        :p_segment1                    => segments[0],
        :p_segment2                    => segments[1],
        :p_segment3                    => segments[3],
        :p_segment4                    => segments[4],
        :p_segment5                    => get_complementary_area(segments[2])
      )

      # copiando as competências
      plsql.bra_hr_posicao.copy_competences(core_position_base.position_id, result[:p_position_id])

      return result[:p_position_id]

    rescue NativeException => native_exception
      raise ActiveRecord::RollBack
    end
  end

  def save_associations(original_position, cloned_position)
    associations = Association.find_all_by_position_id(original_position.id)

    unless associations.blank?
      associations.each do |original_association|
        # save cloned association
        cloned_association = original_association.clone
        cloned_association.created_at = nil
        cloned_association.updated_at = nil
        cloned_association.created_by = cloned_position.created_by

        cloned_position.associations << cloned_association

        # save cloned association items (requirements)
        original_association.association_items.each do |original_association_item|
          cloned_association_item = original_association_item.clone
          cloned_association_item.created_at = nil
          cloned_association_item.updated_at = nil

          cloned_association.association_items << cloned_association_item
        end

      end
    end
  end

  def self.array_of_position_types
    connection.select_rows("
      SELECT flv.meaning, flv.lookup_code
        FROM apps.fnd_lookup_values flv
        WHERE flv.lookup_type = 'POSITION_TYPE'
        AND flv.language = 'PTB'
        ORDER BY flv.meaning")
  end

  def self.array_of_payroll_names
    connection.select_rows("
      SELECT pap.payroll_name
            ,pap.payroll_id
        FROM hr.pay_all_payrolls_f pap
        ORDER BY pap.payroll_name")
  end

  # tabela salarial
  def self.array_of_table_names
    connection.select_rows("
      SELECT pr.name
            ,pr.rate_id
        FROM pay_rates pr
        WHERE business_group_id = 511
        ORDER BY pr.name")
  end

  # grade salarial de acordo com a tabela
  def self.array_of_grade_names
    connection.select_rows("
      SELECT pg.name
            ,pg.grade_id
        FROM per_grades pg
        WHERE pg.business_group_id = 511
        AND pg.date_to IS NULL
        ORDER BY pg.name")
  end

  # base salarial
  def self.array_of_base_names
    connection.select_rows("
      SELECT ppb.name
            ,ppb.pay_basis_id
        FROM per_pay_bases ppb
        WHERE ppb.business_group_id = 511
        ORDER BY ppb.name")
  end

  def rate_id
    @rate_id ||= get_rate_id
  end

  def rate_id=(value)
    unless value.blank?
      @rate_id = value.to_i
    end
  end

  def get_rate_id
    if !self.entry_grade_id.blank? and !self.entry_grade_rule_id.blank?
      sql = "
        SELECT pgrf.rate_id
          FROM pay_grade_rules_f pgrf
          WHERE pgrf.grade_or_spinal_point_id = #{self.entry_grade_id}
          AND pgrf.grade_rule_id = #{self.entry_grade_rule_id}
          AND TRUNC(sysdate) BETWEEN pgrf.effective_start_date AND pgrf.effective_end_date"

      return ActiveRecord::Base.connection.select_value(sql)
    end
  end

  def get_grade_rule_id
    if !self.entry_grade_id.blank? and !self.rate_id.blank?
      sql = "
        SELECT pgrf.grade_rule_id
          FROM pay_grade_rules_f pgrf
          WHERE pgrf.grade_or_spinal_point_id = #{self.entry_grade_id}
          AND pgrf.rate_id = #{rate_id}
          AND TRUNC(sysdate) BETWEEN pgrf.effective_start_date AND pgrf.effective_end_date"

      return ActiveRecord::Base.connection.select_value(sql)
    end
  end

  def get_grade_rule
    if !self.entry_grade_id.blank? and !self.entry_grade_rule_id.blank?
      GradeRule.get_by_grade(self.entry_grade_id, self.entry_grade_rule_id)
    end
  end

  def joined_with_budget_system?(force_reload = false)
    if force_reload or self.joined_with_budget_system.nil?
      self.joined_with_budget_system = verify_if_joined_with_budget_system
    end

    return self.joined_with_budget_system
  end

  def can_be_destroyed?
    has_not_collaborators        = !self.has_collaborators?
    has_not_unfinished_workflows = !self.has_unfinished_workflows?
    has_not_available_openings   = !self.has_available_openings?

    has_not_collaborators and has_not_unfinished_workflows and has_not_available_openings
  end

  # colaboradores associados
  def has_collaborators?(force_reload = false)
    if force_reload or self.has_collaborators.nil?
      self.has_collaborators = verify_if_has_collaborators
    end

    return self.has_collaborators
  end

  # workflows em processamento
  def has_unfinished_workflows?(force_reload = false)
    if force_reload or self.has_unfinished_workflows.nil?
      self.has_unfinished_workflows = verify_if_has_unfinished_workflows
    end

    return self.has_unfinished_workflows
  end

  # vagas em aberto
  def has_available_openings?(force_reload = false)
    if force_reload or self.has_available_openings.nil?
      self.has_available_openings = verify_if_has_available_openings
    end

    return self.has_available_openings
  end

  def destroy_in_oracle
    if can_be_destroyed?
      plsql.connection = ActiveRecord::Base.connection.raw_connection
      begin
        if joined_with_budget_system?
          # exclui logicamente
          plsql.bra_hr_movimentacao.update_position(:p_position_id => self.position_id)

        else
          # exclui fisicamente
          plsql.hr_position_api.delete_position(
            :p_position_id           => self.position_id,
            :p_object_version_number => self.object_version_number)
        end

        # destroy custom position and all dependencies
        # (associations and association items)
        self.position.try(:destroy)

        return true

      rescue NativeException => native_exception
        raise native_exception
      end
    end

    raise 'This position cannot be destroyed!'
  end

  #protected
    def validate_attributes
      self.errors.clear
      # validating presence of attributes
      validates_presence_of_organization_id # uniorg
      validates_presence_of_core_attributes(%w(job_name name rate_id pay_freq_payroll_id entry_grade_id pay_basis_id position_type))
      validates_presence_of_complementary_area
      # validating uniqueness of attributes
      validates_uniqueness_of_name
      validates_capture_of_grade_rule_id
    end

    def validates_presence_of_complementary_area
      unless self.area.blank?
        area_code = get_complementary_area(self.area)
        if area_code.blank?
          self.errors.add('area', 'Não foi possível encontrar a area complementar informada.')
        end
      end
    end

    def validates_capture_of_grade_rule_id
      if !self.entry_grade_id.blank? and !self.rate_id.blank?
        self.entry_grade_rule_id = get_grade_rule_id
        if self.entry_grade_rule_id.blank?
          self.errors.add('entry_grade_id', 'Não foi possível encontrar a grade para a tabela informada.')
        end
      end
    end

    def validates_presence_of_core_attributes(attrs)
      unless attrs.blank?
        attrs.each do |attr|
          if self.send(attr).blank?
            self.errors.add(attr, I18n.t('activerecord.errors.messages.blank'))
          end
        end
      end
    end

    def validates_presence_of_organization_id
      if self.organization_id.blank? && self.organization_name.blank?
        self.errors.add('organization_name', I18n.t('activerecord.errors.messages.blank'))
      elsif self.organization_id.blank? && !self.organization_name.blank?
        self.errors.add('organization_name', I18n.t('activerecord.errors.messages.does_not_exist'))
      end
    end

    def validates_presence_of_name
      if self.name.blank?
        self.errors.add('name', I18n.t('activerecord.errors.messages.blank'))
      end
    end

    def validates_uniqueness_of_name
      unless self.name.blank?
        exists = CorePosition.where("UPPER (name) = UPPER(?)", self.name).count
        unless exists.zero?
          self.errors.add('name', I18n.t('activerecord.errors.messages.already_exists'))
        end
      end
    end

    def capture_incomplete_attributes
      capture_incomplete_organization_id
    end

    def capture_incomplete_organization_id
      self.organization_id = nil

      unless self.organization_name.blank?
        core_organization_unit = CoreOrganizationUnit.where("UPPER(name) = UPPER(?)", self.organization_name).first
        self.organization_id = core_organization_unit.try(:organization_id)
      end
    end

    def get_complementary_area(segment)
      ActiveRecord::Base.connection.select_value("
        SELECT flv.lookup_code
        FROM apps.fnd_lookup_values flv
        WHERE flv.lookup_type = 'TC_HR_COMPLEMENTO_AREA'
        AND flv.LANGUAGE = 'PTB'
        AND flv.meaning = '#{segment}'")
    end

    def get_entry_grade_name
      sql = "
        SELECT pg.name
        FROM hr.per_grades pg
        WHERE grade_id = #{self.entry_grade_id}"

      ActiveRecord::Base.connection.select_value(sql)
    end

    def get_pay_basis_name
      sql = "
        SELECT ppb.name
        FROM hr.per_pay_bases ppb
        WHERE ppb.pay_basis_id = #{self.pay_basis_id}"

      ActiveRecord::Base.connection.select_value(sql)
    end

    def get_entry_grade_rule_name
      sql = "
        SELECT pr.name
        FROM hr.pay_grade_rules_f pgrf, hr.pay_rates pr
        WHERE pgrf.grade_rule_id = #{self.entry_grade_rule_id}
        AND pgrf.rate_id = pr.rate_id
        AND TRUNC(SYSDATE) BETWEEN pgrf.effective_start_date AND pgrf.effective_end_date"

      ActiveRecord::Base.connection.select_value(sql)
    end

    def grade_rule
      @grade_rule ||= GradeRule.get_by_grade(self.entry_grade_id, self.entry_grade_rule_id)
    end

    def get_pay_freq_payroll_name
      sql = "
        SELECT pap.payroll_name
        FROM hr.pay_all_payrolls_f pap
        WHERE pap.payroll_id = #{self.pay_freq_payroll_id}"

      ActiveRecord::Base.connection.select_value(sql)
    end

    def get_position_type_name
      sql = "
        SELECT flv.meaning
        FROM apps.fnd_lookup_values flv
        WHERE flv.lookup_type(+)   = 'POSITION_TYPE'
        AND flv.lookup_code(+)     = '#{self.position_type}'
        AND flv.language(+)        = 'PTB'"

      ActiveRecord::Base.connection.select_value(sql)
    end

    def verify_if_has_collaborators
      sql = "
        SELECT COUNT(*)
        FROM hr.hr_all_positions_f a
            ,apps.hrfg_employee_assignments b
        WHERE a.position_id = b.position_id
         AND sysdate BETWEEN  a.effective_start_date AND a.effective_end_date
         AND ((a.status <> 'INVALID') OR a.status IS NULL )
         AND a.AVAILABILITY_STATUS_ID <> 5
         AND b.business_group_id = 511
         AND a.position_id = #{self.position_id}"

      result = ActiveRecord::Base.connection.select_value(sql)
      return result.to_i > 0
    end

    def self.unfinished_workflows
      sql = "
        SELECT (
          SELECT iav.number_value posicao
            FROM  apps.wf_item_attribute_values iav
            WHERE iav.item_type = ias.item_type
            AND iav.item_key = ias.item_key
            AND iav.NAME = 'BRA_POSITION_ID') bra_position_id
          ,
          (SELECT iav.number_value posicao
            FROM  apps.wf_item_attribute_values iav
            WHERE iav.item_type = ias.item_type
            AND iav.item_key = ias.item_key
            AND iav.NAME = 'BRA_POSITION_ID_ATUAL') bra_position_id_atual
          FROM WF_ITEM_ACTIVITY_STATUSES_V ias
          WHERE ias.item_type = 'BRAHR'
          AND ias.activity_type_code = 'PROCESS'
          AND ias.ACTIVITY_NAME in ('HR_TRANSFER_JSP_PRC','HR_GENERIC_APPROVAL_PRC')
          AND ias.activity_status_code = 'ACTIVE' "

      active_positions = connection.f(sql)
      active_positions = active_positions.select do |p|
        !p['bra_position_id'].blank? or !p['bra_position_id_atual'].blank?
      end
      active_positions
    end


    def verify_if_has_unfinished_workflows
      sql = "
        SELECT COUNT(*)
         FROM apps.WF_ITEM_ACTIVITY_STATUSES_V ias
              ,(SELECT iav.ITEM_KEY
                      ,iav.ITEM_TYPE
                      ,iav.text_value posicao
                      ,hapf.position_id
                      ,hapf.creation_date
                 FROM  apps.wf_item_attribute_values iav
                      ,hr.hr_all_positions_f hapf
                WHERE ITEM_TYPE = 'BRAHR'
                  AND iav.NAME in ('POSICAO','POSICAO_NOVA')
                  AND iav.TEXT_VALUE IS NOT NULL
                  AND iav.text_value = hapf.NAME
                  ) a
         WHERE ias.item_type = 'BRAHR'
          AND ias.activity_type_code = 'PROCESS'
          AND ias.ACTIVITY_NAME in ('HR_TRANSFER_JSP_PRC','HR_GENERIC_APPROVAL_PRC')
          AND ias.activity_status_code = 'ACTIVE'
          AND ias.item_type = a.item_type
          AND ias.item_key  = a.item_key
          AND a.position_id = #{self.position_id}"

      result = ActiveRecord::Base.connection.select_value(sql)
      return result.to_i > 0
    end

    def verify_if_has_available_openings
      sql = "
        SELECT COUNT(*)
         FROM hr.hr_all_positions_f hapf
             ,hr.per_all_vacancies pav
         WHERE ((hapf.status <> 'INVALID') OR hapf.status IS NULL)
          AND hapf.AVAILABILITY_STATUS_ID <> 5
          AND sysdate BETWEEN hapf.effective_start_date AND hapf.effective_end_date
          AND hapf.position_id = pav.position_id
          AND pav.status = 'APPROVED'
          AND pav.date_to is NULL
          AND hapf.position_id = #{self.position_id}"

      result = ActiveRecord::Base.connection.select_value(sql)
      return result.to_i > 0
    end

    def verify_if_joined_with_budget_system
      sql = "
        select COUNT(*)
         from apps.BRA_BUDGET_LINES a
             ,apps.BRA_BUDGET_HEADER b
             ,hr.hr_all_positions_f c
             ,apps.fnd_lookup_values flv
         where a.id_budget = b.id_budget
          and a.id_solicitante = b.id_solicitante
          and b.id_status = 4
          and c.position_id = a.position_id
          and ((c.status <> 'INVALID') or c.status is null)
          and c.AVAILABILITY_STATUS_ID <> 5
          and sysdate between c.effective_start_date and c.effective_end_date
          and a.next_headcount <> 0
          and flv.lookup_type = 'TC_HR_ANO_ORCAMENTO'
          and flv.language = 'PTB'
          and flv.meaning = b.budget_name
          and flv.enabled_flag = 'Y'
          and a.position_id = #{self.position_id}"

      result = ActiveRecord::Base.connection.select_value(sql)
      return result.to_i > 0
    end
end
