class PeopleGroup < ActiveRecord::Base
  set_table_name "pay_people_groups"
  self.primary_key = 'people_group_id'

  DEFAULT_CONDITIONS = "
      enabled_flag = 'Y'
      AND segment30 IS NOT NULL
      AND group_name IS NOT NULL
  "

  default_scope :conditions => DEFAULT_CONDITIONS, :order => :group_name

  def self.search(name)
    sanitized_name = self.sanitize_sql(["%s", name])

    self.connection.select_all("
      SELECT people_group_id, group_name
        FROM pay_people_groups
       WHERE enabled_flag = 'Y'
         AND segment30 IS NOT NULL
         AND group_name IS NOT NULL
         AND group_name LIKE UPPER('%#{sanitized_name}%')
       ORDER BY group_name
      ")
  end
end