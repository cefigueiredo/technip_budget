class WebParameter < ActiveRecord::Base
  set_table_name "bra.bra_customhr_web_parameters"
  set_sequence_name "bra.bra_customhr_web_parameters_s"

  def self.get_by_key(key)
    find_or_create_by_key(:key => key, :value => 'NULL VALUE')
  end
end