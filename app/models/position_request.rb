class PositionRequest < ActiveRecord::Base
  set_table_name "bra.bra_customhr_position_reqs"
  set_sequence_name "bra.bra_customhr_position_reqs_s"

  belongs_to :core_job, :foreign_key => "job_id"

  belongs_to :core_organization_unit, :foreign_key => "organization_id"

  belongs_to :people_group

  has_many :movement_position

  belongs_to :owner,
             :class_name => "User",
             :foreign_key => "requested_by",
             :primary_key => "employee_person_id"

  validates_presence_of :position_name, :core_job, :core_organization_unit, :people_group

  def serialize
    self.attributes.merge({
      :job => self.core_job.attributes,
      :organization => self.core_organization_unit.attributes,
      :people_group => self.people_group.attributes
    })
  end
end