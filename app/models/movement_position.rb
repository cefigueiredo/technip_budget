# -*- encoding: utf-8 -*-
class MovementPosition < ActiveRecord::Base
  set_table_name "bra.bra_customhr_move_positions"
  set_sequence_name "bra.bra_customhr_move_positions_s"

  belongs_to :proposal,
    :class_name => "MovementProposal",
    :foreign_key => "proposal_id"

  belongs_to :collaborator,
    :class_name => "User",
    :primary_key => "employee_person_id",
    :foreign_key => "collaborator_id"

  belongs_to :position

  belongs_to :previous_position,
    :class_name => "Position"

  belongs_to :position_request

  named_scope :hire,  :conditions => { :iteration => 'HIRE'  }
  named_scope :fired, :conditions => { :iteration => 'FIRED' }
  named_scope :promotion, :conditions => { :iteration => 'PROMOTION' }
  named_scope :not_hire, :conditions => "iteration != 'HIRE'"
  named_scope :countables, :conditions => { :uncountable => false }

  before_save :sync_job_ids

  accepts_nested_attributes_for :position_request

  def maintenance?
    self.iteration == 'MAINTENANCE'
  end

  def promotion?
    self.iteration == 'PROMOTION'
  end

  def promotion_in?(pos_id)
    promotion? && (position_id == pos_id || position_request_id == pos_id)
  end

  def promotion_out?(pos_id)
    promotion? && previous_position_id == pos_id
  end

  def hire?
    self.iteration == 'HIRE'
  end

  def fired?
    self.iteration == "FIRED"
  end

  def quantity_by_context
    if self.proposal.parent_proposal
      quantity_approved
    else
      quantity
    end
  end

  def collaborator_name
    return collaborator.full_name if collaborator

    if collaborator_id
      connection.select_value ("
        select full_name
          from per_all_people_f
         where person_id = #{collaborator_id}
      ")
    end
  end

  def job_hierarchy
    jobs = []

    jobs << position.job_hierarchy if position
    jobs << previous_position.job_hierarchy if previous_position
    jobs
  end

  private
    def sync_job_ids
      if position
        self.job_id = position.job_id
      else
        self.job_id = position_request ? position_request.job_id : nil
      end
      self.previous_job_id = previous_position ? previous_position.job_id : nil
    end
end
