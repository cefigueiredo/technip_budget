# -*- encoding : utf-8 -*-
class Position < ActiveRecord::Base
  set_table_name "bra.bra_customhr_positions"
  self.primary_key = 'brazil_code'
  set_sequence_name "bra.bra_customhr_positions_seq"

  has_one :core_position,
    :foreign_key => 'position_id'

  has_many :collaborators,
    :class_name => "User",
    :foreign_key => "position_id"

  belongs_to :owner,
    :class_name => "User",
    :foreign_key => "created_by",
    :primary_key => 'employee_person_id'

  belongs_to :requester,
    :class_name => "User",
    :foreign_key => "requested_by",
    :primary_key => 'employee_person_id'

  validates_presence_of :brazil_code, :created_by
  validates_uniqueness_of :brazil_code

  def grades
    sql = "
        SELECT cast(pgrf.minimum as number(5)) as minimum
             , cast(pgrf.mid_value as number(5)) as mid_value
             , cast(pgrf.maximum as number(5)) as maximum
          FROM hr_all_positions_f hapf
             , pay_grade_rules_f pgrf
         WHERE hapf.entry_grade_rule_id = pgrf.grade_rule_id
           AND TRUNC(sysdate) BETWEEN hapf.effective_start_date AND hapf.effective_end_date
           AND hapf.availability_status_id <> 5
           AND ((hapf.status <> 'INVALID') OR hapf.status IS NULL)
           AND hapf.business_group_id = 511
           AND TRUNC(SYSDATE) BETWEEN pgrf.effective_start_date AND pgrf.effective_end_date
           AND hapf.position_id = #{brazil_code}
    "
    connection.select_one(sql)
  end

  def job_hierarchy
    selected_keys = [
      "position_id",
      "job_id",
      "organization_id",
      "position",
      "job",
      "uniorg",
      "minimum",
      "mid_value",
      "maximum",
    ]

    pos = core_position.attributes.merge(grades).merge({
      "position" => core_position.name,
      "job" => core_position.core_job.name,
      "uniorg" => core_position.core_organization_unit.name
    })

    pos.slice(*selected_keys)
  end

  def job_id
    core_position.job_id
  end
end
