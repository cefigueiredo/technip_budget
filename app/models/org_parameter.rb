class OrgParameter < ActiveRecord::Base
  set_table_name 'bra.bra_customhr_move_org_params'
  set_sequence_name 'bra.bra_customhr_move_org_params_s'

  belongs_to :planning, :class_name => "MovementPlanning", :foreign_key => "planning_id"
  belongs_to :core_organization_unit, :foreign_key => "organization_id"

  validates_presence_of :planning_id, :organization_id
  validates_uniqueness_of :organization_id, :scope => :planning_id

  def nested_position_ids
    core_organization_unit.core_positions.map(&:position_id)
  end
end