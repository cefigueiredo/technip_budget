class JobParameter < ActiveRecord::Base
  set_table_name "bra.bra_customhr_move_job_params"
  set_sequence_name "bra.bra_customhr_move_job_params_s"

  belongs_to :planning, :class_name => "MovementPlanning", :foreign_key => "planning_id"
  belongs_to :core_job, :foreign_key => "job_id"

  validates_presence_of :planning_id, :job_id, :job_type
  validates_uniqueness_of :job_id, :scope => :planning_id

  named_scope :ineligibles, :conditions => { :job_type => 'INELIGIBLE' }
  named_scope :uncountables, :conditions => { :job_type => 'UNCOUNTABLE' }

  def nested_position_ids
    core_job.core_position_ids
  end
end
