class UsersController < ApplicationController
  before_filter :require_user
  helper DirectorsHelper

  include ApplicationHelper

  def index
    if planning
      @directors = User.find_all_presidents_by_site(current_user.site)
    else
      redirect_to error_plannings_path
    end
  end

  def show
    if planning
      @director = User.find(params[:id])

      @serialized_user = serialize(@director)

      respond_to do |format|
        format.html
        format.json do
          render :json => @serialized_user
        end
      end
    else
      rendirect_to error_plannings_path
    end
  end

  def approve
    @director = User.find(params[:id])
    managers = @director.nested_managers_id
    error = false
    managers.each do |manager_id|
      proposal = proposal(manager_id)

      if proposal && proposal.pending_hr?
        if proposal.save
          proposal.approve(current_user)
          proposal.save
        else
          error = true
        end
      end
    end

    if error
     flash[:error] = "Houveram erros na aprovação em massa dos gestores"
    else
      flash[:notice] = "Planejamentos aprovados com sucesso"
    end
    redirect_to :back
  end

  def find
    @users = User.find_by_name_or_employee_number(params[:term])

    respond_to do |format|
      format.js do
        render :json => @users, :status => 200
      end
    end
  end


  private
    def planning
      @planning = MovementPlanning.current
    end

    def serialize(user)
      @managers = user.managers

      user.attributes.merge({
        :managers => @managers.map do |manager|
          diff_headcount      = manager.diff_headcount(@planning.id)
          approved_headcount  = manager.approved_headcount(@planning.id)
          cut_headcount       = manager.cut_headcount(@planning.id)


          manager.attributes.merge({
            :diff_manager_headcount => {
              :icon_html => counter_icon(diff_headcount, true),
              :value => diff_headcount
            },
            :cut_manager_headcount => {
              :icon_html => counter_icon(cut_headcount, true),
              :value => cut_headcount
            },
            :approved_manager_headcount => {
              :icon_html => counter_icon(approved_headcount, true),
              :value => approved_headcount
            },
            :diff_manager_hires => manager.count_hires(@planning.id),
            :managers_link => user_path(manager),
            :cut_proposal_link => user_proposal_path({:user_id => user.id, :id => manager.id}, :hr => true)
          })
        end
      }).to_json
    end

end