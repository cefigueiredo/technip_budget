class ApplicationController < ActionController::Base
  helper :all
  protect_from_forgery

  include Technip::Authentication
  helper_method :current_user, :logged_in?
end
