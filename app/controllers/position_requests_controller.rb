class PositionRequestsController < ApplicationController
  before_filter :require_user

  def index
    @position_requests = PositionRequest.all
  end

  def new
    @position_request = PositionRequest.new
    @people_groups = PeopleGroup.all
  end

  def create
    @position_request = PositionRequest.new(position_request_params)
    @position_request.owner = current_user

    if @position_request.save
      respond_to do |format|
        format.html do
          flash[:notice] = "Requisição de criação de Posição feita com sucesso."
          redirect_to position_requests_path
        end
        format.json do
          render :json => @position_request.to_json, :status => :created
        end
      end
    else
      respond_to do |format|
        format.html do
          flash[:error] = "Existem erros na requisição de criação de Posição."
          render 'new'
        end
        format.json do
          render :json => @position_request.to_json, :status => :forbidden
        end
      end
    end
  end

  def show
  end

  def edit
    @position_request = PositionRequest.find(params[:id])
  end

  def update
  end

  private
    def position_request_params
      params[:position_request].
        except([:id, :requested_by, :requested_by])
    end
end