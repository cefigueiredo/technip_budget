class ManagersController < ApplicationController
  before_filter :require_user

  include ApplicationHelper

  def index
    if planning
      @manager = current_user

      @collaborators = current_user.managers

      respond_to do |format|
        format.html do
          render 'show'
        end
        format.json do
          render :json => serialize(@manager)
        end
      end
    else
      redirect_to error_plannings_path
    end
  end

  def show
    if planning
      @manager = User.find(params[:id])

      @collaborators = @manager.managers

      respond_to do |format|
        format.html
        format.json do
          render :json => serialize(@manager)
        end
      end
    else
      redirect_to error_plannings_path
    end
  end

  def export
    @manager = current_user

    @principal_planning = ReportPlanning.build(planning, @manager)

    @nested_plannings = @manager.nested_managers_id.map do |nested_id|
      nested = User.find(nested_id)

      ReportPlanning.build(planning, nested)
    end

    @total_planning = @principal_planning + @nested_plannings.sum

    @total_planning.name   = 'TOTAL'
    @total_planning.status = ''

    respond_to do |format|
      format.xls do
        headers['Content-Type'] = "application/vnd.ms-excel;"
        headers['Content-Disposition'] = "attachment; filename=export_#{@manager.user_name}.xls;"
        headers['Cache-Control'] = ''

        render '/reports/export', :layout => false
      end
    end
  end


  private
    def planning
      @planning ||= MovementPlanning.current
    end

    def serialize(manager)
      manager.attributes.merge({
        :collaborators => @collaborators.map do |collaborator|
          diff_headcount      = collaborator.diff_headcount(@planning.id)
          approved_headcount  = collaborator.approved_headcount(@planning.id)
          cut_headcount       = collaborator.cut_headcount(@planning.id)


          collaborator.attributes.merge({
            :diff_collaborator_headcount => {
              :icon_html => counter_icon(diff_headcount, true),
              :value => diff_headcount
            },
            :cut_collaborator_headcount => {
              :icon_html => counter_icon(cut_headcount, true),
              :value => cut_headcount
            },
            :approved_collaborator_headcount => {
              :icon_html => counter_icon(approved_headcount, true),
              :value => approved_headcount
            },
            :diff_collaborator_hires => collaborator.count_hires(@planning.id),
            :collaborators_link => manager_path(collaborator),
            :proposal_link => manager_proposal_path(:manager_id => manager.id, :id => collaborator.id)
          })
        end
      }).to_json
    end
end