class ProposalsController < ApplicationController
  before_filter :require_user

  def show
    if planning
      is_hr?
      @nested_managers = !params[:manager_id].nil?

      respond_to do |format|
          format.html
          format.json do
            render :json => resource.serialize
          end
      end
    else
      redirect_to error_plannings_path
    end
  end

  def create
    proposal = MovementProposal.new(params_proposal)

    respond_to do |format|
      format.json do
        if proposal.save
          @proposal = proposal

          render :json => resource.serialize
        else
          render :json => params[:proposal].merge(proposal.errors)
        end
      end
    end
  end

  def update
    proposal = if params[:id]
      MovementProposal.find(:first, :conditions => {
        :id => params[:proposal][:id],
        :planning_id => planning.id,
        :parent_proposal_id => params[:proposal][:parent_proposal_id],
        :manager_id => params[:id]
      })
    else
      MovementProposal.find(:first, :conditions => {
        :id => params[:proposal][:id],
        :planning_id => planning.id,
        :parent_proposal_id => nil,
        :manager_id => current_user.id
      })
    end

    respond_to do |format|
      format.json do
        if proposal.update_attributes(params_proposal)
          proposal.save_resume(current_user) if params[:id]
          @proposal = proposal

          render :json => resource.serialize
        else
          render :json => {
            :proposal => params[:proposal],
            :errors   => proposal.errors
          }
        end
      end
    end
  end

  def send_to_director
    respond_to do |format|
      format.json do
        if resource.update_attributes(params_proposal)
          @proposal.submit_to_approval(current_user)

          render :json => resource.serialize
        else
          render :json => {
            :proposal => params[:proposal],
            :errors   => @proposal.errors
          }
        end
      end
    end
  end

  def approve
    respond_to do |format|
      format.json do
        begin
          resource.attributes = params_proposal
          resource.approve(@proposal.manager)

          render :status => 200 ,:json => resource.serialize
        rescue
          render :status => 500, :json => {
            :proposal => params_proposal,
            :errors => resource.errors
          }
        end
      end
    end
  end

  def search
    if planning
      @managers = summaries.map do |manager|
        url = director_proposal_path(:director_id => (manager["director_id"] || manager["manager_id"]), :id => manager["manager_id"])
        manager.merge({
          :url => url,
          :status_text => I18n.t("interface.proposal.status.#{manager["status"]}")
        })
      end

      respond_to do |format|
        format.html
        format.json do
          render :json => @managers
        end
        format.xls do
          headers['Content-Type'] = "application/vnd.ms-excel;"
          headers['Content-Disposition'] = "attachment; filename=budget_#{@planning.calendar_name}_#{@planning.min_begin_date.year}.xls;"
          headers['Cache-Control'] = ''

          render :action => 'search', :layout => false
        end

      end
    else
      redirect_to error_plannings_path
    end
  end

  protected
    def is_hr?
      @is_hr = params[:director_id] || params[:user_id]
    end

    def resource
      if is_hr?
        parent_proposal = MovementProposal.find_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, params[:id], nil)

        if parent_proposal
          @proposal = MovementProposal.find_or_initialize_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, params[:id], parent_proposal.id) do |proposal|
            proposal.created_by ||= current_user.id
            proposal.status ||= parent_proposal.status
            proposal.approved_by_hr = 1
          end
        else
          @proposal = MovementProposal.find_or_initialize_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, params[:id]) do |proposal|
            proposal.created_by ||= current_user.id
            proposal.status ||= 'PENDING-MANAGER'
            proposal.approved_by_hr = 1
          end
        end
      elsif params[:manager_id]
        parent_proposal = MovementProposal.find_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, params[:id], nil)

        if parent_proposal && parent_proposal.finished?
          @proposal = MovementProposal.find_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, params[:id], parent_proposal.id)
        else
          @proposal = MovementProposal.find_or_initialize_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, params[:id]) do |proposal|
            proposal.created_by ||= current_user.id
            proposal.status ||= 'PENDING-MANAGER'
          end
        end
      else
        parent_proposal = MovementProposal.find_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, current_user.id, nil)

        if parent_proposal && parent_proposal.finished?
          @proposal = MovementProposal.find_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, current_user.id, parent_proposal.id)
        else
          @proposal ||= MovementProposal.find_or_initialize_by_planning_id_and_manager_id(planning.id, current_user.id).tap do |proposal|
            proposal.created_by ||= current_user.id
            proposal.status ||= 'PENDING-MANAGER'
          end
        end
      end
    end

    def planning
      @planning = MovementPlanning.current
    end

    def params_proposal
      if params[:id]
        params[:proposal].except(:id, :planning_id, :manager_id, :created_by).merge({
          :planning_id => planning.id,
          :manager_id => params[:id],
          :created_by => current_user.id
        })
      else
        params[:proposal].except(:id, :planning_id, :manager_id, :created_by).merge({
          :planning_id => planning.id,
          :manager_id => current_user.id,
          :created_by => current_user.id
        })
      end
    end

    def summaries
      MovementProposal.summaries(planning.id, current_user, params)
    end
end
