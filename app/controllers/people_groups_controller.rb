class PeopleGroupsController < ApplicationController
  before_filter :require_user

  def index
    @people_groups = PeopleGroup.search(params[:term])

    respond_to do |format|
      format.html
      format.json { render :json => @people_groups.to_json, :status => 200 }
    end
  end
end