class JobsController < ApplicationController
  before_filter :require_user

  def index
    @jobs = CoreJob.options_of_jobs_in_brazil(params[:term])

    respond_to do |format|
      format.json do
        render :json => @jobs.to_json
      end
    end
  end

end