class ReportsController < ApplicationController
  before_filter :require_user
  before_filter :authorize_hr_user!, :only => [:index]

  def index
    respond_to do |format|
      format.html
      format.xls do
        @results = filter

        if @results.any?
          headers['Content-Type'] = "application/vnd.ms-excel;"
          headers['Content-Disposition'] = "attachment; filename=budget_report.xls;"
          headers['Cache-Control'] = ''

          render :action => 'index', :layout => false
        else
          render :js => "alert(Este usuário não tem permissão para gerar relatórios para este site)", :status => 401
        end
      end
    end
  end

  private
    def authorize_hr_user!
      @planning = MovementPlanning.current
      @report_rule = @planning.get_report_rule(current_user)

      if !@report_rule
        redirect_to :controller => "messages", :action => "forbidden", :status => 403
      end
    end

    def filter
      @report_rule.report_authorized_sites
    end
end