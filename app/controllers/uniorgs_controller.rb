class UniorgsController < ApplicationController
  before_filter :require_user

  def index
    @uniorgs = CoreOrganizationUnit.options_of_organization_units(params[:term])

    respond_to do |format|
      format.json do
        render :json => @uniorgs.to_json
      end
    end
  end

end