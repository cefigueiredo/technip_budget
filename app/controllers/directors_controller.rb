class DirectorsController < ApplicationController
  before_filter :require_user
  helper DirectorsHelper
  include DirectorsHelper
  include ApplicationHelper
  include ActionView::Helpers::SanitizeHelper

  def index
    if planning
      @can_export_reports = ReportRule.has_rules_associated?(current_user, planning)
      @directors = User.find_all_directors_by_site(current_user.site)

      respond_to do |format|
        format.html
      end
    else
      redirect_to error_plannings_path
    end
  end

  def show
    @director = if params[:id]
      User.find(params[:id])
    elsif current_user.is_director?
      User.find(current_user.employee_person_id)
    end

    if planning

      @serialized_director = serialize(@director)

      respond_to do |format|
        format.html
        format.json do
          render :json => @serialized_director
        end
      end
    else
      error_plannings_path
    end
  end

  def approve
    @director = User.find(params[:id])
    managers = @director.nested_managers_id
    error = false
    managers.each do |manager_id|
      proposal = proposal(manager_id)

      if proposal && proposal.pending_hr?
        if proposal.save
          proposal.approve(current_user)
          proposal.save
        else
          error = true
        end
      end
    end

    if error
     flash[:error] = "Houveram erros na aprovação em massa dos gestores"
    else
      flash[:notice] = "Planejamentos aprovados com sucesso"
    end
    redirect_to :back
  end

  def export
    @director = User.find(params[:id])

    @principal_planning = ReportPlanning.build(planning, @director)

    @nested_plannings = @director.nested_managers_id.map do |nested_id|
			nested = User.find(nested_id)

      ReportPlanning.build(planning, nested)
    end

    @total_planning = @principal_planning + @nested_plannings.sum

    @total_planning.name   = 'TOTAL'
    @total_planning.status = ''

    respond_to do |format|
      format.xls do
        headers['Content-Type'] = "application/vnd.ms-excel;"
        headers['Content-Disposition'] = "attachment; filename=export_#{@director.user_name}.xls;"
        headers['Cache-Control'] = ''

        render '/reports/export', :layout => false
      end
    end
  end

  protected
    def planning
      @planning = MovementPlanning.current
    end

    def proposal(manager_id)
        parent_proposal = MovementProposal.find_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, manager_id, nil)

        if parent_proposal
          @proposal = MovementProposal.find_or_initialize_by_planning_id_and_manager_id_and_parent_proposal_id(planning.id, manager_id, parent_proposal.id) do |proposal|
            proposal.created_by ||= current_user.id
            proposal.status = parent_proposal.status
            proposal.approved_by_hr ||= 1
          end
          @proposal.list_movements!(true)
          @proposal.save
        end

        @proposal
    end

    def serialize(director)
      @managers = director.collaborators.select do |user|
        user.manager?
      end

      director.attributes.merge({
        :managers => @managers.map do |manager|
          diff_headcount      = manager.diff_headcount(@planning.id)
          approved_headcount  = manager.approved_headcount(@planning.id)
          cut_headcount       = manager.cut_headcount(@planning.id)


          manager.attributes.merge({
            :diff_manager_headcount => {
              :icon_html => counter_icon(diff_headcount, true),
              :value => diff_headcount
            },
            :cut_manager_headcount => {
              :icon_html => counter_icon(cut_headcount, true),
              :value => cut_headcount
            },
            :approved_manager_headcount => {
              :icon_html => counter_icon(approved_headcount, true),
              :value => approved_headcount
            },
            :diff_manager_hires => manager.count_hires(@planning.id),
            :managers_link => director_path(manager),
            :cut_proposal_link => director_proposal_path(:director_id => director.id, :id => manager.id)
          })
        end
      }).to_json
    end

end
