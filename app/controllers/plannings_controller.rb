class PlanningsController < ApplicationController
  before_filter :require_user
  helper ApplicationHelper


  def new
    @current_year = Date.today.year + 1
    @planning = MovementPlanning.new({:current_planning => true})
    report_rules
  end

  def create
    @planning = MovementPlanning.new(resource_params)

    if @planning.save
      flash[:notice] = "Calendário de Planejamento foi cadastrado com sucesso."
      redirect_to plannings_path
    else
      flash[:error] = "Existem erros no cadastro do Calendário de Planejamento."
      render 'new'
    end
  end

  def update
    @planning = MovementPlanning.find(params[:id])

    if @planning.update_attributes(resource_params)
      flash[:notice] = "Calendário atualizado com sucesso"
      redirect_to plannings_path
    else
      flash[:error] = "Não foi possível atualizar o calendário. Verifique possiveis erros e tente novamente"
      render 'edit'
    end

  end

  def edit
    @planning = MovementPlanning.find(params[:id])
    report_rules

    @current_year = @planning.min_begin_date.year
  end

  def index
    @plannings = MovementPlanning.find(:all, :order => "end_planning_date DESC")
  end

  def error
    @planning = nil
  end

  def show
    @planning = MovementPlanning.find(params[:id])

    respond_to :html
  end

  def destroy
    @planning = MovementPlanning.find(params[:id])

    if @planning.destroy
      flash[:notice] = "Calendário de planejamento removido com sucesso."
    else
      flash[:notice] = "Calendários com planejamentos associados não podem ser removidos."
    end

    redirect_to plannings_path
  end

  def search
    @managers = User.managers
  end

  private
    def report_rules
      @sites = ReportRule.sites
      @report_rules = @planning.report_rules.map do |rule|
        rule.attributes.merge("user" => rule.user.attributes)
      end
    end

    def resource_params
      params[:planning][:report_rules_attributes].each_value do |rule|
        if rule[:authorized_sites]
          rule[:authorized_sites] = rule[:authorized_sites].join(',')
        end
      end

      params[:planning].except(:begin_planning_date, :end_planning_date, :current_year).merge({
        :begin_planning_date => parse_date(params[:planning][:begin_planning_date]),
        :end_planning_date => parse_date(params[:planning][:end_planning_date]),
        :min_begin_date => parse_date(params[:planning][:min_begin_date]),
        :max_end_date => parse_date(params[:planning][:max_end_date])
      })
    end

    def parse_date(date_str)
      DateTime.strptime(date_str, '%d/%m/%Y') if date_str =~ /[0-9]{2}\/[0-9]{2}\/[0-9]{4}/
    end
end