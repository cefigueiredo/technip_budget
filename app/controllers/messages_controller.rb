class MessagesController < ApplicationController
  def index
    @msg ||= params[:id]
  end

  def method_missing(method)
    case method
    when 'not_logged_in'
      @msg = 'O usuário precisa estar logado.'
    when 'forbidden'
      @msg = 'Seu usuário não possui acesso a esta funcionalidade.'
    else
      @msg = 'Erro inesperado.'
    end
    render :action => :index
  end
end
