# Technip Budget 2.0

Sistema responsável pelo cálculo e acompanhamento dos planejamentos de Headcount da Technip.


# Requisitos para desenvolvimento:

Para o desenvolvimento e teste da aplicação é necessário instalar previamente:

  * Instalar Java Runtime Environment 5 ou 6
  * Instalar jruby-1.6.8 (preferir versao sem JRE embutida, e certificar que está rodando com JVM no maximo 6)
  * Instalar bundler
  * [Citrix Access Gateway Plug-in](http://www.citrix.com/downloads/netscaler-gateway/plug-ins.html) conectando em https://portalx.technip.com.br/vpn/index.html
  * [Oracle JDBC Driver](https://github.com/rsim/ruby-plsql#installation)
  * Copiar o jar jdbc/ojdbc14.jar para a pasta lib da instalaçao do Jruby

O desenvolvimento pode ser feito nos sistemas operacionais Mac OSX e Windows, devido a só existir plugin de conexao a vpn para estes ambientes.
E pela conexao VPN bloquear o acesso a Internet e o browser homologado da Technip ser atualmente IE8, é recomendado que o ambiente de teste seja montado em uma máquina virtual Windows XP. Porém os passos são os mesmos.


Para instalar a aplicacao em ambiente de desenvolvimento, após instalar o bundler, é só rodar o comando:

    jruby bundle install

Obs.: A aplicação usa o banco de dados Oracle 10G, localizado no ambiente do cliente, por isso para testar a aplicação é necessário estar conectado na vpn pelo Citrix Access Gateway.


# Requisitos para deploy em staging

Para fazer o deploy da aplicação, é necessário gerar um pacote WAR da aplicação com o warble. O procedimento é:

Conectar a VPN do cliente pelo Citrix Access Gateway

Na raiz da aplicacao rodar o comando:

    warble war

Mover o pacote war via sftp para: br001sv0447.tp.tpnet.intra pasta: /oracle/u1/product/10.1.3/oc4j/j2ee/hr/applications


# Deploy
Fazer deploy no servidor de aplicacao http://bras10simu.tp.tpnet.intra/em/console/ias/cluster/topology
    * Acessar Cluster Topology > br001sv0447.tp.tpnet.intra > hr
    * Na view Applications, clicar em Deploy
    * Step1: Apontar o caminho do pacote war localizado no servidor
    * Step2: Definir Application name, e alterar o context root começando em /budget
    * Step3:
          -Em "Select Security Provider", escolher o Oracle Identity Management e marcar "Enable SSO Authentication"
          - Definir os grupos que tem permissao em "Map Security Roles"
    * Reiniciar servico br001sv0447.tp.tpnet.intra



# Usando a aplicacao (após o deploy)

Abaixo, os caminhos de acesso e uma breve descrição de cada funcionalidade do sistema de planejamento de orçamento.

1. Configuração de calendários.
Permite o RH abrir calendários de planejamento.
* URL: http://bras10simu.tp.tpnet.intra/budget/plannings

2. Planejamento do gestor.
Permite o gestor planejar contratações, demissões e promoções. Após aprovação / corte do RH, o gestor visualiza seu planejamento e a alteração feita pelo RH.
* URL: http://bras10simu.tp.tpnet.intra/budget/proposal

3. Planejamento de gestores de um gestor.
Permite um gestor visualizar os gestores abaixo de sua hierarquia e fazer o planejamento deles, caso necessário. Após aprovação / corte do RH, consegue visualizar o planejamento de cada gestor e a alteração feita pelo RH.
* URL: http://bras10simu.tp.tpnet.intra/budget/managers

4. Aprovação / corte do RH (toda hierarquia).
Permite o RH navegar na hierarquia de gestores, a partir da presidência, e aprovar / cortar o planejamento dos gestores.
* URL: http://bras10simu.tp.tpnet.intra/budget/users

5. Aprovação / corte do RH (hierarquia a partir das diretorias).
Similar ao item anterior, porém a hierarquia começa a partir das diretorias.
* URL: http://bras10simu.tp.tpnet.intra/budget/directors

6. Consulta e acompanhamento de planejamentos.
Permite o RH consultar e acompanhar os planejamentos.
* URL: http://bras10simu.tp.tpnet.intra/budget/proposals/search


