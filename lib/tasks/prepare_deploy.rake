require "fileutils"
require 'pathname'
require 'warbler'

Warbler::Task.new

namespace :technip do
  desc 'Compile assets and make a war package to deploy'
  task :deploy do
    Rake::Task['assets:clean'].invoke
    Rake::Task['assets:precompile'].invoke

    Rake::Task['assets:clean:sha1'].invoke

    Rake::Task['war'].invoke
  end
end