module Technip
  module Authentication
    def require_user
      log_in unless logged_in?
      redirect_if_not_logged_in
    end

    def current_user
      @current_user ||= log_in
    end

    def logged_in?
      not @current_user.nil?
    end

    def log_in
      @uid = session[:user_id] || request.env['REMOTE_USER'] || params[:username]
      unless @uid.nil?
        session[:user_id] = @uid
        @current_user = User.find_by_user_name(@uid.upcase)
        return @current_user
      end
    end

    protected
      def redirect_if_not_logged_in
        redirect_to :controller => 'messages', :action => 'not_logged_in' unless logged_in?
      end
  end
end