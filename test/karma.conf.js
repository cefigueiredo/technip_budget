module.exports = function(config){
  config.set({

    basePath : '../',

    files : [
      'app/assets/javascripts/array.js',
      'app/assets/javascripts/jquery.min.js',
      'app/assets/javascripts/jquery_ujs.js',
      'vendor/bower_components/underscore/underscore.js',
      'vendor/bower_components/angular/angular.js',
      'vendor/bower_components/angular-route/angular-route.js',
      'vendor/bower_components/angular-mocks/angular-mocks.js',
      'vendor/bower_components/angular-resource/angular-resource.js',
      'vendor/bower_components/angular-bootstrap/ui-bootstrap.js',
      'vendor/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'vendor/bower_components/select2/select2',
      'vendor/bower_components/select2/select2_locale_pt-BR',
      'vendor/bower_components/angular-ui-select2/src/ui-select2.js',
      'app/assets/javascripts/budget/**/*.js',
      'test/javascript/unit/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};