describe("Budget controllers", function () {
  describe("ProposalController", function() {
    beforeEach(module('proposalApp'));

    describe("when managing a Proposal", function() {
      var scope, ctrl, $httpBackend, q
          proposal_json = {
            "planning": {
              "id": 1,
              "min_begin_date": "2015-01-01T00:00:00Z",
              "max_end_date": "2015-12-31T00:00:00Z"
            },
            "id": null,
            "parent_proposal_id": null,
            "status": "PENDING-MANAGER",
            "manager": {
              "employee_person_id": 3081,
              "full_name": "BRUNO ALMEIDA DE ABREU",
            },
            "jobs": [
              {
                "name": "JOB 1",
                "job_id": "1",
                "current_team": [
                  {
                    "job_id": 1,
                    "job_name": "JOB 1",
                    "position_id": 1,
                    "employee_person_id": 1,
                    "full_name": "USER 1"
                  },
                  {
                    "job_id": 1,
                    "job_name": "JOB 1",
                    "position_id": 2,
                    "employee_person_id": 2,
                    "full_name": "USER 2"
                  },
                  {
                    "job_id": 1,
                    "job_name": "JOB 1",
                    "position_id": 1,
                    "employee_person_id": 3,
                    "full_name": "USER 3"
                  }
                ],
                "positions": [
                  {
                    "position_id": 1,
                    "job_id": 1,
                    "organization_id": 1,
                    "position": "POSITION 1",
                    "job": "JOB 1",
                    "uniorg": "UNIORG 1",
                    "movements": [
                      {
                        "begin_date": '01/01/2015',
                        "collaborator_id": 1,
                        "collaborator_name": "COLLAB 1",
                        "end_date": null,
                        "iteration": "FIRED",
                        "position_id": 1,
                        "previous_position_id": 1,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": null
                      },
                      {
                        "begin_date": '01/01/2015',
                        "collaborator_id": 3,
                        "collaborator_name": "COLLAB 3",
                        "end_date": '31/12/2015',
                        "iteration": "MAINTENANCE",
                        "position_id": 1,
                        "previous_position_id": 1,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": null
                      },
                      {
                        "begin_date": "01/01/2015",
                        "collaborator_id": null,
                        "collaborator_name": null,
                        "end_date": '31/12/2015',
                        "iteration": "HIRE",
                        "position_id": 1,
                        "previous_position_id": null,
                        "proposal_id": 1,
                        "quantity": 2,
                        "motive": "Increase team"
                      },
                      {
                        "begin_date": '01/01/2015',
                        "collaborator_id": 4,
                        "collaborator_name": "COLLAB 4",
                        "end_date": '31/12/2015',
                        "iteration": "PROMOTION",
                        "position_id": 1,
                        "previous_position_id": 3,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": null
                      }
                    ]
                  },
                  {
                    "position_id": 2,
                    "job_id": 1,
                    "organization_id": 1,
                    "position": "POSITION 2",
                    "job": "JOB 1",
                    "uniorg": "UNIORG 1",
                    "movements": [
                      {
                        "begin_date": '01/01/2015',
                        "collaborator_id": 2,
                        "collaborator_name": "COLLAB 2",
                        "end_date": '31/12/2015',
                        "iteration": "MAINTENANCE",
                        "position_id": 2,
                        "previous_position_id": 2,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": null
                      }
                    ]
                  }
                ]
              },
              {
                "name": "JOB 2",
                "job_id": "2",
                "current_team": [
                  {
                    "job_id": 2,
                    "job_name": "JOB 2",
                    "position_id": 3,
                    "employee_person_id": 4,
                    "full_name": "USER 4"
                  },
                  {
                    "job_id": 2,
                    "job_name": "JOB 2",
                    "position_id": 4,
                    "employee_person_id": 5,
                    "full_name": "USER 5"
                  }
                ],
                "positions": [
                  {
                    "position_id": 3,
                    "job_id": 2,
                    "organization_id": 1,
                    "position": "POSITION 3",
                    "job": "JOB 2",
                    "uniorg": "UNIORG 1",
                    "movements": [
                      {
                        "begin_date": '01/01/2015',
                        "collaborator_id": 3,
                        "collaborator_name": "COLLAB 3",
                        "end_date": '31/12/2015',
                        "iteration": "MAINTENANCE",
                        "position_id": 4,
                        "previous_position_id": 4,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": null
                      },
                      {
                        "begin_date": "01/01/2015",
                        "collaborator_id": null,
                        "collaborator_name": null,
                        "end_date": '31/12/2015',
                        "iteration": "HIRE",
                        "position_id": 3,
                        "previous_position_id": null,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": 'Increase team'
                      }
                    ]
                  }
                ]
              }
            ]
          },
          proposed_json = {
            "proposal": {
              "planning_id": 1,
              "id": 1,
              "parent_proposal_id": null,
              "status": "PENDING-MANAGER",
              "manager_id": 3081,
              "proposed_movements_attributes": [
                {
                  "begin_date": '01/01/2015',
                  "collaborator_id": 1,
                  "end_date": null,
                  "iteration": "FIRED",
                  "position_id": 1,
                  "previous_position_id": 1,
                  "proposal_id": 1,
                  "quantity": 1,
                  "motive": null
                },
                {
                  "begin_date": '01/01/2015',
                  "collaborator_id": 3,
                  "end_date": '31/12/2015',
                  "iteration": "MAINTENANCE",
                  "position_id": 1,
                  "previous_position_id": 1,
                  "proposal_id": 1,
                  "quantity": 1,
                  "motive": null
                },
                {
                  "begin_date": '01/01/2015',
                  "collaborator_id": null,
                  "end_date": '31/12/2015',
                  "iteration": "HIRE",
                  "position_id": 1,
                  "previous_position_id": null,
                  "proposal_id": 1,
                  "quantity": 2,
                  "motive": 'Increase team'
                },
                {
                  "begin_date": '01/01/2015',
                  "collaborator_id": 4,
                  "end_date": '31/12/2015',
                  "iteration": "PROMOTION",
                  "position_id": 1,
                  "previous_position_id": 3,
                  "proposal_id": 1,
                  "quantity": 1,
                  "motive": null,
                  "quantity": 1,
                  "motive": null
                },
                {
                  "begin_date": '01/01/2015',
                  "collaborator_id": 2,
                  "end_date": '31/12/2015',
                  "iteration": "MAINTENANCE",
                  "position_id": 2,
                  "previous_position_id": 2,
                  "proposal_id": 1,
                  "quantity": 1,
                  "motive": null
                },
                {
                  "begin_date": '01/01/2015',
                  "collaborator_id": 3,
                  "end_date": '31/12/2015',
                  "iteration": "MAINTENANCE",
                  "position_id": 4,
                  "previous_position_id": 4,
                  "proposal_id": 1,
                  "quantity": 1,
                  "motive": null
                },
                {
                  "begin_date": "01/01/2015",
                  "collaborator_id": null,
                  "end_date": '31/12/2015',
                  "iteration": "HIRE",
                  "position_id": 3,
                  "previous_position_id": null,
                  "proposal_id": 1,
                  "quantity": 1,
                  "motive": 'Increase team'
                }
              ]
            }
          },
          persisted_json = {
            "planning": {
              "id": 1,
              "min_begin_date": "2015-01-01T00:00:00Z",
              "max_end_date": "2015-12-31T00:00:00Z"
            },
            "id": 1,
            "parent_proposal_id": 1,
            "status": "PENDING-MANAGER",
            "manager": {
              "employee_person_id": 3081,
              "full_name": "BRUNO ALMEIDA DE ABREU",
            },
            "jobs": [
              {
                "name": "JOB 1",
                "job_id": "1",
                "current_team": [
                  {
                    "job_id": 1,
                    "job_name": "JOB 1",
                    "position_id": 1,
                    "employee_person_id": 1,
                    "full_name": "USER 1"
                  },
                  {
                    "job_id": 1,
                    "job_name": "JOB 1",
                    "position_id": 2,
                    "employee_person_id": 2,
                    "full_name": "USER 2"
                  },
                  {
                    "job_id": 1,
                    "job_name": "JOB 1",
                    "position_id": 1,
                    "employee_person_id": 3,
                    "full_name": "USER 3"
                  }
                ],
                "positions": [
                  {
                    "position_id": 1,
                    "job_id": 1,
                    "organization_id": 1,
                    "position": "POSITION 1",
                    "job": "JOB 1",
                    "uniorg": "UNIORG 1",
                    "movements": [
                      {
                        "begin_date": '01/01/2015',
                        "collaborator_id": 1,
                        "collaborator_name": "COLLAB 1",
                        "end_date": null,
                        "iteration": "FIRED",
                        "position_id": 1,
                        "previous_position_id": 1,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": null
                      },
                      {
                        "begin_date": '01/01/2015',
                        "collaborator_id": 3,
                        "collaborator_name": "COLLAB 3",
                        "end_date": '31/12/2015',
                        "iteration": "MAINTENANCE",
                        "position_id": 1,
                        "previous_position_id": 1,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": null
                      },
                      {
                        "begin_date": "01/01/2015",
                        "collaborator_id": null,
                        "collaborator_name": null,
                        "end_date": '31/12/2015',
                        "iteration": "HIRE",
                        "position_id": 1,
                        "previous_position_id": null,
                        "proposal_id": 1,
                        "quantity": 2,
                        "motive": 'Increase team'
                      },
                      {
                        "begin_date": "01/01/2015",
                        "collaborator_id": 4,
                        "collaborator_name": "COLLAB 4",
                        "end_date": '31/12/2015',
                        "iteration": "PROMOTION",
                        "position_id": 1,
                        "previous_position_id": 3,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": null
                      }
                    ]
                  },
                  {
                    "position_id": 2,
                    "job_id": 1,
                    "organization_id": 1,
                    "position": "POSITION 2",
                    "job": "JOB 1",
                    "uniorg": "UNIORG 1",
                    "movements": [
                      {
                        "begin_date": '01/01/2015',
                        "collaborator_id": 2,
                        "collaborator_name": "COLLAB 2",
                        "end_date": '31/12/2015',
                        "iteration": "MAINTENANCE",
                        "position_id": 2,
                        "previous_position_id": 2,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": null
                      }
                    ]
                  }
                ]
              },
              {
                "name": "JOB 2",
                "job_id": "2",
                "current_team": [
                  {
                    "job_id": 2,
                    "job_name": "JOB 2",
                    "position_id": 3,
                    "employee_person_id": 4,
                    "full_name": "USER 4"
                  },
                  {
                    "job_id": 2,
                    "job_name": "JOB 2",
                    "position_id": 4,
                    "employee_person_id": 5,
                    "full_name": "USER 5"
                  }
                ],
                "positions": [
                  {
                    "position_id": 3,
                    "job_id": 2,
                    "organization_id": 1,
                    "position": "POSITION 3",
                    "job": "JOB 2",
                    "uniorg": "UNIORG 1",
                    "movements": [
                      {
                        "begin_date": '01/01/2015',
                        "collaborator_id": 3,
                        "collaborator_name": "COLLAB 3",
                        "end_date": '31/12/2015',
                        "iteration": "MAINTENANCE",
                        "position_id": 4,
                        "previous_position_id": 4,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": null
                      },
                      {
                        "begin_date": '01/01/2015',
                        "collaborator_id": null,
                        "collaborator_name": null,
                        "end_date": '31/12/2015',
                        "iteration": "HIRE",
                        "position_id": 3,
                        "previous_position_id": null,
                        "proposal_id": 1,
                        "quantity": 1,
                        "motive": 'Increase team'
                      }
                    ]
                  }
                ]
              }
            ]
          };

      beforeEach(inject(function(_$httpBackend_, $rootScope, $controller, $q, ProposalService) {
        $httpBackend = _$httpBackend_;
        $httpBackend.whenGET('proposal.json').respond(proposal_json)

        q = $q
        scope = $rootScope.$new();
        ctrl = $controller('ProposalController', {$scope: scope})

        scope.initializeWith('proposal', false, true, false);
        $httpBackend.flush();
      }));

      afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      })

      describe('and retrieving data via GET action', function() {

        it('should create a proposal with 2 jobs from json', function() {
          expect(scope.service.jobs.length).toEqual(2)
        });

        it('should calculate the current Headcount of a proposal', function() {
          expect(scope.service.currentHeadcount).toEqual(5);
        });

        it('should calculate how many collaborators were planned to be hired', function() {
          expect(scope.service.totalHires).toEqual(3);
        });

        it('should calculate how many collaborators were planned to be fired', function() {
          expect(scope.service.totalFires).toEqual(1);
        });

        it('should calculate how many collaborators were planned to be promoted', function() {
          expect(scope.service.totalPromotions).toEqual(1);
        });

        it('should calculate the total Headcount after the planned movements', function() {
          expect(scope.service.totalHeadcount).toEqual(7);
        })

        it('should calculate the Headcount`s delta after the planned movements', function() {
          expect(scope.service.totalDeltaHeadcount).toEqual(2)
        })
      })

      describe('and persisting a new proposal', function() {
        beforeEach(function () {
          proposed_json.id = null;

          $httpBackend.expectPOST('proposal.json').respond(persisted_json)
        })

        it('should pass a correct json and receive the persisted proposal as response', function() {
          scope.save()

          $httpBackend.flush()
          expect(scope.service.id).toEqual(1)
        })

      })

      describe('and persisting a changed proposal', function() {
        beforeEach(function () {
          proposal_json.id = '1'
          $httpBackend.whenGET('proposal.json').respond(proposal_json)

          scope.initializeWith('proposal', false, true, false);
          $httpBackend.flush()
        })

        it('should call a PUT passing the changed json and retrieving the persisted as response', function() {
          $httpBackend.expectPUT('proposal.json').respond(persisted_json)

          scope.save()

          $httpBackend.flush()
          expect(scope.service.status).toEqual('PENDING-MANAGER')
        })

      })

      describe('and when a manager sends the proposal to a director', function() {
        beforeEach(function () {
          persisted_json.status = 'PENDING-DIRECTOR'

          spyOn(window, "confirm").andReturn(true)

          $httpBackend.expectPOST('send_proposal.json').respond(persisted_json);
        })

        it('should hit the address send_proposal with a method POST passing the changed json and retrieving the persisted as response', function() {
          scope.send()

          $httpBackend.flush()

          expect(scope.service.status).toEqual('PENDING-DIRECTOR')
        })
      })
    });
  });
});