describe("ProposalService", function() {
  var service, proposal, jobFactory

  beforeEach(module('proposalApp'))

  beforeEach(inject(function(ProposalService, Job) {
    service = ProposalService
    jobFactory = Job

    proposal = {
      "planning_id": 1,
      "id": null,
      "parent_proposal_id": null,
      "status": "PENDING-MANAGER",
      "manager": {
        "employee_person_id": 1,
        "full_name": "MANAGER"
      },
      "planning": {
        "id": 1,
        "min_begin_date": "2015-01-01T00:00:00Z"
      },
      "created_by": 1,
      "jobs": [
        {
          "name": "JOB 1",
          "job_id": 1,
          "current_team": [
            {
              "job_id": 1,
              "job_name": "JOB 1",
              "position_id": 1,
              "employee_person_id": 1,
              "full_name": "USER 1"
            },
            {
              "job_id": 1,
              "job_name": "JOB 1",
              "position_id": 2,
              "employee_person_id": 2,
              "full_name": "USER 2"
            },
            {
              "job_id": 1,
              "job_name": "JOB 1",
              "position_id": 1,
              "employee_person_id": 3,
              "full_name": "USER 3"
            }
          ],
          "positions": [
            {
              "position_id": 1,
              "job_id": 1,
              "organization_id": 1,
              "position": "POSITION 1",
              "job": "JOB 1",
              "uniorg": "UNIORG 1",
              "movements": [
                {
                  "id": null,
                  "begin_date": null,
                  "collaborator_id": 1,
                  "collaborator_name": "COLLAB 1",
                  "end_date": null,
                  "iteration": "FIRED",
                  "position_id": 1,
                  "previous_position_id": 1,
                  "proposal_id": null,
                  "quantity": 1,
                  "motive": null
                },
                {
                  "id": null,
                  "begin_date": null,
                  "collaborator_id": 3,
                  "collaborator_name": "COLLAB 3",
                  "end_date": null,
                  "iteration": "MAINTENANCE",
                  "position_id": 1,
                  "previous_position_id": 1,
                  "proposal_id": null,
                  "quantity": 1,
                  "motive": null
                },
                {
                  "id": null,
                  "begin_date": "01/02/2015",
                  "collaborator_id": null,
                  "collaborator_name": null,
                  "end_date": null,
                  "iteration": "HIRE",
                  "position_id": 1,
                  "previous_position_id": null,
                  "proposal_id": null,
                  "quantity": 1,
                  "motive": "Increase team"
                },
                {
                  "id": null,
                  "begin_date": null,
                  "collaborator_id": 4,
                  "collaborator_name": "COLLAB 4",
                  "end_date": null,
                  "iteration": "PROMOTION",
                  "position_id": 1,
                  "previous_position_id": 3,
                  "proposal_id": null,
                  "quantity": 1,
                  "motive": null
                }
              ]
            },
            {
              "position_id": 2,
              "job_id": 1,
              "organization_id": 1,
              "position": "POSITION 2",
              "job": "JOB 1",
              "uniorg": "UNIORG 1",
              "movements": [
                {
                  "id": null,
                  "begin_date": '01/01/2015',
                  "collaborator_id": 2,
                  "collaborator_name": "COLLAB 2",
                  "end_date": null,
                  "iteration": "MAINTENANCE",
                  "position_id": 2,
                  "previous_position_id": 2,
                  "proposal_id": null,
                  "quantity": 1,
                  "motive": null
                }
              ]
            }
          ]
        },
        {
          "name": "JOB 2",
          "job_id": 2,
          "current_team": [
            {
              "job_id": 2,
              "job_name": "JOB 2",
              "position_id": 3,
              "employee_person_id": 4,
              "full_name": "USER 4"
            },
            {
              "job_id": 2,
              "job_name": "JOB 2",
              "position_id": 4,
              "employee_person_id": 5,
              "full_name": "USER 5"
            }
          ],
          "positions": [
            {
              "position_id": 3,
              "job_id": 2,
              "organization_id": 1,
              "position": "POSITION 3",
              "job": "JOB 2",
              "uniorg": "UNIORG 1",
              "movements": [
                {
                  "id": null,
                  "begin_date": null,
                  "collaborator_id": 3,
                  "collaborator_name": "COLLAB 3",
                  "end_date": null,
                  "iteration": "MAINTENANCE",
                  "position_id": 3,
                  "previous_position_id": 3,
                  "proposal_id": null,
                  "quantity": 1,
                  "motive": null
                },
                {
                  "id": null,
                  "begin_date": "01/02/2015",
                  "collaborator_id": null,
                  "collaborator_name": null,
                  "end_date": null,
                  "iteration": "HIRE",
                  "position_id": 3,
                  "previous_position_id": null,
                  "proposal_id": null,
                  "quantity": 2,
                  "motive": "Increase team"
                }
              ]
            }
          ]
        }
      ]
    }
  }));

  describe('initialize', function() {
    it('should extend the service with the proposal attributes', function() {
      service.initialize(proposal);

      expect(service.status).toEqual(proposal.status);
      expect(service.planing_id).toEqual(proposal.planing_id);
    })

    it('should create a Job from each job in the given raw proposal', function () {
      service.initialize(proposal);

      expect(service.jobs[0] instanceof jobFactory).toBeTruthy()
      expect(service.jobs[0].name).toEqual(proposal.jobs[0].name)
    })

  })

  describe('calculateTotals', function() {
    beforeEach(function() {
      service.initialize(proposal);
    })

    it('should calculate total currentHeadcount', function() {
      service.calculateTotals()

      expect(service.currentHeadcount).toEqual(5)
    })

    it('should calculate total number of Hires proposed', function() {
      service.calculateTotals()

      expect(service.totalHires).toEqual(3)
    })

    it('should calculate total number of Fired proposed', function() {
      service.calculateTotals()

      expect(service.totalFires).toEqual(1)
    })

    it('should calculate total number of Promotions proposed', function() {
      service.calculateTotals()

      expect(service.totalPromotions).toEqual(1)
    })

    it('should calculate the total headcount after movements planned', function() {
      service.calculateTotals()

      expect(service.totalHeadcount).toEqual(7)
    })

    it('should calculate the headcount`s delta after movements planned', function() {
      service.calculateTotals()

      expect(service.totalDeltaHeadcount).toEqual(2)
    })
  })

  describe('serializeProposal', function() {
    var serialized_proposal = {
      "proposal": {
        "planning_id": 1,
        "id": null,
        "parent_proposal_id": null,
        "status": "PENDING",
        "manager_id": 1,
        "created_by": 1,
        "proposed_movements_attributes": [
          {
            "id": null,
            "position_id": 1,
            "proposal_id": null,
            "collaborator_id": 1,
            "previous_position_id": 1,
            "begin_date": null,
            "end_date": null,
            "iteration": "FIRED",
            "quantity": 1,
            "motive": null
          },
          {
            "id": null,
            "position_id": 1,
            "proposal_id": null,
            "collaborator_id": 3,
            "previous_position_id": 1,
            "begin_date": null,
            "end_date": null,
            "iteration": "MAINTENANCE",
            "quantity": 1,
            "motive": null
          },
          {
            "id": null,
            "position_id": 1,
            "proposal_id": null,
            "collaborator_id": null,
            "previous_position_id": null,
            "begin_date": "01/02/2015",
            "end_date": null,
            "iteration": "HIRE",
            "quantity": 1,
            "motive": 'Increase team'
          },
          {
            "id": null,
            "position_id": 1,
            "proposal_id": null,
            "collaborator_id": 4,
            "previous_position_id": 3,
            "begin_date": null,
            "end_date": null,
            "iteration": "PROMOTION",
            "quantity": 1,
            "motive": null
          },
          {
            "id": null,
            "position_id": 2,
            "proposal_id": null,
            "collaborator_id": 2,
            "previous_position_id": 2,
            "begin_date": '01/01/2015',
            "end_date": null,
            "iteration": "MAINTENANCE",
            "quantity": 1,
            "motive": null
          },
          {
            "id": null,
            "position_id": 1,
            "proposal_id": null,
            "collaborator_id": 4,
            "previous_position_id": 3,
            "begin_date": null,
            "end_date": null,
            "iteration": "PROMOTION",
            "quantity": 1,
            "motive": null
          },
          {
            "id": null,
            "position_id": 4,
            "proposal_id": null,
            "collaborator_id": 3,
            "previous_position_id": 4,
            "begin_date": null,
            "end_date": null,
            "iteration": "MAINTENANCE",
            "quantity": 1,
            "motive": null
          },
          {
            "id": null,
            "position_id": 3,
            "proposal_id": null,
            "collaborator_id": null,
            "previous_position_id": null,
            "begin_date": "01/02/2015",
            "end_date": null,
            "iteration": "HIRE",
            "quantity": 1,
            "motive": "Increase team"
          }
        ]
      }
    }

    beforeEach(function() {
      service.initialize(proposal);
    }),

    xit('should return an object representing the proposal to be persisted on database', function() {
      expect(service.serializeProposal()).toEqual(serialized_proposal);
    })
  })

  describe('setPlanningMonths', function() {
    beforeEach(function() {
      service.initialize(proposal);
    })

    xit('should return an array of dates-months', function() {
      service.monthsList = []
      service.setPlanningMonths()


      expect(service.monthsList.length).toEqual(12)
      expect(service.monthsList[0]).toEqual(new Date("2015-01-01 00:00:00 GMT -0200"))
      expect(service.monthsList[11]).toEqual(new Date("2015-12-01 00:00:00 GMT -0200"))
    })
  })

  describe('possiblePromotionPositions(jobId)', function() {
    beforeEach(function() {
      service.initialize(proposal);
    })

    it('should list all positions that a collaborator from a given job could be promoted for', function() {
      current_job = service.jobs[0]
      possiblePromotionPositions = service.possiblePromotionPositions(current_job.job_id)

      expect(possiblePromotionPositions).toContain(service.jobs[1].positions[0])
      expect(possiblePromotionPositions).not.toContain(current_job.positions[0])
      expect(possiblePromotionPositions).not.toContain(current_job.positions[1])
    })
  })
})

describe("MovementFactory", function() {
  var movement, movementAttributes;

  beforeEach(module('proposalApp'))

  beforeEach(inject(function(Movement) {
    movementAttributes = {
      "id": 1,
      "proposal_id": 1,
      "collaborator_id": 1,
      'collaborator_name': 'COLAB 1',
      "position_id": 1,
      "begin_date": null,
      "end_date": null,
      "previous_position_id": 1,
      "iteration": "MAINTENANCE",
      'quantity': 1,
      'motive': null
    },

    movement = new Movement(movementAttributes)
  }))

  describe('serialize', function() {
    it('should return an object with only the attributes required to persist', function() {
      expected_attributes = [
        'id',
        'position_id',
        'proposal_id',
        'collaborator_id',
        'previous_position_id',
        'begin_date',
        'end_date',
        'iteration',
        'quantity',
        'motive'
      ]

      expect(_(movement.serialize()).keys()).toEqual(expected_attributes)
    })

    it('should return the "_destroy" attribute only if true', function() {
      expect(_(movement.serialize()).keys()).not.toContain('_destroy')

      movement._destroy = true
      expect(_(movement.serialize()).keys()).toContain('_destroy')
    })
  })

  describe('fireCollaborator()', function() {
    it('should set the movement as "FIRED"', function() {
      movement.fireCollaborator()

      expect(movement.iteration).toEqual('FIRED')
    })

    it('should set the position_id to null', function() {
      movement.fireCollaborator()

      expect(movement.position_id).toBeNull()
    })

    it('should revert when the movement has already been set as "FIRED"', function() {
      movement.iteration = 'FIRED'

      expect(movement.iteration).toEqual('FIRED')
      movement.fireCollaborator()
      expect(movement.iteration).toEqual('MAINTENANCE')
    })
  })

  describe('revert()', function() {
    describe('when movement is not a HIRE', function() {
      it('should set the movement as "MAINTENANCE"', function() {
        movement.revert()

        expect(movement.iteration).toEqual('MAINTENANCE')
      })

      it('should set the position_id to null as previous_position_id', function() {
        movement.fireCollaborator()
        movement.revert()

        expect(movement.position_id).toBeNull()
      })
    })

    describe("when movement is a HIRE", function() {
      describe("and the movement was already persisted", function() {
        beforeEach(inject(function(Movement) {
          persisted_hire = {
            "id": 1,
            "proposal_id": 1,
            "collaborator_id": null,
            'collaborator_name': null,
            "position_id": 1,
            "begin_date": '01/01/2015',
            "end_date": null,
            "previous_position_id": null,
            "iteration": "HIRE",
            'quantity': 1,
            'motive': null
          },

          persisted_movement = new Movement(persisted_hire)

        }))

        it('should mark the movement to be destroyed', function() {
          expect(persisted_movement._destroy).toBeFalsy()

          persisted_movement.revert()

          expect(persisted_movement._destroy).toBeTruthy()
        });
      });
    });
  })

  describe('promoteCollaborator()', function() {
    it("should set the movement as 'PROMOTION'", function() {
      movement.promoteCollaborator()

      expect(movement.iteration).toEqual('PROMOTION')
    })
  })

  describe('isValidPromotion()', function() {
    it("should return false if new promotion_id is equal previous_promotion_id", function() {
      movement.promoteCollaborator()
      position_id = movement.previous_position_id

      expect(movement.isValidPromotion(position_id)).toBeFalsy()
    })

    it("should return true if new promotion_id is different from previous_promotion_id", function() {
      movement.promoteCollaborator()
      position_id = movement.previous_position_id + 1

      expect(movement.isValidPromotion(position_id)).toBeTruthy()
    })
  })
})

describe("PositionFactory", function() {
  var position, position_attributes;

  beforeEach(module('proposalApp'))

  beforeEach(inject(function(Position, Movement, Movements) {
    position_attributes = {
      "position_id": 3,
      "job_id": 2,
      "organization_id": 1,
      "position": "POSITION 3",
      "job": "JOB 2",
      "uniorg": "UNIORG 1",
      "movements": [
        {
          "begin_date": null,
          "collaborator_id": 4,
          "collaborator_name": "COLLAB 4",
          "end_date": null,
          "iteration": "PROMOTION",
          "position_id": 4,
          "previous_position_id": 3,
          "proposal_id": null,
          "quantity": 1,
          "motive": null
        },
        {
          "begin_date": null,
          "collaborator_id": 3,
          "collaborator_name": "COLLAB 3",
          "end_date": null,
          "iteration": "MAINTENANCE",
          "position_id": 3,
          "previous_position_id": 3,
          "proposal_id": null,
          "quantity": 1,
          "motive": null
        },
        {
          "begin_date": "01/02/2015",
          "collaborator_id": null,
          "collaborator_name": null,
          "end_date": null,
          "iteration": "HIRE",
          "position_id": 3,
          "previous_position_id": null,
          "proposal_id": null,
          "quantity": 1,
          "motive": "Increase team"
        }
      ]
    }

    other_position_attributes = {
      "position_id": 4,
      "job_id": 1,
      "organization_id": 1,
      "position": "POSITION 4",
      "job": "JOB 1",
      "uniorg": "UNIORG 1",
      "movements": [
        {
          "begin_date": null,
          "collaborator_id": 5,
          "collaborator_name": "COLLAB 5",
          "end_date": null,
          "iteration": "PROMOTION",
          "position_id": 3,
          "previous_position_id": 1,
          "proposal_id": null,
          "quantity": 1,
          "motive": null
        }
      ]
    }

    position = new Position(position_attributes)
    other_position = new Position(other_position_attributes)
  }))

  describe('calculateMovements', function() {

    beforeEach(inject(function(Movements) {
      Movements.by_position_id = {
        '3': position.movements,
        '4': other_position.movements
      }
    }))

    it('should calculate the quantity of "HIRES" planned to that position', function() {
      position.calculateMovements()

      expect(position.hiresCount).toEqual(1)
    })

    it('should calculate the quantity of collaborators planned to be promoted to that position', function() {
      position.calculateMovements()
      other_position.calculateMovements()

      expect(position.promotionsInCount).toEqual(1)
    })

    it('should calculate the quantity of collaborators planned to be promoted to that position', function() {
      other_position.calculateMovements()
      position.calculateMovements()

      expect(position.promotionsInCount).toEqual(1)
    })

    it('should calculate the quantity of collaborators planned to be promoted to other position', function() {
      position.calculateMovements()

      expect(position.promotionsOutCount).toEqual(1)
    })

    it('should calculate the quantity of collaborators planned to be Fired', function() {
      position.calculateMovements()

      expect(position.firesCount).toEqual(0)
    })
  })

  describe('hireCollaborator()', function() {
    it('should create a new movement with iteration "HIRE"', function() {
      expect(_(position.movements).where({iteration: 'HIRE'}).length).toEqual(1)

      position.hireCollaborator()

      expect(_(position.movements).where({iteration: 'HIRE'}).length).toEqual(2)
    })
  })

  describe('listActualMovements()', function() {
    it('should list only movements that have not iteration "HIRE"', function() {
      expect(position.listActualMovements().length).toEqual(2)
    })
  })

  describe('listHireMovements()', function() {
    it('should list only "HIRE" movements', function() {
      expect(position.listHireMovements().length).toEqual(1)
    })

    describe('there is a movement marked with _destroy', function() {
      beforeEach(inject(function(Movement) {
        hire = {
          "begin_date": "01/02/2015",
          "collaborator_id": null,
          "collaborator_name": null,
          "end_date": null,
          "id": 1,
          "iteration": "HIRE",
          "position_id": 3,
          "previous_position_id": null,
          "proposal_id": null,
          "quantity": 1,
          "motive": "Increase team"
        }

        destroyed_movement = new Movement(hire)
        destroyed_movement._destroy = true
        position.movements.push(destroyed_movement)

      }))

      it('should not list only the destroyed movement', function() {

        expect(position.movements).toContain(destroyed_movement)
        expect(position.listHireMovements()).not.toContain(destroyed_movement)
        expect(position.listHireMovements().length).toEqual(1)
      })
    })
  })

  describe('revertHire(hire)', function() {
    describe('when the movement has already an id from database', function() {
      beforeEach(inject(function(Movement) {
        persited_hire = {
          "begin_date": "01/02/2015",
          "collaborator_id": null,
          "collaborator_name": null,
          "end_date": null,
          "id": 4,
          "iteration": "HIRE",
          "position_id": 3,
          "previous_position_id": null,
          "proposal_id": 1,
          "quantity": 1,
          "motive": "Increase team"
        }

        hire = new Movement(persisted_hire)
        position.movements.push(hire)

        spyOn(hire, 'revert')

      }))

      it('should call the method revertHire() from the movement', function() {
        position.revertHire(hire)

        expect(hire.revert).toHaveBeenCalled()
      })
    })
  })

})

describe('JobFactory', function() {
  var job;

  beforeEach(module('proposalApp'))

  beforeEach(inject(function(Job, Position, Movements){
    job_attributes = {
      "name": "JOB 2",
      "job_id": "2",
      "current_team": [
        {
          "job_id": 2,
          "job_name": "JOB 2",
          "position_id": 3,
          "employee_person_id": 4,
          "full_name": "USER 4"
        },
        {
          "job_id": 2,
          "job_name": "JOB 2",
          "position_id": 3,
          "employee_person_id": 5,
          "full_name": "USER 5"
        },
        {
          "job_id": 2,
          "job_name": "JOB 2",
          "position_id": 5,
          "employee_person_id": 6,
          "full_name": "USER 6"
        }
      ],
      "positions": [
        {
          "position_id": 3,
          "job_id": 2,
          "organization_id": 1,
          "position": "POSITION 3",
          "job": "JOB 2",
          "uniorg": "UNIORG 1",
          "movements": [
            {
              "begin_date": null,
              "collaborator_id": 4,
              "collaborator_name": "COLLAB 1",
              "end_date": null,
              "iteration": "PROMOTION",
              "position_id": 2,
              "previous_position_id": 3,
              "proposal_id": null,
              "quantity": 1,
              "motive": null
            },
            {
              "begin_date": null,
              "collaborator_id": 3,
              "collaborator_name": "COLLAB 2",
              "end_date": null,
              "iteration": "MAINTENANCE",
              "position_id": 3,
              "previous_position_id": 3,
              "proposal_id": null,
              "quantity": 1,
              "motive": null
            },
            {
              "begin_date": "01/02/2015",
              "collaborator_id": null,
              "collaborator_name": null,
              "end_date": null,
              "iteration": "HIRE",
              "position_id": 3,
              "previous_position_id": null,
              "proposal_id": null,
              "quantity": 1,
              "motive": "Increse team"
            }
          ]
        },
        {
          "position_id": 1,
          "job_id": 2,
          "organization_id": 1,
          "position": "POSITION 3",
          "job": "JOB 2",
          "uniorg": "UNIORG 1",
          "movements": [
            {
              "begin_date": null,
              "collaborator_id": 4,
              "collaborator_name": "COLLAB 3",
              "end_date": null,
              "iteration": "PROMOTION",
              "position_id": 3,
              "previous_position_id": 1,
              "proposal_id": null,
              "quantity": 1
            }
          ]
        }
      ]
    }

    other_job_attributes = {
      "name": "JOB 3",
      "job_id": "3",
      "current_team": [],
      "positions": [
        {
          "position_id": 2,
          "job_id": 3,
          "organization_id": 1,
          "position": "POSITION 3",
          "job": "JOB 2",
          "uniorg": "UNIORG 1",
          "movements": []
        }
      ]
    }
    job = new Job(job_attributes)
    other_job = new Job(other_job_attributes)

    jobs = [job, other_job]

    Movements.initialize(jobs)
  }))

  describe('calculateMovements', function() {

    it('should calculate the quantity of "HIRES" planned to that job', function() {
      job.calculateMovements()

      expect(job.hiresCount).toEqual(1)
    })

    it('should calculate the quantity of collaborators planned to be promoted to other job', function() {
      job.calculateMovements()
      other_job.calculateMovements()

      expect(job.promotionsOutCount).toEqual(2)
      expect(other_job.promotionsInCount).toEqual(1)
    })

    it('should calculate the quantity of collaborators planned to be promoted to other position on the same job', function() {
      job.calculateMovements()
      other_job.calculateMovements()

      expect(job.promotionsInCount).toEqual(1)
      expect(job.promotionsOutCount).toEqual(2)
    })

    it('should calculate the quantity of collaborators planned to be Fired', function() {
      job.calculateMovements()

      expect(job.firesCount).toEqual(0)
    })
  })

  describe('teamSize()', function() {
    xit('should calculate the quantity of members that has valid positions before planning', function() {
      expect(job.teamSize()).toEqual(2)
    })
  })
})
